#pragma once

#include "core/SDCommons.h"

class SDSurfaces {
public:
	/*
	 * Diviser une BrepFace en 2 au milieu du paramètre de direction dir
	 * Les nouvelles brepFaces seront insérées dans le Brep équivalent
	 */
	static bool SplitBrepFace(SDFace* inFace,
			int dir, ON_NurbsSurface *leftOrTopSr, ON_NurbsSurface *rightOrBotSrf);
	/*
	  Description:
	    Expert user function.
	    Splits an edge into two edges.  The input edge
	    becomes the left portion and a new edge is created
	    for the right portion.
	  Parameters:
	    edge_index - [in] index of edge in brep.m_E[]
	    edge_t - [in] 3d edge splitting parameter
	    trim_t - [in] array of trim splitting parameters.
	             trim_t[eti] is the parameter for splitting the
	             trim brep.m_T[edge.m_ti[eti]].
	    vertex_index - [in] if not -1, then this vertex will be
	             used for the new vertex.  Otherwise a new
	             vertex is created.
	    bSetTrimBoxesAndFlags - [in] if true, trim boxes and flags
	             are set.  If false, the user is responsible for
	             doing this.  Set to true if you are unsure
	             what to use.  If you pass false, then need to
	             call SetTrimBoundingBoxes(..,bLazy=true)
	             so that the trim iso flags and bounding info
	             is correctly updated.  If you pass true, then
	             the trim flags and bounding boxes get set
	             inside of SplitEdge.
	  Returns:
	    True if successful.

	    https://github.com/qcad/qcad/blob/master/src/3rdparty/opennurbs/opennurbs_brep.cpp
	 */
	bool SplitEdge(ON_Brep* brep,
			int edge_index,
			double edge_t,
			const ON_SimpleArray<double>& trim_t,
			int vertex_index = -1,
			bool bSetTrimBoxesAndFlags = true
	);
};
