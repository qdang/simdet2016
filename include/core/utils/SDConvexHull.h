#ifndef _SDCONVEXHULL_H_
#define _SDCONVEXHULL_H_

#include <vector>
#include <stdlib.h>
#include <eigen3/Eigen/Dense>

using namespace Eigen;
using namespace std;

class SDConvexHull
{
public:
	//static vector<Vector2d> Find(vector<Vector2d>& P);
	SDConvexHull();
	void init(vector<Vector2d>& P, bool runAlgo = true);
	int isPointInside(double& testx, double& testy);

	vector<Vector2d> 	m_Hull;
	int					m_Vert;
private:
	double isLeft( Vector2d& P0, Vector2d& P1, Vector2d& P2 );
};

#endif
