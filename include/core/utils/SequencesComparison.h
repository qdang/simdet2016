#include <iostream>
#include <vector>
#include <eigen3/Eigen/SVD>
#include "SDIms/core/MathExt.hpp"

using namespace SDIms;
using namespace std;

class SwapableNumberPair;

class SequencesComparison {
public:
	SequencesComparison();
	/*
	 * Ordonner les deux séquences de nombres. Retourner les indices
	 * de seq2 après avoir ordonner les nombres de seq2 pour qu'ils soient
	 * cohérents à ceux de seq1
	 */
	static bool orderTwoSequences(vector<SwapableNumberPair>& seq1,
			vector<SwapableNumberPair>& seq2,
			vector<vector<int> >& outSeq2Idx);
	/*
	 * The Goodman-Kruskal index measures rank correlation between two sequences.
	 * Plus l'indice est positive plus les deux séquences se conviennent
	 */
	static double estimateGoodmanKruskalIndex(double* seq1, double* seq2, int& n);
};

class SwapableNumberPair {
public:
	SwapableNumberPair();
	SwapableNumberPair(double& num1, double& num2);
	SwapableNumberPair(const SwapableNumberPair& other);
	SwapableNumberPair& operator=(const SwapableNumberPair& other);
	/*
	 * return : 0 not equal, 1 equal, 2 equal but must be permuted
	 */
	int compareTo(const SwapableNumberPair& other);
	void swap();
	double m_num1, m_num2;
};
