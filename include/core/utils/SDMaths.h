/*
 * SDMaths.h
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#ifndef SDMATHS_H_
#define SDMATHS_H_

#include <iostream>
#include <complex>
#include <OpenNURBS/opennurbs.h>

extern "C" {

// More details for using http://www.mathkeisan.com/usersguide/man/dgeev.html
/*
DGEEV(3)              LAPACK driver routine (version 3.1)             DGEEV(3)



NAME
       DGEEV  - for an N-by-N real nonsymmetric matrix A, the eigenvalues and,
       optionally, the left and/or right eigenvectors

SYNOPSIS
       SUBROUTINE DGEEV( JOBVL, JOBVR, N, A, LDA, WR, WI, VL, LDVL, VR,  LDVR,
                         WORK, LWORK, INFO )

           CHARACTER     JOBVL, JOBVR

           INTEGER       INFO, LDA, LDVL, LDVR, LWORK, N

           DOUBLE        PRECISION  A( LDA, * ), VL( LDVL, * ), VR( LDVR, * ),
                         WI( * ), WORK( * ), WR( * )

PURPOSE
       DGEEV computes for an N-by-N real nonsymmetric matrix A, the  eigenval-
       ues and, optionally, the left and/or right eigenvectors.

       The right eigenvector v(j) of A satisfies
                        A * v(j) = lambda(j) * v(j)
       where lambda(j) is its eigenvalue.
       The left eigenvector u(j) of A satisfies
                     u(j)**H * A = lambda(j) * u(j)**H
       where u(j)**H denotes the conjugate transpose of u(j).

       The  computed  eigenvectors are normalized to have Euclidean norm equal
       to 1 and largest component real.


ARGUMENTS
       JOBVL   (input) CHARACTER*1
               = 'N': left eigenvectors of A are not computed;
               = 'V': left eigenvectors of A are computed.

       JOBVR   (input) CHARACTER*1
               = 'N': right eigenvectors of A are not computed;
               = 'V': right eigenvectors of A are computed.

       N       (input) INTEGER
               The order of the matrix A. N >= 0.

       A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
               On entry, the N-by-N matrix A.  On exit, A has  been  overwrit-
               ten.

       LDA     (input) INTEGER
               The leading dimension of the array A.  LDA >= max(1,N).

       WR      (output) DOUBLE PRECISION array, dimension (N)
               WI       (output)  DOUBLE PRECISION array, dimension (N) WR and
               WI contain the real and imaginary parts, respectively,  of  the
               computed  eigenvalues.   Complex conjugate pairs of eigenvalues
               appear consecutively with the eigenvalue  having  the  positive
               imaginary part first.

       VL      (output) DOUBLE PRECISION array, dimension (LDVL,N)
               If JOBVL = 'V', the left eigenvectors u(j) are stored one after
               another in the columns of VL, in the same order as their eigen-
               values.   If  JOBVL  =  'N', VL is not referenced.  If the j-th
               eigenvalue is real, then u(j) = VL(:,j), the j-th column of VL.
               If  the  j-th and (j+1)-st eigenvalues form a complex conjugate
               pair, then u(j) = VL(:,j) + i*VL(:,j+1) and
               u(j+1) = VL(:,j) - i*VL(:,j+1).

       LDVL    (input) INTEGER
               The leading dimension of the array VL.  LDVL >= 1; if  JOBVL  =
               'V', LDVL >= N.

       VR      (output) DOUBLE PRECISION array, dimension (LDVR,N)
               If  JOBVR  =  'V',  the  right eigenvectors v(j) are stored one
               after another in the columns of VR, in the same order as  their
               eigenvalues.   If JOBVR = 'N', VR is not referenced.  If the j-
               th eigenvalue is real, then v(j) = VR(:,j), the j-th column  of
               VR.  If the j-th and (j+1)-st eigenvalues form a complex conju-
               gate pair, then v(j) = VR(:,j) + i*VR(:,j+1) and
               v(j+1) = VR(:,j) - i*VR(:,j+1).

       LDVR    (input) INTEGER
               The leading dimension of the array VR.  LDVR >= 1; if  JOBVR  =
               'V', LDVR >= N.

       WORK       (workspace/output)   DOUBLE   PRECISION   array,   dimension
       (MAX(1,LWORK))
               On exit, if INFO = 0, WORK(1) returns the optimal LWORK.

       LWORK   (input) INTEGER
               The  dimension  of the array WORK.  LWORK >= max(1,3*N), and if
               JOBVL = 'V' or JOBVR = 'V', LWORK >=  4*N.   For  good  perfor-
               mance, LWORK must generally be larger.

               If  LWORK  = -1, then a workspace query is assumed; the routine
               only calculates the optimal size of  the  WORK  array,  returns
               this  value  as the first entry of the WORK array, and no error
               message related to LWORK is issued by XERBLA.

       INFO    (output) INTEGER
               = 0:  successful exit
               < 0:  if INFO = -i, the i-th argument had an illegal value.
               > 0:  if INFO = i, the QR algorithm failed to compute  all  the
               eigenvalues,  and  no eigenvectors have been computed; elements
               i+1:N of WR and WI contain eigenvalues which have converged.
*/

void dgeev_(char *jobvl, char *jobvr, int *n, double *a,
		int *lda, double *wr, double *wi, double *vl,
		int *ldvl, double *vr, int *ldvr,
		double *work, int *lwork, int *info);
/*
 *
 * http://www-heller.harvard.edu/people/shaw/programs/lapack.html
 * http://stackoverflow.com/questions/3519959/computing-the-inverse-of-a-matrix-using-lapack-in-c
 *
  This file has my implementation of the LAPACK routine dgeev
  for C++.  This program solves for the eigenvalues and, if
  desired, the eigenvectors for a square asymmetric matrix H.
  Since the matrix is asymmetrix, both real and imaginary
  components of the eigenvalues are returned.

  There are two function calls defined in this header, of the
  forms

    void dgeev(double **H, int n, double *Er, double *Ei)
    void dgeev(double **H, int n, double *Er, double *Ei, double **Evecs)

    H: the n by n matrix that we are solving
    n: the order of the square matrix H
    Er: an n-element array to hold the real parts of the eigenvalues
        of H.
    Ei: an n-element array to hold the imaginary parts of the
        eigenvalues of H.
    Evecs: an n by n matrix to hold the eigenvectors of H, if
           they are requested.

  The function call is defined twice, so that whether or not
  eigenvectors are called for the proper version is called.

  Scot Shaw
  30 August 1999

  29 August 2012:
  *** Update by Jérémy RIVIERE
  Added the ifndef trick to avoid multiple insertion issues.
 */
// matrix balancing
// refs:
// http://www.cs.kent.ac.uk/projects/cxxr/doc/html/Lapack_8h_source.html
// http://rgm2.lab.nig.ac.jp/RGM2/func.php?rd_id=expm:dgebal
// http://www.weizmann.ac.il/matlab/techdoc/ref/eig.html
// void dgebal_(const char* job, const int* n, double* a, const int* lda,
//		int* ilo, int* ihi, double* scale, int* info);

/*
*
* DSYEV computes all eigenvalues and, optionally, eigenvectors of a
*  real symmetric matrix A.
*
*  Arguments
*  =========
*
*  JOBZ    (input) CHARACTER*1
*          = 'N':  Compute eigenvalues only;
*          = 'V':  Compute eigenvalues and eigenvectors.
*
*  UPLO    (input) CHARACTER*1
*          = 'U':  Upper triangle of A is stored;
*          = 'L':  Lower triangle of A is stored.
*
*  N       (input) INTEGER
*          The order of the matrix A.  N >= 0.
*
*  A       (input/output) DOUBLE PRECISION array, dimension (LDA, N)
*          On entry, the symmetric matrix A.  If UPLO = 'U', the
*          leading N-by-N upper triangular part of A contains the
*          upper triangular part of the matrix A.  If UPLO = 'L',
*          the leading N-by-N lower triangular part of A contains
*          the lower triangular part of the matrix A.
*          On exit, if JOBZ = 'V', then if INFO = 0, A contains the
*          orthonormal eigenvectors of the matrix A.
*          If JOBZ = 'N', then on exit the lower triangle (if UPLO='L')
*          or the upper triangle (if UPLO='U') of A, including the
*          diagonal, is destroyed.
*
*  LDA     (input) INTEGER
*          The leading dimension of the array A.  LDA >= max(1,N).
*
*  W       (output) DOUBLE PRECISION array, dimension (N)
*          If INFO = 0, the eigenvalues in ascending order.
*
*  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
*          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
*
*  LWORK   (input) INTEGER
*          The length of the array WORK.  LWORK >= max(1,3*N-1).
*          For optimal efficiency, LWORK >= (NB+2)*N,
*          where NB is the blocksize for DSYTRD returned by ILAENV.
*
*          If LWORK = -1, then a workspace query is assumed; the routine
*          only calculates the optimal size of the WORK array, returns
*          this value as the first entry of the WORK array, and no error
*          message related to LWORK is issued by XERBLA.
*
*  INFO    (output) INTEGER
*          = 0:  successful exit
*          < 0:  if INFO = -i, the i-th argument had an illegal value
*          > 0:  if INFO = i, the algorithm failed to converge; i
*                off-diagonal elements of an intermediate tridiagonal
*                form did not converge to zero.
*
*  =====================================================================
*  */
void dsyev_(char *jobz, char *uplo, int *n, double *a,
		int *lda, double *w, double* work, int *lwork, int *info);

// LU decomoposition of a general matrix
void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

// generate inverse of a matrix given its LU decomposition
void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);

// Routines to solve Ax=b type linear systems
/*
 * N	number of columns of A
 * nrhs	number of columns of b, usually 1
 * lda	number of rows (Leading Dimension of A) of A
 * ipiv	 pivot indices
 * ldb	number of rows of b
*/
void dgesv_(int *N, int *nrhs, double *A, int *lda, int *ipiv, double *b, int *ldb, int *info);
}

using namespace std;

#define SD_ZERO_TOLERANCE 1.0e-16
#define SD_UMBILIC_CURVATURE_RATIO ON_DBL_PINF
#define SD_ZERO_CURVATURE_TOLERANCE ON_ZERO_CURVATURE_TOLERANCE

class SDMaths {
public:
	SDMaths();
	virtual ~SDMaths();

	/************** Matrix utils **********************/

	/*
	 * Evaluer la déterminante de la matrice carrée définie par
	 * un tableau de 2 dimension, et de dimension n
	 * Input : double** array de 2 dimensions, les éléments de la
	 * 		matrice de gauche à droite, de haut en bas
	 * 		int la dimentsion de la matrice
	 * Ouput : double la déterminante
	 */
	static double EvalDeterminant(double**, int);
	static double EvalDeterminant(double[][4], int);
	static double EvalDotProduct(ON_3dVector, ON_3dVector);
	static double EvalDotProduct2d(ON_2dPoint, ON_2dPoint);
	static ON_3dVector EvalCrossProduct(ON_3dVector, ON_3dVector);
	/*
	 * Evaluer les valeurs propres et les vecteurs propres de la
	 * matrice générale
	 * Input : H double** [in] - tableau de 2 dimensions
	 * 			n int [in] - dimension de la matrice
	 * 			Er double* [out] - les éléments réels des vecteurs propres
	 * 			Ei double* [out] - les éléments imaginaires des vecteurs propres
	 * 			Evecs double** [out] - les vecteurs propres sous formes des colonnes
	 */
	static bool dgeev(double **H, int n, double *Er, double *Ei, double **Evecs, bool* hasConjugatedEigVectors = NULL);

	// Evaluer les valeurs propres et les vecteurs propres d'une matrice
	// symmétrique
	static bool dsyev(double **H, int n, double *Er, double **Evecs);
	static void dsyev_sort(double *Er, double **Evecs, int n);
	// Inverser la matrice
	static void inverse(double* A, int N);

	// Résoudre le système linéaire A*x = B (A carrée 3x3, B 3x1)
	static bool dgesv(double** A, int n, double* B);

	static void AnalyzeEigenVectorsIndices(double* realEigenValues, double* imgEigenValues,
			int& minusOneIdx, int* oneIdx = NULL, int* oneNum = NULL, bool* hasConjEigenVectors = NULL);
	static double MatrixTrace(double** A, int size_m);
	static bool isMatrixIdentity(double** A, int size_m);
	static bool areMatricesEqual(ON_Matrix&, ON_Matrix&);

	/*********************Geometry Utils*************************/

	/*
	 * Calculer la distance minimale d'un point (from) à un segement (to1, to2)
	 * Référence : 	http://mathworld.wolfram.com/Point-LineDistance2-Dimensional.html
	 *				http://forums.codeguru.com/showthread.php?194400-Distance-between-point-and-line-segment
	 * Entrées:
	 * 		- from [in]: le point à partir duquel la distance est calculée
	 * 		- to1 [in]: extrémité du segment
	 * 		- to2 [in]: extrémité du segment
	 * Sorties:
	 * 		la distance minimale
	 */
	static double DistancePointToSegment(ON_2dPoint& from, ON_2dPoint& to1, ON_2dPoint& to2);
	static ON_3dPoint ProjectPointOnLine(const ON_3dVector& lineDirective,
			const ON_3dPoint& pointOnLine, const ON_3dPoint& point);
	static ON_3dPoint ReflectPointAcrossPlane(const ON_3dVector& planeNormal,
			const ON_3dPoint& pointOnPlan, const ON_3dPoint& pointOfInterest);
	static double DistancePointToPlane(const ON_3dVector& planeNormal,
			const ON_3dPoint& pointOnPlan, const ON_3dPoint& pointOfInterest);
	static ON_3dPoint RotatePointAroundLine(const ON_3dVector& axis,
			const ON_3dPoint& origin, const double& angle, const ON_3dPoint& pointOfInterest);
	static bool FindIntersectionLinePlane(const ON_3dVector& planeNormal, const ON_3dPoint& pointOnPlan,
			const ON_3dPoint& linePoint1, const ON_3dPoint& linePoint2, ON_3dPoint& intersection);

	/*******************Numerical Utils**********************/

	template<typename T>
	static bool equal(const T& t1, const T& t2, double tolerance = SD_ZERO_TOLERANCE) {return fabs(t1-t2) <= tolerance;}
	static bool almostEqual(double A, double B, int maxUlps);
	template <typename T>
	static string 	NumberToString ( T Number )
	{
		ostringstream ss;
		ss << Number;
		return ss.str();
	}

private:
	static double* dgeev_ctof(double **in, int rows, int cols);
	static void dgeev_ftoc(double *in, double **out, int rows, int cols);
	static void dgeev_sort(double *Er, double *Ei, double **Evecs, int N);
	static bool dgeev_getRealEigVec(int *n, double *wr, double *wi, double *vr, int *ldvr, double** evecs);
};

#endif /* SDMATHS_H_ */
