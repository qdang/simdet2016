/*
 * SDPoint.h
 *
 *  Created on: Jul 29, 2013
 *      Author: dqviet
 */

#ifndef SDPOINT_H_
#define SDPOINT_H_

#include "core/SDCommons.h"

#include <SDIms/core/Point.hpp>

using namespace SDIms;

class SDSignature
{
public:
	SDSignature();
	SDSignature(double, double);
	SDSignature(double, double, double, double);
	SDSignature(const SDSignature&);
	SDSignature& operator=(const SDSignature&);
	bool 	operator==(const SDSignature& other);
	double 	operator-(const SDSignature& other);
	bool	equalsTo(const SDSignature& other, SDCurvatureComparison method = SDCurvatureComparison::Gaussian,
					double tolerance = SD_ZERO_CURVATURE_TOLERANCE);
	double			getCurvatureRatio();
	void			setMaxCurvature(double);
	const double& 	getMaxCurvature();
	void			setMinCurvature(double);
	const double& 	getMinCurvature();
	const double&	getGaussCurvature();
	const double&	getMeanCurvature();
private:
	void	updateCurvatures();

	double 	m_MaxCurvature;
	double 	m_MinCurvature;
	double 	m_GaussCurvature;
	double 	m_MeanCurvature;
};

class SDParametricProperties
{
public:
	SDParametricProperties();
	SDParametricProperties(const SDParametricProperties&);
	SDParametricProperties& operator=(const SDParametricProperties&);
	virtual ~SDParametricProperties();
	void	setU(double);
	void	setV(double);
	void	setBrepIndex(int);
	void	setFaceIndex(int);
	double	getU();
	double	getV();
	int 	getBrepIndex();
	int		getFaceIndex();
private:
	double 	m_U, m_V;
	int		m_BrepIndex, m_FaceIndex;
};

class SDPoint : public SDIms::Point
{
public:
	SDPoint();
	SDPoint(double* coord, double* normal, double* maxTangent, double* minTangent);
	SDPoint(const SDPoint&);
	SDPoint& operator=(const SDPoint&);
	~SDPoint();

	SDParametricProperties& 	getParamProps();
	SDSignature& 				getSignature();
	Vec2& 						getGridId();
	void	setParamProps(const SDParametricProperties& paramProps);
	void	setSignature(const SDSignature& signature);
	void	setGridId(int x, int y);

	double	distanceTo(SDPoint& other);
	bool 	isEvaluable();
	void 	setEvaluable(bool evaluable);
	bool 	isUmbilic() ;
	bool 	isUmbilic(double ratio);
	void 	setUmbilic(bool umbilic);
	bool 	leftOf(SDPoint&);
	string 	ToString();

private:
	SDSignature				m_Signature;
	SDParametricProperties	m_ParamProps;
	Vec2					m_GridId;

	bool	m_Evaluable;
	bool	m_Umbilic;
};

#else
class SDPoint;

#endif /* SDPOINT_H_ */
