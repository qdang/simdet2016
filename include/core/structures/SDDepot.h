/*
 * SDDepot.h
 *
 *  Created on: Jul 29, 2013
 *      Author: dqviet
 */

#ifndef SDDEPOT_H_
#define SDDEPOT_H_

#include "core/SDCommons.h"

#include "SDIms/core/Isometry.hpp"

using namespace std;
using namespace Eigen;

class SDFacesPair;
class SDTransformation;

class SDDepot {
public:
	SDDepot();
	~SDDepot();

	void clearSamples();

	vector<SDPoint*> 	m_Samples;
	SDGrid<SDPoint>		m_SamplesGrid;
	// Indices des échatillons clé dans m_Samples
	vector<int>			m_KeySamplesIndices;

	vector<SDFace*>		m_Faces;
	map<int, vector<SDTransformation*> >	m_Isometries;
	map<int, vector<SDFacesPair> >	m_SimFaces;
	vector<SDFacesPair>				m_NotMatchedFaces;
	vector<Vec3>					m_ProjPointOfIsometry;

	// Test pca on model
	Vec3	m_ModelPc1, m_ModelPc2, m_ModelPc3, m_ModelBarycenter;
};

#endif /* SDDEPOT_H_ */
