/*
 * SDTransformation.h
 *
 *  Created on: Aug 27, 2013
 *      Author: dqviet
 */

#ifndef SDTRANSFORMATION_H_
#define SDTRANSFORMATION_H_

#include "core/SDCommons.h"
#include "SDIms/core/Isometry.hpp"

#include "OpenNURBS/opennurbs_plane.h"
#include "OpenNURBS/opennurbs_point.h"
#include "OpenNURBS/opennurbs_uuid.h"
#include "OpenNURBS/opennurbs_xform.h"

class ON_2dPoint;
class ON_2dVector;
class ON_Matrix;

using namespace std;
using namespace SDIms;

class SDTransformation : public Isometry
{
public:
	SDTransformation();
	SDTransformation(SDPoint*, SDPoint*);
	SDTransformation(SDPoint*, SDPoint*, Matrix4d&);
	SDTransformation(SDPoint*, SDPoint*, SDGrid<SDPoint>*);
	SDTransformation(SDPoint* p1, SDPoint* p2, vector<SDPoint>& vp1, vector<SDPoint>& vp2);
	virtual ~SDTransformation();
	SDTransformation(const SDTransformation&);
	SDTransformation& operator=(const SDTransformation&);

	void		evalGLPlanePoints(double planeDiag = 1.0);
	virtual string 		ToString();

	SDPoint& 	getModP2();
	SDPoint* 	getP1();
	SDPoint* 	getP2();
	ON_UUID& 	getUuid();
	bool 		isValid();

	void 		setModP2(const SDPoint& modP2);
	void 		setP1(SDPoint* p1);
	void 		setP2(SDPoint* p2);
	void 		setValid(bool valid);
	void 		setUuid(const ON_UUID& uuid);
	void 		setGridPoint(SDGrid<SDPoint>*);

protected:
	void	Init();
	void 	cd_ProjectVectorsOnPlan(vector<SDPoint> &neighbors, SDPoint& markPoint);
	void	cd_GetOrthonormalBaseCoord(vector<SDPoint>& neighbors, SDPoint& markPoint, vector<Vec2>&);
	void	cd_GetAnglesBetweenAxes(vector<Vec2>& base, vector<Vec2>&);
	void 	cd_GetVectorsOrderByAngles(int* order, vector<Vec2>& angles);

	ON_UUID							m_uuid;
	SDPoint							*m_P1, *m_P2, *m_ModP2;
	bool							m_Valid;
	vector<vector<int> >			m_P1Mask, m_P2Mask;
	SDGrid<SDPoint>					*m_GridPoint;

private:
	void	CheckFramesDirections(vector<SDPoint>&, vector<SDPoint>&, bool fromGrid = true);
	vector<SDPoint> m_voisins_P1, m_voisins_P2;
};

class SDAffineTransformation : SDTransformation {
public:
	SDAffineTransformation();
	virtual ~SDAffineTransformation();
	const ON_3dVector& getRotation() const;
	void setRotation(const ON_3dVector& rotation);
	const ON_3dVector& getTranslation() const;
	void setTranslation(const ON_3dVector& translation);

private:
	void	Init();
	ON_3dVector		m_Translation;
	ON_3dVector		m_Rotation;
};

#else
class SDTransformation;
#endif /* SDTRANSFORMATION_H_ */
