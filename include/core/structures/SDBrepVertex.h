/*
 * SDBrepVertex.h
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#ifndef INCLUDE_CORE_STRUCTURES_SDBREPVERTEX_H_
#define INCLUDE_CORE_STRUCTURES_SDBREPVERTEX_H_

#include "core/SDCommons.h"

class SDBrepVertex {
public:
	SDBrepVertex();
	SDBrepVertex(const SDBrepVertex& other);
	SDBrepVertex& operator=(const SDBrepVertex& other);
	bool operator==(const SDBrepVertex& other);
	int m_vi;
	double m_u, m_v;
	double m_tangentsAngle; // l'angle entre deux tangentes
	// singular trim have the same vertex for 2 extremities, must hold
	// this vertex as two
	bool m_inSingularTrim;
};


#endif /* INCLUDE_CORE_STRUCTURES_SDBREPVERTEX_H_ */
