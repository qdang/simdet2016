/*
 * SDModel.h
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#ifndef SDMODEL_H_
#define SDMODEL_H_

#include "core/SDCommons.h"

#include "core/utils/SDMaths.h"

using namespace std;
using namespace Eigen;
using namespace SDIms;

typedef vector<vector<ON_4dPoint> > SheavesMatrix;

class SDFaceChild;
class SDDelaunayTriangle;
class SDParameters;

class SDModel : public ONX_Model {
public:
	SDModel();
	SDModel(string filePath);
	SDModel(SDParameters*);
	virtual ~SDModel();
	bool PointAt(int brep_index, int face_index, double u, double v, SDPoint* p, bool calculProperties = true);

	map<int, SDBrep*>	m_Breps;
	static ON_BoundingBox		m_Bbox;
	static double				m_BboxDiag;
	ON_3dmView 			m_View;
private:
	SDParameters* 		m_Params;

public:	// for test cases
	void outputControlPointsOfPlane();
};

class SDFaceChild {
public:
	enum SDFaceChildPos {
		WEST_OR_NORTH,
		EAST_OR_SOUTH
	};
	SDFaceChild();
	SDFaceChild(const SDFaceChild& other);
	SDFaceChild& operator=(const SDFaceChild& other);
	SDFaceChildPos	m_Pos;
	int				m_Depth;
	SDFace*			m_Face;
};

class SDDelaunayTriangle {
public:
	SDDelaunayTriangle(int a, int b, int c);
	SDDelaunayTriangle(const SDDelaunayTriangle& other);
	SDDelaunayTriangle& operator=(const SDDelaunayTriangle& other);
	bool	operator<(const SDDelaunayTriangle& other);
	bool	inTriangle(int inA, int& outB, int& outC);

	vector<int> m_vertices;
	bool	m_check;
};

#endif /* SDMODEL_H_ */
