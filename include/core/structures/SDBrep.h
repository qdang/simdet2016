/*
 * SDBrep.h
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#ifndef INCLUDE_CORE_STRUCTURES_SDBREP_H_
#define INCLUDE_CORE_STRUCTURES_SDBREP_H_

#include "core/SDCommons.h"

class SDFace;

class SDBrep {
public:
	SDBrep();
	SDBrep(ON_Brep*);
	SDBrep(const SDBrep& other);
	virtual ~SDBrep();
	SDBrep&		operator=(const SDBrep&);
	SDBrep*		operator=(SDBrep*);

	ON_Brep*	getONBrep();
	SDFace*		getSDFace(int);
	// Chercher la référence de SDFace qui contient ON_BrepFace.m_face_index = ONFi
	bool		getSDFace(int ONFi, SDFace*& sdFace);

	void setBrepIndex(int&);
	int  getBrepIndex();

	std::vector<SDFace*>	m_Faces;
private:
	ON_Brep*	m_Brep;		// Real Brep that SDBrep references to
	int			m_BrepIndex; // Brep index in SDModel
};


#endif /* INCLUDE_CORE_STRUCTURES_SDBREP_H_ */
