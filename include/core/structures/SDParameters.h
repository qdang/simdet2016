/*
 * SDParameters.h
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#ifndef SDPARAMETERS_H_
#define SDPARAMETERS_H_

#include "core/SDCommons.h"

using namespace std;

class SDParameters
{
public:
	static const int 	SD_INVALID_INDEX;
	static const char* 	SD_SAMP_MODELFILEPATH ;
	static const char* 	SD_SAMP_METHOD;
	static const char* 	SD_SAMP_UKEYRATIO ;
	static const char* 	SD_SAMP_VKEYRATIO ;
	static const char* 	SD_SAMP_URATIO ;
	static const char* 	SD_SAMP_VRATIO ;
	static const char* 	SD_SAMP_UGAPNUMBER ;
	static const char* 	SD_SAMP_VGAPNUMBER ;
	static const int	SD_SAMP_UNIFORM;
	static const int	SD_SAMP_ADAPTIVE;

	static const char* SD_PAIR_UMBILICRATIO ;
	static const char* SD_PAIR_NEIGHBORSRADIUS ;
	static const char* SD_PAIR_MAXPARTNERS ; //

	static const char* SD_CLUST_ISOMETRY ;
	static const char* SD_CLUST_MAXCLUSTERNUMBER ;
	static const char* SD_CLUST_MAXDATAPOINT;
	static const char* SD_CLUST_TRANSFORMATIONDIMENSION ;
	static const char* SD_VAL_THRESHOLD ;

	static const char* SD_REND_ISMODELDRAWN;
	static const char* SD_REND_ISSAMPLEDRAWN ;
	static const char* SD_REND_ISPAIRDRAWN ;
	static const char* SD_REND_ISCLUSTERDRAWN ;
	static const char* SD_REND_ISPATCHDRAWN ;

	/** 3dm file loader */
	string 		m_Sampling_ModelFilePath;

	/** Sampling step */
	int 		m_Sampling_Method;
	double** 	m_Sampling_UGap;
	double** 	m_Sampling_VGap;
	int** 		m_Sampling_UGapQuantity;
	int** 		m_Sampling_VGapQuantity;
	double 		m_Sampling_UKeyRatio;
	double		m_Sampling_VKeyRatio;
	double 		m_Sampling_URatio;
	double		m_Sampling_VRatio;
	double 		m_Sampling_MinDistSamples;

	/** Pairing step */
	double 		m_Pairing_NeighborsRadius;
	int			m_Pairing_MaxPartners;
	double 		m_Pairing_UmbilicRatio;

	/** Classification step */
	int			m_Clustering_Isometry;
	int 		m_Clustering_MaxClustersNumber;
	int 		m_Clustering_TransformDim;
	int			m_Clustering_MaxDataPoints;

	/** Validation step */
	double		m_Validating_Threshold;

	/** Rendering */
	bool 		m_Rendering_IsModelDrawn;
	bool 		m_Rendering_IsSamplesDrawn;
	bool 		m_Rendering_IsPairsDrawn;
	bool		m_Rendering_IsPatchDrawn;
	bool		m_Rendering_IsClusterDrawn;

	bool		m_IsDefault;
	string		m_FilePath;

	SDParameters();
	virtual ~SDParameters();
	bool LoadIniFile(const char* iniFile);
	void LoadParameters(const char* section);
	bool WriteIniFile(const char* iniFile);
	void Print();
	bool IsDefault();
	void SetDefault(bool);

};

#endif /* SDPARAMETERS_H_ */
