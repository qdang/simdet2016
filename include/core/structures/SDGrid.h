/*
 * SDGrid.hpp
 *
 *  Created on: Nov 18, 2012
 *      Author: dqviet
 */

#ifndef SDGRID_HPP_
#define SDGRID_HPP_

#include <iostream>
#include <vector>
#include <ANN/ANN.h>
#include <core/structures/SDPoint.h>

using namespace std;

template<class T> class SDFaceGrid
{
public:
	SDFaceGrid()
	{
		m_NumCol = m_NumRow = 0;
		m_PointGrid	= NULL;
	}

	SDFaceGrid(int height, int width)
	{
		m_NumRow = height; m_NumCol = width;
		Initialize(m_NumRow, m_NumCol);
	}

	SDFaceGrid(const SDFaceGrid<T>& other)
	{
		if(m_NumCol > 0 || m_NumRow > 0) {
			Clear();
		}

		// copy data
		if(other.m_NumRow > 0 && other.m_NumCol > 0)
		{
			m_NumCol = other.m_NumCol;
			m_NumRow = other.m_NumRow;
			Initialize(m_NumRow, m_NumCol);
			for(int row = 0; row < m_NumRow; row++)
			{
				for(int col = 0; col < m_NumCol; col++)
				{
					m_PointGrid[row][col] = other.m_PointGrid[row][col];
				}
			}
			m_PointsNumber	= other.m_PointsNumber;
		}
	}

	SDFaceGrid(SDFaceGrid<T>& other)
	{
		if(m_NumCol > 0 || m_NumRow > 0) {
			Clear();
		}

		// copy data
		if(other.m_NumRow > 0 && other.m_NumCol > 0)
		{
			m_NumCol = other.m_NumCol;
			m_NumRow = other.m_NumRow;
			Initialize(m_NumRow, m_NumCol);
			for(int row = 0; row < m_NumRow; row++)
			{
				for(int col = 0; col < m_NumCol; col++)
				{
					m_PointGrid[row][col] = other.m_PointGrid[row][col];
				}
			}
			m_PointsNumber	= other.m_PointsNumber;
		}
	}

	SDFaceGrid<T>& operator=(const SDFaceGrid<T>& other)
	{
		if(m_NumCol > 0 || m_NumRow > 0) {
			Clear();
		}

		// copy data
		if(other.m_NumRow > 0 && other.m_NumCol > 0)
		{
			m_NumCol = other.m_NumCol;
			m_NumRow = other.m_NumRow;
			Initialize(m_NumRow, m_NumCol);
			for(int row = 0; row < m_NumRow; row++)
			{
				for(int col = 0; col < m_NumCol; col++)
				{
					m_PointGrid[row][col] = other.m_PointGrid[row][col];
				}
			}
			m_PointsNumber	= other.m_PointsNumber;
		}
		return *this;
	}

	SDFaceGrid<T>& operator=(SDFaceGrid<T>& other)
	{
		if(m_NumCol > 0 || m_NumRow > 0) {
			Clear();
		}

		// copy data
		if(other.m_NumRow > 0 && other.m_NumCol > 0)
		{
			m_NumCol = other.m_NumCol;
			m_NumRow = other.m_NumRow;
			Initialize(m_NumRow, m_NumCol);
			for(int row = 0; row < m_NumRow; row++)
			{
				for(int col = 0; col < m_NumCol; col++)
				{
					m_PointGrid[row][col] = other.m_PointGrid[row][col];
				}
			}
			m_PointsNumber	= other.m_PointsNumber;
		}
		return *this;
	}

	~SDFaceGrid()
	{
		if(m_PointGrid)
		{
			Clear();
		}
	}

	bool Add(int row, int col, T& p)
	{
		if(row < 0 || row >= m_NumRow || col < 0 || col >= m_NumCol)
		{
			return false;
		}

		m_PointGrid[row][col] = p;
		m_PointsNumber++;

		return true;
	}

	int Count()
	{
		return m_PointsNumber;
	}

	bool Get(int row, int col, T& out)
	{
		if(row < 0 || row >= m_NumRow || col < 0 || col >= m_NumCol)
		{
			return false;
		}
		else
		{
			out = m_PointGrid[row][col];
			return true;
		}
	}

	T* Get(int row, int col) {
		if(row < 0 || row >= m_NumRow || col < 0 || col >= m_NumCol)
		{
			return NULL;
		}
		else
		{
			return &m_PointGrid[row][col];
		}
	}

	int Width()
	{
		return m_NumCol;
	}

	int Height()
	{
		return m_NumRow;
	}

private:
	int m_NumCol, m_NumRow, m_PointsNumber;
	T** m_PointGrid;

	void Initialize(int height, int width)
	{
		m_PointsNumber	= 0;
		m_PointGrid 	= new T*[height];
		for(int row = 0; row < height; row++)
		{
			m_PointGrid[row]	= new T[width];
		}
	}

	void Clear()
	{
		if(m_PointGrid) {
			for(int row = 0; row < m_NumRow; row++) {
				for(int col = 0; col < m_NumCol; col++) {
					m_PointGrid[row][col].~T();
				}
				delete[] m_PointGrid[row];
			}
			m_PointGrid		= NULL;
		}
		m_NumCol = m_NumRow = m_PointsNumber = 0;
	}
};

template<class T>
class SDBrepGrid
{
public:
	SDBrepGrid()
	{
		m_size 		= 0;
		m_FaceGrid	= NULL;
	}

	SDBrepGrid(int size)
	{
		m_size 		= size;
		m_FaceGrid	= new SDFaceGrid<T>[m_size];
	}

	SDBrepGrid(const SDBrepGrid<T>& other)
	{
		if(other.m_size > 0)
		{
			clear();

			m_size 		= other.m_size;
			m_FaceGrid	= new SDFaceGrid<T>[m_size];

			for(int s = 0; s < m_size; s++)
			{
				m_FaceGrid[s] = other.m_FaceGrid[s];
			}
		}
	}

	SDBrepGrid(SDBrepGrid<T>& other)
	{
		if(other.m_size > 0)
		{
			clear();

			m_size 		= other.m_size;
			m_FaceGrid	= new SDFaceGrid<T>[m_size];

			for(int s = 0; s < m_size; s++)
			{
				m_FaceGrid[s] = other.m_FaceGrid[s];
			}
		}
	}

	SDBrepGrid<T>& operator=(const SDBrepGrid<T>& other)
	{
		if(other.m_size > 0)
		{
			clear();

			m_size 		= other.m_size;
			m_FaceGrid	= new SDFaceGrid<T>[m_size];

			for(int s = 0; s < m_size; s++)
			{
				m_FaceGrid[s] = other.m_FaceGrid[s];
			}
		}

		return *this;
	}

	SDBrepGrid<T>& operator=(SDBrepGrid<T>& other)
	{
		if(other.m_size > 0)
		{
			clear();

			m_size 		= other.m_size;
			m_FaceGrid	= new SDFaceGrid<T>[m_size];

			for(int s = 0; s < m_size; s++)
			{
				m_FaceGrid[s] = other.m_FaceGrid[s];
			}
		}

		return *this;
	}

	~SDBrepGrid()
	{
		clear();
	}

	bool Add(int pos, SDFaceGrid<T>& faceGrid)
	{
		if(pos < 0 || pos >= m_size)
		{
			return false;
		}
		else
		{
			m_FaceGrid[pos] = faceGrid;
			return true;
		}
	}

	bool Get(int pos, SDFaceGrid<T>& out)
	{
		if(pos < 0 || pos >= m_size)
		{
			return false;
		}
		else
		{
			out	= m_FaceGrid[pos];
			return true;
		}
	}

	int Size()
	{
		return m_size;
	}

private:
	int m_size;
	SDFaceGrid<T> *m_FaceGrid;
	void clear() {
		if(m_FaceGrid)
		{
			for(int i = 0; i < m_size; i++)
				m_FaceGrid[i].~SDFaceGrid<T>();
			delete[] m_FaceGrid;
			m_FaceGrid	= NULL;
			m_size 		= 0;
		}
	}
};

template<class T>
class SDGrid
{
public:
	enum Direction
	{
	ROW_DIRECTION,
	COLUMN_DIRECTION
	};

	SDGrid()
	{
		m_size 		= 0;
		m_BrepGrid	= NULL;
	}

	SDGrid(int size)
	{
		init(size);
	}

	SDGrid(const SDGrid<T>& other)
	{
		clear();

		if(other.m_size > 0)
		{
			init(other.m_size);
			for(int s = 0; s < m_size; s++)
			{
				m_BrepGrid[s] = other.m_BrepGrid[s];
			}
		}
	}

	SDGrid(SDGrid<T>& other)
	{
		clear();

		if(other.m_size > 0)
		{
			init(other.m_size);
			for(int s = 0; s < m_size; s++)
			{
				m_BrepGrid[s] = other.m_BrepGrid[s];
			}
		}
	}

	SDGrid<T>& operator=(const SDGrid<T>& other)
	{
		clear();

		if(other.m_size > 0)
		{
			init(other.m_size);
			for(int s = 0; s < m_size; s++)
			{
				m_BrepGrid[s] = other.m_BrepGrid[s];
			}
		}

		return *this;
	}

	SDGrid<T>& operator=(SDGrid<T>& other)
	{
		clear();

		if(other.m_size > 0)
		{
			init(other.m_size);
			for(int s = 0; s < m_size; s++)
			{
				m_BrepGrid[s] = other.m_BrepGrid[s];
			}
		}

		return *this;
	}

	~SDGrid()
	{
		clear();
	}

	bool Add(int pos, SDBrepGrid<T>& b)
	{
		if(pos < 0 || pos >= m_size)
		{
			return false;
		}
		else
		{
			m_BrepGrid[pos] = b;
			return true;
		}
	}

	bool Get(int pos, SDBrepGrid<T>& out)
	{
		if(pos < 0 || pos >= m_size)
		{
			return false;
		}
		else
		{
			out = m_BrepGrid[pos];
			return true;
		}
	}

	// TODO: A revoir: la taille des breps dans le modèle
	// ne sont pas identiques!!!
	int Size(Direction dir = ROW_DIRECTION)
	{
		if(dir == ROW_DIRECTION)
			return m_size;
		else
		{
			if(m_size > 0)
			{
				return m_BrepGrid[0].Size();
			}
			else
			{
				return 0;
			}
		}
	}

	void setSize(int size) {
		init(size);
	}

private:
	int m_size;
	SDBrepGrid<T> *m_BrepGrid;

	void init(int size) {
		clear();
		m_size		= size;
		m_BrepGrid	= new SDBrepGrid<T>[m_size];
	}

	void clear() {
		if (m_BrepGrid) {
			for(int i = 0; i < m_size; i++)
				m_BrepGrid[i].~SDBrepGrid<T>();
			delete[] m_BrepGrid;
			m_size	= 0;
		}
	}
};

#else

template<class T> class SDFaceGrid;
template<class T> class SDBrepGrid;
template<class T> class SDGrid;

#endif /* SDGRID_HPP_ */
