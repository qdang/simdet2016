/*
 * SDFace.h
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#ifndef INCLUDE_CORE_STRUCTURES_SDFACE_H_
#define INCLUDE_CORE_STRUCTURES_SDFACE_H_

#include <Qt/quuid.h>

#include "core/SDCommons.h"

#include "core/utils/SDConvexHull.h"
#include "SDIms/core/Point.hpp"

using namespace SDIms;
using namespace Eigen;

class SDBrep;
class SDBrepVertex;

class SDFace
{
public:
	SDFace();
	SDFace(const SDFace&);
	SDFace(ON_BrepFace*);
	virtual ~SDFace();
	SDFace& operator=(const SDFace&);

	void			setONBrepFace(ON_BrepFace*);
	ON_BrepFace*	getONBrepFace();
	void			setSDBrep(SDBrep*);
	SDBrep*			getSDBrep();
	QUuid			getUUID();
	bool 			PointAt(double u, double v, SDPoint* point, bool calculProperties = true);
	string			OriginalFacePos();
	void			setFaceIndex(int& fi);
	int				getFaceIndex();
	bool			updateCornersFrame(vector<int>* cornerOrders = NULL);
	bool			getNeighborFace(SDBrepVertex& v1, SDBrepVertex& v2, SDFace*& face);

	bool			intersectPlane(Vec3& planeNormal, Vec3& planePoint);

	SDPoint			getCornersCenter(vector<int>* cornerOrd = NULL);
	bool			IsParamInsideParametricDomain(double &u, double &v,
						SDConvexHull &hullOuter, vector<SDConvexHull> &hullInner);
	void			generateSamplesInFace();
	void			generateUniformSamples();
	void			generateUniformDistanceSamples();

	SDPoint*					m_CornersCenter;
	vector<SDPoint*>			m_Corners, m_CornersOF; // OF : on the fly

	vector<SDBrepVertex> 	m_vertIdxList;
	// L'ordre des corners d'une face au moment du calcul
	// Façon légale pour récupérer les corners dans l'ordre :
	// m_Corners[m_CornersLoop[i]] - i = 1..n
	vector<int>				m_CornersLoop;

	Frame					m_CornersFrame;
	// propriété on-the-fly pour l'extension des faces
	ON_UUID					m_CurrentTransf;
	// l'indice de ON_BrepFace dans ON_Brep.m_F[]
	int						m_ONFaceIndex;
	int						m_ParamRefineStep;
	double					m_BBDiagonal;
	ON_BoundingBox			m_BBox;
	vector<Vec3>			m_PointsOnFace;
	vector<SDPoint>			m_SDPointsOnFace;
	vector<Vec3>			m_CornersPos;

	// Used for redundancy check
	vector<SDFace*>				m_SimFaces;
	vector<IsometryType>	m_SimTypes;
	bool						m_IsSamplingUniform;
private:
	ON_BrepFace* 	m_Face;		// Real face that SDFace references to
	SDBrep*			m_Brep;		// SDBrep that SDFace belongs to
	int				m_FaceIndex;

	void	getCornerVerticesIndices();
	void	estimateCorners();
	void	estimateBoundingBox();
	void 	getLoopUniformParamsDistPoly(
				ON_BrepLoop* loop,
				vector<Vector2d>& poly);
	void 	getLoopUniformParamsPoly(
				ON_BrepLoop* loop,
				const double& minDist,
				const double& tolerance,
				vector<Vector2d>& poly);
};


#endif /* INCLUDE_CORE_STRUCTURES_SDFACE_H_ */
