/*
 * SDFacesPair.h
 *
 *  Created on: Jul 29, 2016
 *      Author: formation
 */

#ifndef INCLUDE_CORE_STRUCTURES_SDFACESPAIR_H_
#define INCLUDE_CORE_STRUCTURES_SDFACESPAIR_H_

#include "core/SDCommons.h"

using namespace std;

/**
 * SDFacesPairCorners holds circular indices of a face Fi compared to another face Fj
 */
class SDFacesPairCorners{
public:
	SDFacesPairCorners();
	SDFacesPairCorners(int size);
	~SDFacesPairCorners();
	SDFacesPairCorners(const SDFacesPairCorners&);
	SDFacesPairCorners& operator=(const SDFacesPairCorners&);
	SDFacesPairCorners	inverse();
	void	set(vector<int>&, vector<int>&);
	void	setFj(vector<int>&);
	int		size();
	int		getFi(int& i);
	int		getFj(int& i);
private:
	vector<int>	m_FiIdx, // corners indices of Fi
				m_FjIdx; // other face Fj
	// the number of corners concerned to the relationship between 2 faces
	int			m_size;
};

class SDTransformation;

class SDFacesPair {
public:
	enum SDMatchType {
		FACES,
		POINTS,
		TOREVISE
	};
	SDFacesPair();
	SDFacesPair(SDFace* f1, SDFace* f2,
			SDTransformation* t, SDMatchType type = FACES);
	SDFacesPair(const SDFacesPair& other);
	SDFacesPair& operator=(const SDFacesPair& other);

	~SDFacesPair();
	void add(SDFace* f1, SDFace* f2);
	void set(SDFace* f1, SDFace* f2,
			SDTransformation* t, SDMatchType type = FACES);

	SDFace 				*m_f1, *m_f2;
	vector<SDFace*>		m_OrigFaces, m_TransFaces;
	vector<Vec3>		m_OrigPoints, m_TransPoints;
	SDTransformation* 	m_T;
	SDMatchType			m_MatchType;
	vector<int>			m_F2OrdCornIdx;
	SDFacesPairCorners	m_CornersIdx;
	double				m_Volume;
	Vec3				m_pc1, m_pc2, m_pc3, m_center;
};



#endif /* INCLUDE_CORE_STRUCTURES_SDFACESPAIR_H_ */
