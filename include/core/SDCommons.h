/*
 * SDCommons.h
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#ifndef INCLUDE_CORE_SDCOMMONS_H_
#define INCLUDE_CORE_SDCOMMONS_H_

#include <unistd.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <queue>
#include <map>

#include <iostream>
#include <fstream>
#include <sstream>

#include <math.h>
#include <cmath>

#include <pthread.h>

#include <simpleini/SimpleIni.h>

#include <OpenNURBS/opennurbs.h>
#include <glog/logging.h>

#include <quuid.h>

#include "SDIms/core/Global.hpp"

#include "core/utils/SDMaths.h"

#include "core/structures/SDParameters.h"
#include "core/structures/SDPoint.h"
#include "core/structures/SDBrepVertex.h"
#include "core/structures/SDFace.h"
#include "core/structures/SDBrep.h"
#include "core/structures/SDModel.h"
#include "core/structures/SDGrid.h"
#include "core/structures/SDTransformation.h"
#include "core/structures/SDFacesPair.h"

#include "core/structures/SDModel.h"
#include "core/structures/SDDepot.h"

#endif /* INCLUDE_CORE_SDCOMMONS_H_ */
