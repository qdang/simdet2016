#ifndef SDPIPELINE_H_
#define SDPIPELINE_H_

#include <vector>
#include <dirent.h>
#include "core/structures/SDModel.h"
#include "core/structures/SDDepot.h"

class SDPipeline {
public:
	SDPipeline();
	~SDPipeline();

	void	grabFaces();
	void	run();
	void	runMultiModels();
	void	NormalizeModel();

	SDModel*			m_Model;
	SDDepot*			m_Depot;

	int		m_MaxSymIdx;
};

#endif
