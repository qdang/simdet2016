/*
 * MathExt.hpp
 *
 *  Created on: Oct 9, 2013
 *      Author: dqviet
 */

#ifndef SDMATHEXT_HPP_
#define SDMATHEXT_HPP_
#include "SDIms/core/Global.hpp"

using namespace SDIms;

namespace SDIms {
	const double SDIms_ZERO_TOL = 1e-5;

	class MathExt {
	public:
		template <typename T>
		static bool equal(const T& a, const T& b, double tolerance = SDIms_ZERO_TOL) {
			return fabs(a - b) <= tolerance;
		}

		/*
		 * Référence : http://en.wikipedia.org/wiki/Vector_projection
		 */
		/*
		 * Calculer la longueur de la projection de a sur b
		 * Return : le scalaire de la longueur
		 */
		static double scalarProjection(const Vec3& a, const Vec3& b);
		/*
		 * Calculer le vecteur de la projection de a sur b
		 * Return : projection - le vecteur de la projection
		 */
		static void vectorProjection(const Vec3& a, const Vec3& b, Vec3& projection);
		/*
		 * Calculer la projection d'un point sur une droite
		 * lineDirective doit être unitaire
		 */
		static void pointOnLineProjection(const Vec3& lineDirective, const Vec3& linePoint,
				const Vec3& point, Vec3& projection);
		/*
		 * Calculer le vecteur de la projection de a sur le plan défini par deux vecteurs (t1, t2).
		 * t1 et t2 doivent être unitaires.
		 * Return : true si t1 et t2 ne sont pas colinéaires
		 * 			projection - le vecteur de la projection
		 */
		static bool vectorOnPlaneProjection(const Vec3& t1, const Vec3& t2, const Vec3& a, Vec3& projection);
		static bool vectorOnPlaneProjection(const Vec3& normal, const Vec3& a, Vec3& projection);
		static bool lineSegmentOnPlaneProjection(const Vec3& planNormal,
												const Vec3& planPoint,
												const Vec3& pStart,
												const Vec3& pEnd,
												Vec3& vProj);
		static bool pointOnPlaneProjection(const Vec3& point,
											const Vec3& planeNormal,
											const Vec3& planePoint,
											Vec3& vProj);
		static bool reflectPointViaPlane(const Vec3& point,
				const Vec3& planeNormal,
				const Vec3& planePoint,
				Vec3& vRefl);

		// http://fr.wikipedia.org/wiki/Distance_d'un_point_à_un_plan
		static double distancePointPlane(const Vec3& pa, const Vec3& normal, const Vec3& pPlane);

		static double HausdorffDistance(const vector<Vec3>& p, const vector<Vec3>& q);

		/*
		 * Calculer l'angle orienté entre deux vecteurs situés dans un plan défini par un vecteur normal.
		 * L'angle est calculé par rapport au vecteur de référence refV.
		 * Etant donné que tous les vecteurs sont normalisés
		 * Return : angle.
		 * http://stackoverflow.com/questions/5188561/signed-angle-between-two-3d-vectors-with-same-origin-within-the-same-plane-reci
		 */
		static double evalOrientedAngle2Vectors(const Vec3& refV, const Vec3& normal, const Vec3& intV);

		/*
		 * Le modulo dans C/C++ peut donner un résultat négatif, cette fonction règle ce problème
		 */
		static int mod(int a, int b);

		template<typename Derived>
		static bool is_finite(const Eigen::MatrixBase<Derived>& x)
		{
		    return ( (x - x).array() == (x - x).array()).all();
		}

		template<typename Derived>
		static bool is_nan(const Eigen::MatrixBase<Derived>& x)
		{
		    return ((x.array() != x.array())).all();
		}
	};
}


#endif /* SDMATHEXT_HPP_ */
