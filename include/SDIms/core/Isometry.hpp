/*
 * Isometry.hpp
 *
 *  Created on: Oct 8, 2013
 *      Author: dqviet
 */

#ifndef SDISOMETRY_HPP_
#define SDISOMETRY_HPP_

#include "SDIms/core/Global.hpp"
#include "SDIms/core/Point.hpp"
#include "SDIms/core/MathExt.hpp"

using namespace Eigen;
using namespace SDIms;

typedef Vector3d Vec3;

namespace SDIms {

	class Feature;

	class Isometry {
	public:
		Isometry();
		Isometry(const Isometry& other);
		Isometry& operator=(const Isometry& other);
		~Isometry();
		void init(Point* p1, Point* p2);
		void init(Point* p1, Point* p2, Matrix4d& transformation);
		double	distanceTo(Isometry& other);
		double	distanceTo(Isometry* other);

		int	TOneEigSpaceSize();
		int TMinusOneEigSpaceSize();

		static string GetIsometryClassName(int name);
		string	info();
		static double model_BBoxDiagonal;

	private:
		void evalTransformation();
		bool evalInvariants(Matrix4d& mat);
		void evalFixedPoints();
		void classify();

		bool evalTranslation();

	public:
		IsometryClasses		m_Class;
		IsometryType		m_Type;
		Feature*			m_Feature;
		Point				*m_P1, *m_P2;
		Matrix4d			m_T, m_G;
		Vec3				m_ta, m_FixedPoint;
		int					m_EigValT1Num, m_EigValT_1Num;
		Vec3				m_EigValT1Idx, m_EigValT_1Idx;
		Matrix3cd			m_EigSpaceT;	// eigenspace of m_T
		Vector3cd			m_EigValT;
		double				m_BBDiagonal;
	};

	class Feature {
	public:
		Feature();
		Feature(Isometry* isometry);
		Feature(const Feature& other);
		Feature& operator=(const Feature& other);
		virtual ~Feature();
		double	distanceTo(Feature* other);
		string	info();

		IsometryClasses		m_Class;
		int			m_featureSize;
		Isometry*	m_isometry;
	};

	class FeatureRot : public virtual Feature {
	public:
		FeatureRot(Isometry* isometry);
		FeatureRot(const FeatureRot& other);
		~FeatureRot();
		double distanceTo(FeatureRot& other);
		double distanceTo(FeatureRot* other);

		Vec3	m_Axis;
		Vec3	m_Origin;
		double	m_Angle;
	};

	class FeatureTrans : public virtual Feature {
	public:
		FeatureTrans(Isometry* isometry);
		FeatureTrans(const FeatureTrans& other);
		~FeatureTrans();
		double distanceTo(FeatureTrans& other);
		double distanceTo(FeatureTrans* other);

		Vec3	m_Translation;
	};

	class FeatureSym : public virtual Feature {
	public:
		FeatureSym(Isometry* isometry);
		FeatureSym(const FeatureSym& other);
		~FeatureSym();
		double distanceTo(FeatureSym& other);
		double distanceTo(FeatureSym* other);

		Vec3	m_PlaneNormal;
		Vec3	m_PlanePoint;

		Vec3	m_GLA, m_GLB, m_GLC, m_GLD; // For drawing symmetry plane
	};

	class FeatureViss : public FeatureRot, public FeatureTrans {
	public:
		FeatureViss(Isometry* isometry);
		FeatureViss(const FeatureViss& other);
		~FeatureViss();
		double distanceTo(FeatureViss& other);
		double distanceTo(FeatureViss* other);
	};

	class FeatureSymRot : public FeatureSym, public FeatureRot {
	public:
		FeatureSymRot(Isometry* isometry);
		FeatureSymRot(const FeatureSymRot& other);
		~FeatureSymRot();
		double distanceTo(FeatureSymRot& other);
		double distanceTo(FeatureSymRot* other);
	};

	class FeatureSymTrans : public FeatureSym, public FeatureTrans {
	public:
		FeatureSymTrans(Isometry* isometry);
		FeatureSymTrans(const FeatureSymTrans& other);
		~FeatureSymTrans();
		double distanceTo(FeatureSymTrans& other);
		double distanceTo(FeatureSymTrans* other);
	};

	class FeatureCSym : public Feature {
	public:
		FeatureCSym(Isometry* isometry);
		FeatureCSym(const FeatureCSym& other);
		~FeatureCSym();

		Vec3*	m_Center;
	};
}

#endif /* SDISOMETRY_HPP_ */
