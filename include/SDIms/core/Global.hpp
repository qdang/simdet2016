/*
 * SDGlobal.hpp
 *
 *  Created on: Oct 8, 2013
 *      Author: dqviet
 */

#ifndef SDGLOBAL_HPP_
#define SDGLOBAL_HPP_

#include <iostream>
#include <string.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/SVD>
#include <eigen3/Eigen/Geometry>

using namespace std;
using namespace Eigen;

namespace SDIms {
	typedef Vector3d Coord3;
	typedef Vector3d Vec3;
	typedef Vector2d Vec2;

	/**
	 * Soient E un espace affine, attaché à un espace vectoriel
	 * T de dimension finie, f: E->E, une application affine,
	 * u: T->T son application linéaire associée, u dans O(T),
	 * le groupe orthogonal de T. Suppose que E est orienté (une
	 * base de T définit une orientation).
	 */
	enum IsometryClasses {
		IDENTITY,
		TRANSLATION,		// u = IdT
		AXIS_ROTATION,
		VISSAGE,
		SYMMETRY,
		SYMMETRY_TRANSLATION,
		SYMMETRY_ROTATION,
		CENTRAL_SYMMETRY,
		UNKNOWN,
		FIRST = IDENTITY,
		LAST = SYMMETRY

	};

	enum IsometryType {
		DIRECT, INDIRECT
	};

	enum SDSimilarityType
	{
		NumericalSimilarity,
		TopologicalSimilarity,
		GeometricalSimilarity,
		Unknown
	};

	enum SDIsometryType
	{
		Direct,
		Indirect
	};

	enum SDIsometryClass
	{
		Invalid,
		Identity,
		DirectNoFixedPoint,
		DirectWithFixedPoints,
		IndirectNoFixedPoint,
		IndirectWithFixedPointLine,
		IndirectWithFixedPointPlane,
		FirstClass = DirectNoFixedPoint,
		LastClass = IndirectWithFixedPointPlane
	};

	enum SDCurvatureComparison
	{
		MinMax,
		Gaussian
	};

	enum SDNeighboursDirection
	{
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouthWest,
		West,
		NorthWest
	};
}

#endif /* SDGLOBAL_HPP_ */
