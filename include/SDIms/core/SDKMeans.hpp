#ifndef __SDKMEANS_HPP__
#define __SDKMEANS_HPP__

#include <vector>
#include <eigen3/Eigen/Dense>
#include <SDIms/core/Isometry.hpp>

using namespace std;
using namespace Eigen;
using namespace SDIms;

class SDKMeans {
public:
	SDKMeans();
	void		setDataPoints(vector<Feature*>& data);
	void		setIsometryClass(IsometryClasses c);
	void		run(int maxClust, int maxLoopClust = 10);
	int			getClustersNumber();
	int			getClusterPtsIndices(int iClust, vector<int>& idx);
	int			getClusterCentroidIndex(int iClust);

private:
	IsometryClasses		m_IsomClass;
	vector<Feature*> 	m_Data;
	int					m_ClusterNum, m_PtsNum;
	VectorXi			m_PtsLabels;
	VectorXi			m_CentroidsIdx;
	MatrixXd			m_CovMatrix;

	void		generateCovarianceMatrix();
	bool		getCentroids(int k, vector<int>& centroids);
	void		estimateDistPointsCentroids(vector<int>& centroids, MatrixXd& matrix);
	// http://benpfaff.org/writings/clc/shuffle.html
	void shuffle(vector<int>& array, size_t n);
};

#endif
