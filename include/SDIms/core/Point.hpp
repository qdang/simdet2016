/*
 * Point.hpp
 *
 *  Created on: Oct 8, 2013
 *      Author: dqviet
 */

#ifndef SDPOINT_HPP_
#define SDPOINT_HPP_

#include "SDIms/core/Global.hpp"
#include "SDIms/core/MathExt.hpp"

using namespace SDIms;

namespace SDIms {

	class Frame {
	public:
		Frame();
		Frame(double* normal, double* maxTangent, double* minTangent);
		Frame(Vec3& normal, Vec3& maxTangent, Vec3& minTangent);
		Frame(const Frame& other);
		Frame& operator=(const Frame& other);
		~Frame();

		Vec3& getMaxTangent() ;
		Vec3& getMinTangent() ;
		Vec3& getNormal() ;
		Matrix3d&	getMatrix() ;
		void set(double* normal, double* maxTangent, double* minTangent);
		void set(Vec3& normal, Vec3& maxtTangent, Vec3& minTangent);
		void setMaxTangent(const Vec3& maxTangent);
		void setMinTangent(const Vec3& minTangent);
		void setNormal(const Vec3& normal);
		void normalizeCoord(const Vec3& input, Vec3& output);
		string toString();
	protected:
		Vec3 	m_Normal, m_MinTangent, m_MaxTangent;
		Matrix3d	m_Matrix;
		void	updateMatrix();
	};

	class Point {
	public:
		Point();
		Point(double* coord, double* normal, double* maxTangent, double* minTangent);
		Point(Vec3& coord, Vec3& normal, Vec3& maxTangent, Vec3& minTangent);
		Point(Coord3& coord, Frame& frame);
		Point(const Point& other);
		Point& operator=(const Point& other);
		~Point();

		string toString();
		Vec3& getCoord() ;
		Frame& getFrame() ;
		void set(Vec3& coord, Vec3& normal, Vec3& maxTangent, Vec3& minTangent);
		void set(double* coord, double* normal, double* maxTangent, double* minTangent);
		void setCoord(const Vec3& coord);
		void setFrame(const Frame& frame);
		void setFrame(Vec3& normal, Vec3& maxTangent, Vec3& minTangent);

	protected:
		Vec3	m_Coord;
		Frame	m_Frame;
	};
}
#endif /* SDPOINT_HPP_ */
