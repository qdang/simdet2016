/*
 * main.cpp
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#include "qapplication.h"

#include "core/structures/SDModel.h"
#include "core/SDPipeline.h"

int main(int argc, char* argv[])
{
	// Initialiser le logger
	// How to: http://google-glog.googlecode.com/svn/trunk/doc/glog.html
	google::InitGoogleLogging("SimDet");
	FLAGS_logtostderr	= 1;		// 0: output to file, 1: output to console
	FLAGS_log_dir		= "log";	// Ouput to file in folder "log"

	// Stand-alone
	SDPipeline pipeline;
	pipeline.runMultiModels();

	return 0;
}
