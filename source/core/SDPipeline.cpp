#include "core/SDPipeline.h"

SDPipeline::SDPipeline() {
	m_Model		= NULL;
	m_Depot		= NULL;
	m_MaxSymIdx	= -1;
}

SDPipeline::~SDPipeline() {
	m_Model		= NULL;
	m_Depot		= NULL;
}

void SDPipeline::grabFaces() {
	if(m_Model) {
		int i, j;
		m_Model->m_Bbox.Destroy();
		m_Depot->m_Faces.clear();

		int brepIds[]	= {};
		//= {13};	// pour tester séparément les breps
		int nBrepIds	= sizeof(brepIds) / sizeof(int);

		if(nBrepIds == 0) {
			for(i = 0; i < (int)m_Model->m_Breps.size(); i++) {
				for(j = 0; j < (int)m_Model->m_Breps[i]->m_Faces.size(); j++)
				{
					m_Depot->m_Faces.push_back(m_Model->m_Breps[i]->m_Faces[j]);
				}
			}
		} else {
			for(i = 0; i < nBrepIds; i++) {
				if(brepIds[i] >= 0 && brepIds[i] < (int)m_Model->m_Breps.size()) {
					int facesIds[]	= {};//{28, 81};
					int nFaces	= sizeof(facesIds) / sizeof(int);
					if(nFaces == 0) {
						for(j = 0; j < (int)m_Model->m_Breps[brepIds[i]]->m_Faces.size(); j++)
						{
							m_Depot->m_Faces.push_back(m_Model->m_Breps[brepIds[i]]->m_Faces[j]);
						}
					} else {
						for(j = 0; j < nFaces; j++) {
							if(facesIds[j] >= 0 &&
									facesIds[j] < (int)m_Model->m_Breps[brepIds[i]]->m_Faces.size()) {
								m_Depot->m_Faces.push_back(m_Model->m_Breps[brepIds[i]]->m_Faces[facesIds[j]]);
							}
						}
					}
				}
			}
		}

		// find model bounding box
		for(i = 0; i < (int)m_Depot->m_Faces.size(); i++) {
			if(m_Depot->m_Faces[i]->m_BBox.IsValid()) {
				m_Model->m_Bbox	= ON_BoundingBox(m_Depot->m_Faces[i]->m_BBox.m_min,
						m_Depot->m_Faces[i]->m_BBox.m_max);
				break;
			}
		}

		for(j = i + 1; j < (int)m_Depot->m_Faces.size(); j++) {
			if(m_Depot->m_Faces[j]->m_BBox.IsValid()) {
				if(m_Model->m_Bbox.m_min.x > m_Depot->m_Faces[j]->m_BBox.m_min.x)
					m_Model->m_Bbox.m_min.x = m_Depot->m_Faces[j]->m_BBox.m_min.x;
				if(m_Model->m_Bbox.m_min.y > m_Depot->m_Faces[j]->m_BBox.m_min.y)
					m_Model->m_Bbox.m_min.y = m_Depot->m_Faces[j]->m_BBox.m_min.y;
				if(m_Model->m_Bbox.m_min.z > m_Depot->m_Faces[j]->m_BBox.m_min.z)
					m_Model->m_Bbox.m_min.z = m_Depot->m_Faces[j]->m_BBox.m_min.z;
				if(m_Model->m_Bbox.m_max.x < m_Depot->m_Faces[j]->m_BBox.m_max.x)
					m_Model->m_Bbox.m_max.x = m_Depot->m_Faces[j]->m_BBox.m_max.x;
				if(m_Model->m_Bbox.m_max.y < m_Depot->m_Faces[j]->m_BBox.m_max.y)
					m_Model->m_Bbox.m_max.y = m_Depot->m_Faces[j]->m_BBox.m_max.y;
				if(m_Model->m_Bbox.m_max.z < m_Depot->m_Faces[j]->m_BBox.m_max.z)
					m_Model->m_Bbox.m_max.z = m_Depot->m_Faces[j]->m_BBox.m_max.z;
			}
		}

		Isometry::model_BBoxDiagonal	= m_Model->m_Bbox.Diagonal().Length();
		m_Model->m_BboxDiag				= m_Model->m_Bbox.Diagonal().Length();
	}
	LOG(INFO) << "Finish grabing faces. Num faces = " << m_Depot->m_Faces.size();
}

void SDPipeline::run() {
	if(m_Model && m_Depot) {
		if(m_Depot->m_Faces.size() == 0)
			grabFaces();


	}
}

void SDPipeline::NormalizeModel() {
	if(m_Model && m_Depot && m_MaxSymIdx != -1) {
		ON_3dPoint newCenter(m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_center(0),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_center(1),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_center(2));
		ON_3dPoint newOx(m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc1(0),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc1(1),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc1(2));
		ON_3dPoint newOy(m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc2(0),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc2(1),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc2(2));
		ON_3dPoint newOz(m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc3(0),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc3(1),
				m_Depot->m_SimFaces[SYMMETRY][m_MaxSymIdx].m_pc3(2));
		ON_Xform transform;
		transform.Rotation(
				newCenter,
				newOx, newOy, newOz,
				ON_3dPoint::Origin,
				ON_3dVector::XAxis, ON_3dVector::YAxis, ON_3dVector::ZAxis
		);
		for(unsigned int i = 0; i < m_Model->m_Breps.size(); i++) {
			m_Model->m_Breps[i]->getONBrep()->Transform(transform);
		}
	}
}

void SDPipeline::runMultiModels() {
	// list of directories
	vector<string> dirs = {//"/home/dqviet/workspace/data/instruments",
			//"/home/dqviet/workspace/data/planes",
			//"/home/dqviet/workspace/data/bikes",
			"/home/dqviet/workspace/data/cars",
			"/home/dqviet/workspace/data/household",
			"/home/dqviet/workspace/data/glasses",
			"/home/dqviet/workspace/data/machinery",
			"/home/dqviet/workspace/data/boats",
			"/home/dqviet/workspace/data/animals"};
	DIR *pDIR;
	for(unsigned int iDir = 0; iDir < dirs.size(); iDir++) {
		if((pDIR = opendir(dirs[iDir].c_str()))){
			struct dirent *entry;
			while((entry = readdir(pDIR))){
				if(strstr(entry->d_name, ".3dm")) {
					string fileName(entry->d_name);
					string filePath	= dirs[iDir];
					filePath	= filePath + "/" + fileName;
					if(m_Depot) {
						if(m_Depot->m_Faces.size() > 0) {
							for(unsigned int i = 0; i < m_Depot->m_Faces.size(); i++) {
								m_Depot->m_Faces[i]	= NULL;
							}
							m_Depot->m_Faces.clear();
						}
					}
					if(m_Model) {
						delete m_Model;
					}
					if(m_Depot) {
						delete m_Depot;
					}
					LOG(INFO) << "==================================";
					LOG(INFO) << "Loading model " << filePath;

					// charger le modèle
					m_Model = new SDModel(filePath);
					// initialiser le dépot des résultats
					m_Depot	= new SDDepot();

					if(m_Model && m_Depot) {
						if(m_Depot->m_Faces.size() == 0)
							grabFaces();

						// On n'a pas optimisé les codes, il faut donc limiter le nombre
						// de faces pour le souci de performance
						if(m_Depot->m_Faces.size() > 5000) continue;
					}
					LOG(INFO) << "==================================";
				}
			}
		}
	}
}
