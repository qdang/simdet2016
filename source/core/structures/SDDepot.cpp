/*
 * SDDepot.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: dqviet
 */

#include "core/structures/SDDepot.h"

SDDepot::SDDepot() {
	// TODO Auto-generated constructor stub
	for(int i = SDIms::FIRST; i <= SDIms::LAST; i++) {
		m_Isometries.insert(pair<int, vector<SDTransformation*> >(i, vector<SDTransformation*>()));
		m_SimFaces.insert(pair<int, vector<SDFacesPair> >(i, vector<SDFacesPair>()));
	}
}

SDDepot::~SDDepot() {
	clearSamples();
	for(int i = SDIms::FIRST; i <= SDIms::LAST; i++) {
		for(unsigned int j = 0; j < m_Isometries[i].size(); j++) {
			delete m_Isometries[i][j];
		}
		m_SimFaces[i].clear();
		m_Isometries[i].clear();
	}
	m_Isometries.clear();
	m_SimFaces.clear();
	m_SimFaces.clear();

	for(unsigned int i = 0; i < m_Faces.size(); i++) {
		m_Faces[i]	= NULL;
	}
	m_Faces.clear();
}

void SDDepot::clearSamples() {
	for(unsigned int i = 0; i < m_Samples.size(); i++) {
		delete m_Samples[i];
	}
	m_Samples.clear();
	m_KeySamplesIndices.clear();
	m_SamplesGrid.~SDGrid();
}
