/*
 * SDTransformation.cpp
 *
 *  Created on: Aug 27, 2013
 *      Author: dqviet
 */

#include "core/structures/SDTransformation.h"

SDTransformation::SDTransformation()
	: SDIms::Isometry(), m_P1(NULL), m_P2(NULL) {
	m_uuid = ON_UuidFromString(QUuid::createUuid().toByteArray().constData());
}

SDTransformation::SDTransformation(SDPoint* p1, SDPoint* p2)
	: SDIms::Isometry(), m_P1(p1), m_P2(p2), m_GridPoint(NULL) {
	m_uuid = ON_UuidFromString(QUuid::createUuid().toByteArray().constData());
	this->init(p1, p2);
}

SDTransformation::SDTransformation(SDPoint* p1, SDPoint* p2, Matrix4d& mat)
	: SDIms::Isometry(), m_P1(p1), m_P2(p2), m_GridPoint(NULL) {
	m_uuid = ON_UuidFromString(QUuid::createUuid().toByteArray().constData());
	this->init(p1, p2, mat);
}

SDTransformation::SDTransformation(SDPoint* p1, SDPoint* p2, SDGrid<SDPoint>* g)
	: SDIms::Isometry(), m_P1(p1), m_P2(p2), m_ModP2(p2), m_GridPoint(g) {
	m_uuid = ON_UuidFromString(QUuid::createUuid().toByteArray().constData());
	Init();
}

SDTransformation::SDTransformation(SDPoint* p1, SDPoint* p2, vector<SDPoint>& vp1, vector<SDPoint>& vp2)
	: SDIms::Isometry(), m_P1(p1), m_P2(p2), m_ModP2(p2), m_GridPoint(NULL),
	  m_voisins_P1(vp1), m_voisins_P2(vp2) {
	m_uuid = ON_UuidFromString(QUuid::createUuid().toByteArray().constData());
	Init();
}

SDTransformation::~SDTransformation() {
	m_P1	= NULL;
	m_P2	= NULL;
	m_GridPoint	= NULL;
	m_ModP2	= NULL;
	m_P1Mask.clear();
	m_P2Mask.clear();
}

void SDTransformation::evalGLPlanePoints(double planeDiag) {
	if(m_Type == INDIRECT) {
		if(m_Feature) {
			FeatureSym* fs = dynamic_cast<FeatureSym*>(m_Feature);
			Matrix3d frame;
			frame << 1, 0, 0, 0, 1, 0, 0, 0, 1;
			double angle, maxAngle = 0.0;
			int maxIdx = -1;
			for(int i = 0; i < 3; i++) {
				angle = acos(frame.row(i).dot(fs->m_PlaneNormal));
				if(angle > maxAngle) {
					maxAngle	= angle;
					maxIdx		= i;
				}
			}
			Vec3 t1, t2;
			t1	= frame.row(maxIdx).cross(fs->m_PlaneNormal);
			t1	= fs->m_PlaneNormal.cross(t1);
			t2	= t1.cross(fs->m_PlaneNormal);

			fs->m_GLA	= fs->m_PlanePoint + planeDiag/2.0 *(t1+t2);
			fs->m_GLB	= fs->m_PlanePoint + planeDiag/2.0 *(t1-t2);
			fs->m_GLC	= fs->m_PlanePoint + planeDiag/2.0 *(-t1-t2);
			fs->m_GLD	= fs->m_PlanePoint + planeDiag/2.0 *(-t1+t2);
		}
	}
}

string SDTransformation::ToString() {
	ostringstream oss;
	return oss.str();
}

void SDTransformation::Init() {
	if(!m_P1->leftOf(*m_ModP2)) {
		m_ModP2	= m_P1;
		SDPoint* tmp	= m_P1;
		m_P1	= m_P2;
		m_P2	= tmp;
	}

	CheckFramesDirections(m_voisins_P1, m_voisins_P2, (m_GridPoint != NULL)? true: false);

	this->init(m_P1, m_ModP2);
}

void SDTransformation::setGridPoint(SDGrid<SDPoint>* g) {
	m_GridPoint	= g;
}

SDPoint& SDTransformation::getModP2() {
	return *m_ModP2;
}

void SDTransformation::setModP2(const SDPoint& modP2) {
	m_ModP2 = const_cast<SDPoint*>(&modP2);
}

SDPoint* SDTransformation::getP1() {
	return m_P1;
}

void SDTransformation::setP1(SDPoint* m_P1) {
	m_P1 = m_P1;
}

SDPoint* SDTransformation::getP2() {
	return m_P2;
}

void SDTransformation::setP2(SDPoint* p2) {
	m_P2 = p2;
}

bool SDTransformation::isValid() {
	return m_Valid;
}

void SDTransformation::setValid(bool valid) {
	m_Valid = valid;
}

ON_UUID& SDTransformation::getUuid() {
	return m_uuid;
}

void SDTransformation::setUuid(const ON_UUID& uuid) {
	m_uuid = uuid;
}

SDTransformation::SDTransformation(const SDTransformation& other) : SDIms::Isometry(other) {
	m_uuid			= other.m_uuid;
	m_P1			= other.m_P1;
	m_P2			= other.m_P2;
	m_ModP2			= other.m_ModP2;
	m_Valid			= other.m_Valid;
	m_P1Mask		= other.m_P1Mask;
	m_P2Mask		= other.m_P2Mask;
	m_voisins_P1	= other.m_voisins_P1;
	m_voisins_P2	= other.m_voisins_P2;
}

SDTransformation& SDTransformation::operator =(const SDTransformation& other) {
	SDIms::Isometry::operator=(other);
	m_uuid			= other.m_uuid;
	m_P1			= other.m_P1;
	m_P2			= other.m_P2;
	m_ModP2			= other.m_ModP2;
	m_Valid			= other.m_Valid;
	m_P1Mask		= other.m_P1Mask;
	m_P2Mask		= other.m_P2Mask;
	m_voisins_P1	= other.m_voisins_P1;
	m_voisins_P2	= other.m_voisins_P2;
	return *this;
}

void SDTransformation::CheckFramesDirections(vector<SDPoint>& voisins_P1, vector<SDPoint>& voisins_P2,
		bool fromGrid) {

	DLOG(INFO) << "Begin checkDirections" ;

	int masque[8][2] =
			{
					{-1, 0},
					{-1, 1},
					{0, 1},
					{1, 1},
					{1, 0},
					{1, -1},
					{0, -1},
					{-1, -1}
			};

	if(fromGrid) {
		int 	bi1 = m_P1->getParamProps().getBrepIndex(),
				fi1 = m_P1->getParamProps().getFaceIndex(),
				bi2 = m_ModP2->getParamProps().getBrepIndex(),
				fi2 = m_ModP2->getParamProps().getFaceIndex(),
				iP1	= m_P1->getGridId()[0],
				jP1	= m_P1->getGridId()[1],
				iP2	= m_ModP2->getGridId()[0],
				jP2	= m_ModP2->getGridId()[1];

		SDBrepGrid<SDPoint> fb1, fb2;
		SDFaceGrid<SDPoint> fg1, fg2;
		m_GridPoint->Get(bi1, fb1);
		m_GridPoint->Get(bi2, fb2);
		fb1.Get(fi1, fg1);
		fb2.Get(fi2, fg2);

		for (int i = 0; i < 8; ++i)
		{
			SDPoint m_P1v, m_P2v;
			bool result	= fg1.Get(iP1 + masque[i][0], jP1 + masque[i][1], m_P1v);
			if(!result || !m_P1v.isEvaluable())
			{
				m_P1v = *m_P1;
			}
			voisins_P1.push_back(m_P1v);
			DLOG(INFO) << "m_P1 ajoute voisin " << i ;

			result	= fg2.Get(iP2 + masque[i][0], jP2 + masque[i][1], m_P2v);
			if(!result || !m_P2v.isEvaluable())
			{
				m_P2v = *m_ModP2;
			}
			voisins_P2.push_back(m_P2v);
			DLOG(INFO) << "m_P2 ajoute voisin " << i ;
		}

		DLOG(INFO) << voisins_P1.size() << " | " << voisins_P2.size();
	}

	cd_ProjectVectorsOnPlan(voisins_P1, *m_P1);
	cd_ProjectVectorsOnPlan(voisins_P2, *m_ModP2);

	Frame frame = m_ModP2->getFrame();
	Vec3 normal = frame.getNormal(),
		 inversedMaxTangent	= frame.getMaxTangent() * -1,
		 inversedMinTangent	= frame.getMinTangent() * -1;
	SDPoint case2;
	case2.setFrame(normal, inversedMaxTangent, inversedMinTangent);
	SDPoint case3;
	case3.setFrame(normal, frame.getMaxTangent(), inversedMinTangent);
	SDPoint case4;
	case4.setFrame(normal, inversedMaxTangent, frame.getMinTangent());

	vector<Vec2> base, base1, base2, base3, base4;
	cd_GetOrthonormalBaseCoord(voisins_P1, *m_P1, base);
	cd_GetOrthonormalBaseCoord(voisins_P2, *m_ModP2, base1);
	cd_GetOrthonormalBaseCoord(voisins_P2, case2, base2);
	cd_GetOrthonormalBaseCoord(voisins_P2, case3, base3);
	cd_GetOrthonormalBaseCoord(voisins_P2, case4, base4);

	vector<Vec2> angles, angles1, angles2, angles3, angles4;
	cd_GetAnglesBetweenAxes(base, angles);
	cd_GetAnglesBetweenAxes(base1, angles1);
	cd_GetAnglesBetweenAxes(base2, angles2);
	cd_GetAnglesBetweenAxes(base3, angles3);
	cd_GetAnglesBetweenAxes(base4, angles4);

	int *ordre, *ordre1, *ordre2, *ordre3, *ordre4;
	ordre = new int[voisins_P1.size()];
	ordre1 = new int[voisins_P1.size()];
	ordre2 = new int[voisins_P1.size()];
	ordre3 = new int[voisins_P1.size()];
	ordre4 = new int[voisins_P1.size()];

	cd_GetVectorsOrderByAngles(ordre, angles);
	cd_GetVectorsOrderByAngles(ordre1, angles1);
	cd_GetVectorsOrderByAngles(ordre2, angles2);
	cd_GetVectorsOrderByAngles(ordre3, angles3);
	cd_GetVectorsOrderByAngles(ordre4, angles4);

	double *erreurs = new double[4];
	for(int i = 0; i < 4; i ++)
		erreurs[i] = 0;

	// Approche Goodman-Kruskal
	double ki1max, ki2max, ki1min, ki2min,
			kj1max, kj2max, kj1min, kj2min;
	for(unsigned int i = 0; i < voisins_P1.size() - 1; i++)
	{
		ki1max = voisins_P1[ordre[i]].getSignature().getMaxCurvature();
		ki1min = voisins_P1[ordre[i]].getSignature().getMinCurvature();
		for(unsigned int j = i + 1; j < voisins_P1.size(); j++)
		{
			kj1max = voisins_P1[ordre[j]].getSignature().getMaxCurvature();
			kj1min = voisins_P1[ordre[j]].getSignature().getMinCurvature();

			// Case 1
			ki2max = voisins_P2[ordre1[i]].getSignature().getMaxCurvature();
			ki2min = voisins_P2[ordre1[i]].getSignature().getMinCurvature();

			kj2max = voisins_P2[ordre1[j]].getSignature().getMaxCurvature();
			kj2min = voisins_P2[ordre1[j]].getSignature().getMinCurvature();

			erreurs[0]	+= ((ki1max - kj1max) *
							(ki1min - kj1min) *
							(ki2max - kj2max) *
							(ki2min - kj2min) >= 0) ? 1 : -1;

			// Case 2
			ki2max = voisins_P2[ordre2[i]].getSignature().getMaxCurvature();
			ki2min = voisins_P2[ordre2[i]].getSignature().getMinCurvature();

			kj2max = voisins_P2[ordre2[j]].getSignature().getMaxCurvature();
			kj2min = voisins_P2[ordre2[j]].getSignature().getMinCurvature();

			erreurs[1]	+= ((ki1max - kj1max) *
							(ki1min - kj1min) *
							(ki2max - kj2max) *
							(ki2min - kj2min) >= 0) ? 1 : -1;

			// Case 3
			ki2max = voisins_P2[ordre3[i]].getSignature().getMaxCurvature();
			ki2min = voisins_P2[ordre3[i]].getSignature().getMinCurvature();

			kj2max = voisins_P2[ordre3[j]].getSignature().getMaxCurvature();
			kj2min = voisins_P2[ordre3[j]].getSignature().getMinCurvature();

			erreurs[2]	+= ((ki1max - kj1max) *
							(ki1min - kj1min) *
							(ki2max - kj2max) *
							(ki2min - kj2min) >= 0) ? 1 : -1;

			// Case 4
			ki2max = voisins_P2[ordre4[i]].getSignature().getMaxCurvature();
			ki2min = voisins_P2[ordre4[i]].getSignature().getMinCurvature();

			kj2max = voisins_P2[ordre4[j]].getSignature().getMaxCurvature();
			kj2min = voisins_P2[ordre4[j]].getSignature().getMinCurvature();

			erreurs[3]	+= ((ki1max - kj1max) *
							(ki1min - kj1min) *
							(ki2max - kj2max) *
							(ki2min - kj2min) >= 0) ? 1 : -1;
		}
	}

	int iMin = 0;
	double errMin = erreurs[iMin];
	for(int i = 1; i < 4; i++)
	{
		if(errMin <= erreurs[i])		// > pour "brute force", < pour "Goodman - Kruskal"
		{
			iMin = i;
			errMin = erreurs[i];
		}
	}

	int* tmp = ordre1;
	if(iMin == 1)
	{
		//m_ModP2.getFrame().set(frame.getNormal(), inversedMaxTangent, inversedMinTangent);
		m_ModP2->setFrame(case2.getFrame());
		tmp = ordre2;
	}
	else if(iMin == 2)
	{
		//m_ModP2.getFrame().set(frame.getNormal(), frame.getMaxTangent(), inversedMinTangent);
		m_ModP2->setFrame(case3.getFrame());
		tmp = ordre3;
	}
	else if(iMin == 3)
	{
		//m_ModP2.getFrame().set(frame.getNormal(), inversedMaxTangent, frame.getMinTangent());
		m_ModP2->setFrame(case4.getFrame());
		tmp = ordre4;
	}
	DLOG(INFO) << "iMin = " << iMin;

	// Sauvegarder la masque des voisins du point origine et du point transformé
	for(int i = 0; i < 8; i++)
	{
		vector<int> orow, prow;
		for(int j = 0; j < 2; j++)
		{
			orow.push_back(masque[ordre[i]][j]);
			prow.push_back(masque[tmp[i]][j]);
		}
		m_P1Mask.push_back(orow);
		m_P2Mask.push_back(prow);
	}

	base.clear();base1.clear();base2.clear();base3.clear();base4.clear();
	angles.clear();angles1.clear();angles2.clear();angles3.clear();angles4.clear();
	delete[] ordre;
	delete[] ordre1;
	delete[] ordre2;
	delete[] ordre3;
	delete[] ordre4;
	//delete tmp;
	delete[] erreurs;
}

void SDTransformation::cd_ProjectVectorsOnPlan(vector<SDPoint> &neighbors, SDPoint& markPoint)
{
	if(neighbors.size() != 8)
		DLOG(INFO) << "cd_ProjectVectorsOnPlan co van de !!!!";
	Vec3 tmp;
	for(unsigned int i = 0; i < neighbors.size(); i++)
	{
		neighbors[i].getCoord()[0] -= markPoint.getCoord()[0];
		neighbors[i].getCoord()[1] -= markPoint.getCoord()[1];
		neighbors[i].getCoord()[2] -= markPoint.getCoord()[2];

		double length = sqrt(neighbors[i].getCoord()[0] * neighbors[i].getCoord()[0] +
				neighbors[i].getCoord()[1] * neighbors[i].getCoord()[1]
				+ neighbors[i].getCoord()[2] * neighbors[i].getCoord()[2]);
		// Normalisation
		if(length > 0)
		{
			neighbors[i].getCoord()[0] /= length;
			neighbors[i].getCoord()[1] /= length;
			neighbors[i].getCoord()[2] /= length;
		}

		tmp = neighbors[i].getCoord().cross(markPoint.getFrame().getNormal()); // p' perpendiculaire à la normale et à neighbors[i
		tmp = tmp.cross(markPoint.getFrame().getNormal()); // p'' perpendiculaire à la normale et p'
		tmp.normalize();
		neighbors[i].getCoord()[0] = tmp[0];
		neighbors[i].getCoord()[1] = tmp[1];
		neighbors[i].getCoord()[2] = tmp[2];
	}
}

/**
 * Passer les vecteurs projetés vers la base orthonormale des tangentes,
 * càd calculer les coordonnées du vecteur dans cette base
 */
void SDTransformation::cd_GetOrthonormalBaseCoord(vector<SDPoint>& neighbors, SDPoint& markPoint, vector<Vec2>& base)
{
	for(unsigned int i = 0; i < neighbors.size(); i++)
	{
		Vec2 p;
		p[0] = neighbors[i].getCoord().dot(markPoint.getFrame().getMinTangent());
		p[1] = neighbors[i].getCoord().dot(markPoint.getFrame().getMaxTangent());
		p.normalize();
		base.push_back(p);
	}
	DLOG(INFO) << "cd_GetOrthonormalBaseCoord - size(base) = " << base.size();
}

/**
 * Calculer les angles des vecteurs dans la base orthonormée par rapport à
 * l'axe Ox et à l'axe Oy
 */
void SDTransformation::cd_GetAnglesBetweenAxes(vector<Vec2>& base, vector<Vec2>& angles)
{
	Vec2 Ox, Oy;
	Ox << 1.0, 0.0;
	Oy << 0.0, 1.0;
	for(unsigned int i = 0; i < base.size(); i++)
	{
		Vec2 angle;
		angle[0] = acos(base[i].dot(Ox));
		angle[1] = acos(base[i].dot(Oy));
		angles.push_back(angle);
	}
	DLOG(INFO) << "cd_GetAnglesBetweenAxes - size(angles) = " << angles.size();
}

void SDTransformation::cd_GetVectorsOrderByAngles(int* order, vector<Vec2>& angles)
{
	int n = angles.size();

	// déterminer la direction des vecteurs projetés
	int direction = 1;
	if(angles[0][0] <= M_PI/2 && angles[0][1] <= M_PI/2)
	{
		if(angles[1][0] <= angles[0][0])
			direction = -1;
	}
	else if(angles[0][0] > M_PI/2 && angles[0][1] <= M_PI/2)
	{
		if(angles[1][1] <= angles[0][1])
			direction = -1;
	}
	else if(angles[0][0] > M_PI/2 && angles[0][1] > M_PI/2)
	{
		if(angles[1][1] <= angles[0][1])
			direction = -1;
	}
	else if(angles[0][0] <= M_PI/2 && angles[0][1] > M_PI/2)
	{
		if(angles[1][0] > angles[0][0])
			direction = -1;
	}

	// Déterminer le vecteur le plus proche des axes [0 1] et [1 0]
	double anglesSum[n];
	for(int i = 0; i < n; i++)
	{
		anglesSum[i] = angles[i][0] + angles[i][1];
	}

	int iStart = 0;
	int minSum = anglesSum[iStart];
	for(int i = 1; i < n; i++)
	{
		if(anglesSum[i] < minSum)
		{
			iStart = i;
			minSum = anglesSum[i];
		}
	}

	// Déterminer l'order
	for(int i = 0; i < n; i++)
	{
		order[i] = iStart;
		iStart = iStart + direction;
		if(iStart < 0)
			iStart = n - 1;
		else if(iStart >= n)
			iStart = 0;
	}
	DLOG(INFO) << "End of cd_GetVectorsOrderByAngles";
}

SDAffineTransformation::SDAffineTransformation() {
}

SDAffineTransformation::~SDAffineTransformation() {
}

const ON_3dVector& SDAffineTransformation::getRotation() const {
	return m_Rotation;
}

void SDAffineTransformation::setRotation(const ON_3dVector& rotation) {
	m_Rotation = rotation;
}

const ON_3dVector& SDAffineTransformation::getTranslation() const {
	return m_Translation;
}

void SDAffineTransformation::setTranslation(const ON_3dVector& translation) {
	m_Translation = translation;
}

void SDAffineTransformation::Init() {
	SDTransformation::Init();
}
