/*
 * SDBrepVertex.cpp
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#include "core/structures/SDBrepVertex.h"

SDBrepVertex::SDBrepVertex() {
	m_inSingularTrim	= false;
}

SDBrepVertex::SDBrepVertex(const SDBrepVertex& other) {
	m_vi	= other.m_vi;
	m_u		= other.m_u;
	m_v		= other.m_v;
	m_tangentsAngle	= other.m_tangentsAngle;
	m_inSingularTrim	= other.m_inSingularTrim;
}

SDBrepVertex& SDBrepVertex::operator=(const SDBrepVertex& other) {
	m_vi	= other.m_vi;
	m_u		= other.m_u;
	m_v		= other.m_v;
	m_inSingularTrim	= other.m_inSingularTrim;
	return *this;
}

bool SDBrepVertex::operator==(const SDBrepVertex& other) {
	//return (m_u	= other.m_u && m_v == other.m_v);
	return (!other.m_inSingularTrim && m_vi == other.m_vi);
}
