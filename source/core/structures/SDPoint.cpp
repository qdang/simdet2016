/*
 * SDPoint.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: dqviet
 */

#include "core/structures/SDPoint.h"

SDPoint::SDPoint() {

}

SDPoint::SDPoint(double* coord, double* normal, double* maxTangent, double* minTangent) :
	SDIms::Point(coord, normal, maxTangent, minTangent) {

}

SDPoint::~SDPoint() {

}

SDSignature::SDSignature() {
	m_MinCurvature	= m_MaxCurvature =
	m_GaussCurvature	= m_MeanCurvature = 0.0;
}

SDSignature::SDSignature(double max, double min) {
	m_MinCurvature	= min;
	m_MaxCurvature	= max;
	updateCurvatures();
}

SDSignature::SDSignature(double max, double min, double gauss, double mean) {
	m_MinCurvature	= min;
	m_MaxCurvature	= max;
	m_GaussCurvature	= gauss;
	m_MeanCurvature	= mean;
}

SDSignature::SDSignature(const SDSignature& other) {
	m_MinCurvature	= other.m_MinCurvature;
	m_MaxCurvature	= other.m_MaxCurvature;
	m_GaussCurvature	= other.m_GaussCurvature;
	m_MeanCurvature	= other.m_MeanCurvature;
}

SDSignature& SDSignature::operator =(const SDSignature& other) {
	m_MinCurvature	= other.m_MinCurvature;
	m_MaxCurvature	= other.m_MaxCurvature;
	m_GaussCurvature	= other.m_GaussCurvature;
	m_MeanCurvature	= other.m_MeanCurvature;
	return *this;
}

bool SDSignature::operator ==(const SDSignature& other) {
	return (m_MinCurvature	== other.m_MinCurvature &&
			m_MaxCurvature	== other.m_MaxCurvature &&
			m_GaussCurvature	== other.m_GaussCurvature &&
			m_MeanCurvature	== other.m_MeanCurvature);
}

double SDSignature::operator -(const SDSignature& other) {
	double 	max_ = m_MaxCurvature - other.m_MaxCurvature,
			min_ = m_MinCurvature - other.m_MinCurvature;
	return max_ * max_ + min_ * min_;
}

bool SDSignature::equalsTo(const SDSignature& other, SDCurvatureComparison method, double tolerance)
{
	if(method == SDCurvatureComparison::MinMax) {
		return (SDMaths::equal(m_MinCurvature, other.m_MinCurvature, tolerance) &&
				SDMaths::equal(m_MaxCurvature, other.m_MaxCurvature, tolerance));
	}
	else {
		return (SDMaths::equal(m_GaussCurvature, other.m_GaussCurvature, tolerance));
	}
}

double SDSignature::getCurvatureRatio() {
	if(SDMaths::equal(m_MaxCurvature, 0.0))
		return SD_UMBILIC_CURVATURE_RATIO;
	else
		return (m_MinCurvature / m_MaxCurvature);
}

void SDSignature::setMaxCurvature(double curvature) {
	m_MaxCurvature	= curvature;
	updateCurvatures();
}

const double& SDSignature::getMaxCurvature() {
	return m_MaxCurvature;
}

void SDSignature::setMinCurvature(double curvature) {
	m_MinCurvature	= curvature;
	updateCurvatures();
}

const double& SDSignature::getMinCurvature() {
	return m_MinCurvature;
}

const double& SDSignature::getGaussCurvature() {
	return m_GaussCurvature;
}

const double& SDSignature::getMeanCurvature() {
	return m_MeanCurvature;
}

void SDSignature::updateCurvatures() {
	m_GaussCurvature	= m_MaxCurvature * m_MinCurvature;
	m_MeanCurvature		= (m_MaxCurvature + m_MinCurvature) / 2.0;
}

SDParametricProperties::SDParametricProperties() {
	m_U	= m_V	= 0.0;
	m_BrepIndex	= m_FaceIndex	= -1;
}

SDParametricProperties::~SDParametricProperties() {
}

void SDParametricProperties::setU(double u) {
	m_U	= u;
}

void SDParametricProperties::setV(double v) {
	m_V	= v;
}

void SDParametricProperties::setBrepIndex(int ib) {
	m_BrepIndex	= ib;
}

void SDParametricProperties::setFaceIndex(int ifa) {
	m_FaceIndex	= ifa;
}

double SDParametricProperties::getU() {
	return m_U;
}

double SDParametricProperties::getV() {
	return m_V;
}

int SDParametricProperties::getBrepIndex() {
	return m_BrepIndex;
}

SDParametricProperties::SDParametricProperties(const SDParametricProperties& other) {
	m_BrepIndex	= other.m_BrepIndex;
	m_FaceIndex	= other.m_FaceIndex;
	m_U			= other.m_U;
	m_V			= other.m_V;
}

SDParametricProperties& SDParametricProperties::operator =(
		const SDParametricProperties& other) {
	m_BrepIndex	= other.m_BrepIndex;
	m_FaceIndex	= other.m_FaceIndex;
	m_U			= other.m_U;
	m_V			= other.m_V;
	return *this;
}

int SDParametricProperties::getFaceIndex() {
	return m_FaceIndex;
}

SDPoint::SDPoint(const SDPoint& other) : SDIms::Point(other) {
	m_ParamProps	= other.m_ParamProps;
	m_Signature		= other.m_Signature;
	m_GridId		= other.m_GridId;
	m_Evaluable		= other.m_Evaluable;
	m_Umbilic		= other.m_Umbilic;
}

SDPoint& SDPoint::operator =(const SDPoint& other) {
	SDIms::Point::operator=(other);
	m_ParamProps	= other.m_ParamProps;
	m_Signature		= other.m_Signature;
	m_GridId		= other.m_GridId;
	m_Evaluable		= other.m_Evaluable;
	m_Umbilic		= other.m_Umbilic;
	return *this;
}

SDParametricProperties& SDPoint::getParamProps() {
	return m_ParamProps;
}

void SDPoint::setParamProps(const SDParametricProperties& paramProps) {
	m_ParamProps = paramProps;
}

SDSignature& SDPoint::getSignature() {
	return m_Signature;
}

void SDPoint::setSignature(const SDSignature& signature) {
	m_Signature = signature;
}

bool SDPoint::isEvaluable() {
	return m_Evaluable;
}

void SDPoint::setEvaluable(bool evaluable) {
	m_Evaluable = evaluable;
}

bool SDPoint::isUmbilic() {
	return m_Umbilic;
}

bool SDPoint::isUmbilic(double ratio) {
	m_Umbilic	= ((m_Signature.getMinCurvature() / m_Signature.getMaxCurvature()) >= ratio);
	return m_Umbilic;
}

void SDPoint::setUmbilic(bool umbilic) {
	m_Umbilic = umbilic;
}

bool SDPoint::leftOf(SDPoint& other) {
	if(m_ParamProps.getBrepIndex() == other.getParamProps().getBrepIndex())
	{
		if(m_ParamProps.getFaceIndex() == other.getParamProps().getFaceIndex())
		{
			if(m_ParamProps.getU() == other.getParamProps().getU())
			{
				return (m_ParamProps.getV() - other.getParamProps().getV() < 0);
			}
			else
			{
				return (m_ParamProps.getU() - other.getParamProps().getU() < 0);
			}
		}
		else
		{
			return (m_ParamProps.getFaceIndex() - other.getParamProps().getFaceIndex() < 0);
		}
	}
	else
	{
		return (m_ParamProps.getBrepIndex() - other.getParamProps().getBrepIndex() < 0);
	}
}

Vec2& SDPoint::getGridId() {
	return m_GridId;
}

void SDPoint::setGridId(int x, int y) {
	m_GridId[0]	= x;
	m_GridId[1]	= y;
}

double SDPoint::distanceTo(SDPoint& other) {
	return (m_Coord - other.m_Coord).norm();
}

string SDPoint::ToString() {
	ostringstream oss;
	//oss << "(" << m_Coord[0] << ", " << m_Coord[1] << ", " << m_Coord[2] << ") - " << m_Signature.ToString();
	oss << SDIms::Point::toString();
	return oss.str();
}
