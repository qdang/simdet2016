/*
 * SDModel.cpp
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#include "core/structures/SDModel.h"

SDModel::SDModel() : ONX_Model() {

}

ON_BoundingBox SDModel::m_Bbox 	= ON_BoundingBox();
double SDModel::m_BboxDiag		= 1;

SDModel::SDModel(SDParameters* param) : ONX_Model() {
	if(param) {
		m_Params	= param;
		SDModel(param->m_Sampling_ModelFilePath);
	}
}

SDModel::SDModel(string filePath) : ONX_Model() {
	FILE *fp 	= ON::OpenFile(filePath.c_str(), "rb");
	if(fp)
	{
		if(this->Read(filePath.c_str())) {
			LOG(INFO) << "Chargement du modèle depuis : " << filePath;
			m_Bbox		= BoundingBox();
			m_BboxDiag	= m_Bbox.Diagonal().Length();

			// Sélection des surfaces de type Brep.
			int sdBrepIndex = 0;
			for (int i = 0; i < m_object_table.Count(); i++)
			{
				//ON_Object *pObject = const_cast<ON_Object*>(this->m_object_table[i].m_object);
				//ON_Brep* Brep = ON_Brep::Cast(pObject);
				ONX_Model_Object model_object = m_object_table[i];
				const ON_Brep* Brep = ON_Brep::Cast( model_object.m_object );

				if (Brep)
				{
					//LOG(INFO) << "Brep trouvée à l'indice: " << i;

					/*
					Orientation of ON_Brep faces:
					The "UV" orientation of surfaces in a Brep is arbitrary.  If the BOOL ON_BrepFace::m_bRev
					member is FALSE, then the face's orientation agrees with thes urface's natural Du X Dv orientation.
					When the member is TRUE, the face's orientation is opposite the surface's natural Du X Dv
					orientation.
					If your application cannot handle ON_BrepFaces that have a
					TRUE m_bRev flag, then call ON_Brep::FlipReversedSurfaces().
					See the comments in ON_Brep::FlipReversedSurfaces() and
					ON_Brep::SwapFaceParameters() for details.
					*/

					// TODO: à régler la variable Brep car elle est const
					/*Brep->FlipReversedSurfaces();
					Brep->Standardize();
					Brep->Compact();*/
					SDBrep* sdBrep	= new SDBrep(const_cast<ON_Brep*>(Brep));
					sdBrep->setBrepIndex(sdBrepIndex);
					m_Breps[sdBrepIndex] = sdBrep;
					sdBrepIndex++;
				}
			}
			LOG(INFO) << "Nombre de B-Rep du modèle : " << sdBrepIndex - 1;
		} else {
			LOG(INFO) << "Erreur lors du chargement du modèle";
		}
		ON::CloseFile(fp);
	}
	else
	{
		LOG(INFO) << "Fichier 3dm non trouvé : " << filePath;
	}
}

SDModel::~SDModel() {
	for(unsigned int i = 0; i < m_Breps.size(); i++) {
		delete m_Breps[i];
	}
	m_Breps.clear();
	m_Bbox.~ON_BoundingBox();
	m_View.~ON_3dmView();
	m_Params	= NULL;
	Destroy();
	//LOG(INFO) << "Détruire SDModel";
}



void SDModel::outputControlPointsOfPlane() {
	ON_Brep* brep = m_Breps[0]->getONBrep();
	ON_BrepFace* face = brep->Face(3);
	ON_NurbsSurface* srf	= face->NurbsSurface();
	if(srf) {
		LOG(INFO) << "Knot count = " << srf->KnotCount(0) << "-" << srf->KnotCount(1);
		LOG(INFO) << "CV count = " << srf->CVCount(0) << " - " << srf->CVCount(1);
		int dim = (srf->IsRational()) ? srf->m_dim : srf->m_dim - 1;
		for(int iu = 0; iu < srf->CVCount(0); iu++) {
			for(int iv = 0; iv < srf->CVCount(1); iv++) {
				double* cv = srf->CV(iu, iv);
				ostringstream oss;
				for(int j = 0; j < dim; j++) {
					oss << cv[j] << ", ";
				}
				oss << cv[dim] << ";";
				LOG(INFO) << oss.str();
			}
		}
	}
	brep = m_Breps[1]->getONBrep();
	face = brep->Face(3);
	srf	= face->NurbsSurface();
	if(srf) {
		LOG(INFO) << "Knot count = " << srf->KnotCount(0) << "-" << srf->KnotCount(1);
		LOG(INFO) << "CV count = " << srf->CVCount(0) << " - " << srf->CVCount(1);
		int dim = (srf->IsRational()) ? srf->m_dim : srf->m_dim - 1;
		for(int iu = 0; iu < srf->CVCount(0); iu++) {
			for(int iv = 0; iv < srf->CVCount(1); iv++) {
				double* cv = srf->CV(iu, iv);
				ostringstream oss;
				for(int j = 0; j < dim; j++) {
					oss << cv[j] << ", ";
				}
				oss << cv[dim] << ";";
				LOG(INFO) << oss.str();
			}
		}
	}
}

bool SDModel::PointAt(int brep_index, int face_index, double u, double v, SDPoint* p, bool calculProperties)
{
	return m_Breps.at(brep_index)->getSDFace(face_index)->PointAt(u, v, p, calculProperties);
}

SDFaceChild::SDFaceChild() {
	m_Pos	= WEST_OR_NORTH;
	m_Depth	= 0;
	m_Face	= NULL;
}

SDFaceChild::SDFaceChild(const SDFaceChild& other) {
	m_Pos	= other.m_Pos;
	m_Depth	= other.m_Depth;
	m_Face	= other.m_Face;
}

SDFaceChild& SDFaceChild::operator =(const SDFaceChild& other) {
	m_Pos	= other.m_Pos;
	m_Depth	= other.m_Depth;
	m_Face	= other.m_Face;
	return *this;
}

SDDelaunayTriangle::SDDelaunayTriangle(int a, int b, int c) {
	m_vertices.push_back(a);
	m_vertices.push_back(b);
	m_vertices.push_back(c);
	int i, j, tmp;
	for(i = 0; i < 2; i++) {
		for(j = i + 1; j < 3; j++) {
			if(m_vertices[i] >= m_vertices[j]) {
				tmp				= m_vertices[i];
				m_vertices[i]	= m_vertices[j];
				m_vertices[j]	= tmp;
			}
		}
	}

	m_check	= false;
}

SDDelaunayTriangle::SDDelaunayTriangle(const SDDelaunayTriangle& other) {
	m_vertices	= other.m_vertices;
	m_check		= other.m_check;
}

SDDelaunayTriangle& SDDelaunayTriangle::operator=(const SDDelaunayTriangle& other) {
	m_vertices	= other.m_vertices;
	m_check		= other.m_check;
	return *this;
}

bool SDDelaunayTriangle::operator<(const SDDelaunayTriangle& other) {
	if(m_vertices[0] < other.m_vertices[0]) {
		return true;
	} else if (m_vertices[0] == other.m_vertices[0]) {
		if(m_vertices[1] < other.m_vertices[1]) {
			return true;
		} else if (m_vertices[1] == other.m_vertices[1]) {
			return (m_vertices[2] <= other.m_vertices[2]);
		} else {
			return false;
		}

	} else {
		return false;
	}
}

bool SDDelaunayTriangle::inTriangle(int inA, int& outB, int& outC) {
	bool found	= false;
	int i;
	for(i = 0; i < 3; i++) {
		if(m_vertices[i] == inA) {
			found = true;
			break;
		}
	}

	if(found) {
		int i2 = (i + 1) % 3, i3 = (i + 2) % 3;
		if(m_vertices[i2] <= m_vertices[i3]) {
			outB	= m_vertices[i2];
			outC	= m_vertices[i3];
		} else {
			outB	= m_vertices[i3];
			outC	= m_vertices[i2];
		}
	}

	return found;
}
