/*
 * SDFace.cpp
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#include "core/structures/SDFace.h"

SDFace::SDFace() {
	m_Brep		= NULL;
	m_Face		= NULL;
	m_IsSamplingUniform	= false;
	m_ParamRefineStep	= 10;
	m_ONFaceIndex 		= -1;
}

SDFace::SDFace(ON_BrepFace* face) {
	setONBrepFace(face);
}

SDFace::~SDFace() {
	m_Brep		= NULL;
	m_Face		= NULL;
	m_Corners.clear();
	m_CornersOF.clear();
	m_PointsOnFace.clear();
	m_SDPointsOnFace.clear();
	m_CornersPos.clear();
	m_vertIdxList.clear();
	m_CornersLoop.clear();

	m_SimTypes.clear();
	for(unsigned int i = 0; i < m_SimFaces.size(); i++) {
		m_SimFaces[i]	= NULL;
	}
	m_SimFaces.clear();
}

SDFace::SDFace(const SDFace& other) {
	m_Brep			= other.m_Brep;
	m_Face			= other.m_Face;
	m_Corners		= other.m_Corners;
	m_CornersOF		= other.m_CornersOF;
	m_FaceIndex		= other.m_FaceIndex;
	m_vertIdxList	= other.m_vertIdxList;
	m_CurrentTransf	= other.m_CurrentTransf;
	m_CornersLoop	= other.m_CornersLoop;
	m_ONFaceIndex	= other.m_ONFaceIndex;
	m_SimFaces		= other.m_SimFaces;
	m_SimTypes		= other.m_SimTypes;
	m_BBDiagonal	= other.m_BBDiagonal;
	m_BBox			= other.m_BBox;
	m_PointsOnFace	= other.m_PointsOnFace;
	m_ParamRefineStep = other.m_ParamRefineStep;
}

SDFace& SDFace::operator =(const SDFace& other) {
	m_Brep			= other.m_Brep;
	m_Face			= other.m_Face;
	m_Corners		= other.m_Corners;
	m_CornersOF		= other.m_CornersOF;
	m_FaceIndex		= other.m_FaceIndex;
	m_vertIdxList	= other.m_vertIdxList;
	m_CurrentTransf	= other.m_CurrentTransf;
	m_CornersLoop	= other.m_CornersLoop;
	m_ONFaceIndex	= other.m_ONFaceIndex;
	m_SimFaces		= other.m_SimFaces;
	m_SimTypes		= other.m_SimTypes;
	m_BBDiagonal	= other.m_BBDiagonal;
	m_BBox			= other.m_BBox;
	m_PointsOnFace	= other.m_PointsOnFace;
	m_ParamRefineStep = other.m_ParamRefineStep;
	return *this;
}

ON_BrepFace* SDFace::getONBrepFace() {
	return m_Face;
}

void SDFace::setSDBrep(SDBrep* brep) {
	m_Brep	= brep;
}

SDBrep* SDFace::getSDBrep() {
	return m_Brep;
}

void SDFace::setONBrepFace(ON_BrepFace* face) {
	m_Face	= face;
	m_ONFaceIndex	= face->m_face_index;
	estimateCorners();
	estimateBoundingBox();
}

QUuid SDFace::getUUID() {
	ON_String str;
	ON_UuidToString(m_Face->ModelObjectId(), str);
	return QUuid(str.Array());
}

string SDFace::OriginalFacePos() {
	ostringstream oss;
	oss << "(" << m_Brep->getBrepIndex() << ", " << m_Face->m_face_index << ")";
	return oss.str();
}

bool SDFace::PointAt(double u, double v, SDPoint* point, bool calculProperties)
{
	ON_3dPoint puv;
	ON_3dVector du, dv, duu, duv, dvv;
	const ON_Surface* srf = m_Face->SurfaceOf();
	//DLOG(INFO) << "Surface degrees = " << srf->Degree(0) << " , " << srf->Degree(1);

	if(srf->Ev2Der(u, v, puv, du, dv, duu, duv, dvv))
	{
		double gauss, mean, kappa1, kappa2;
		//SDIms::Vec3 N, K1, K2;
		SDIms::Vec3 p;
		p << puv.x, puv.y, puv.z;
		point->getParamProps().setU(u);
		point->getParamProps().setV(v);
		if(!calculProperties) {
			point->setCoord(p);
			point->setEvaluable(true);
		} else {
			// output K1,K2,N is right handed frame
			ON_3dVector N, K1, K2;
			srf->EvNormal(u, v, N);
			//if(m_Face->m_bRev) N.Reverse();
			bool isEvaluated = ON_EvPrincipalCurvatures(du, dv, duu, duv, dvv, N,
					&gauss, &mean, &kappa1, &kappa2, K1, K2);
			//bool isEvaluated = SDModel::MyPropertiesEvaluation(du, dv, duu, duv, dvv, m_Face->m_bRev,
			//				N, K1, K2, kappa1, kappa2, gauss, mean);

			point->setEvaluable(isEvaluated);
			if(isEvaluated)
			{
				//point->set(p.data(), N.data(), K1.data(), K2.data());
				point->set(p.data(),
						SDIms::Vec3(N.x,N.y,N.z).data(),
						SDIms::Vec3(K1.x,K1.y,K1.z).data(),
						SDIms::Vec3(K2.x,K2.y,K2.z).data());
				point->setSignature(SDSignature(kappa1, kappa2, gauss, mean));
			}
			else
			{
				//DLOG(INFO) << "Could not evaluate principal curvatures";
				return false;
			}
		}

		return true;
	}
	else
	{
		//DLOG(INFO) << "Could not evaluate 2nd derivative";
		return false;
	}
}

void SDFace::getCornerVerticesIndices() {
	if(!m_Face)
		return;
	ON_Brep* brep	= m_Face->Brep();
	for(int li = 0; li < m_Face->m_li.Count(); li++) {
		ON_BrepLoop* loop = m_Face->Loop(li);
		if(loop->m_type == ON_BrepLoop::outer) {
			ON_2dPoint uv;
			SDPoint point;
			for(int ti = 0; ti < loop->TrimCount(); ti++) {
				ON_BrepTrim* trim = loop->Trim(ti);
				SDBrepVertex bv0, bv1;
				uv = brep->m_C2[trim->m_c2i]->PointAtStart();
				bv0.m_vi 	= trim->m_vi[0];
				bv0.m_u		= uv.x;
				bv0.m_v		= uv.y;

				ON_SimpleArray<int>& tmpE =brep->m_V[trim->m_vi[0]].m_ei;
				if(tmpE.SizeOfArray() == 2) {

				}

				uv = brep->m_C2[trim->m_c2i]->PointAtEnd();
				bv1.m_vi 	= trim->m_vi[1];
				bv1.m_u		= uv.x;
				bv1.m_v		= uv.y;
				bv1.m_inSingularTrim	= (trim->m_vi[0] == trim->m_vi[1]);//(trim->m_type == ON_BrepTrim::singular);

				m_vertIdxList.push_back(bv0);
				m_vertIdxList.push_back(bv1);
			}

			// supprimmer les indices répétées
			vector<SDBrepVertex>::iterator it, itt;
			for(it = m_vertIdxList.begin(); it != m_vertIdxList.end(); it++) {
				for(itt = it + 1; itt !=m_vertIdxList.end(); itt++) {
					if(*it == *itt){
						m_vertIdxList.erase(itt);
						itt = it;
					}
				}
			}
			return; // considérer seulement 1 loop "outer"
		}
	}
}

void SDFace::setFaceIndex(int& fi) {
	m_FaceIndex	= fi;
}

int SDFace::getFaceIndex() {
	return m_FaceIndex;
}

bool SDFace::getNeighborFace(SDBrepVertex& v1, SDBrepVertex& v2, SDFace*& face) {
	ON_Brep* brep	= m_Brep->getONBrep();
	ON_SimpleArray<int> v1EiList	= brep->Vertex(v1.m_vi)->m_ei,
			v2EiList	= brep->Vertex(v2.m_vi)->m_ei;

	face	= NULL;
	int iCommonEdge = -1;
	// chercher le Edge commun de deux coins
	for(int i = 0; i < v1EiList.Count(); i++) {
		iCommonEdge = v2EiList.Search(v1EiList[i]);
		if(iCommonEdge != -1) break;
	}

	if(iCommonEdge != -1) {
		ON_BrepEdge* edge = brep->Edge(v2EiList[iCommonEdge]);
		if(edge->m_ti.Count() == 1) return false;
		for(int i = 0; i < edge->m_ti.Count(); i++) {
			ON_BrepTrim* trim = brep->Trim(edge->m_ti[i]);
			ON_BrepLoop* loop = brep->Loop(trim->m_li);
			if(loop->m_fi != m_ONFaceIndex) {
				//cout << "This face index = " << m_ONFaceIndex << endl;
				//cout << "Neighbor face index = " << loop->m_fi << endl;
				return m_Brep->getSDFace(loop->m_fi, face);
			}
		}
		return false;
	} else {
		return false;
	}
}

void SDFace::estimateCorners() {
	if(!m_Face)
		return;

	getCornerVerticesIndices();

	Vec3 center, tVec;
	vector<SDPoint> tmp;
	for(unsigned int i = 0; i < m_vertIdxList.size(); i++) {
		SDPoint sdp;
		PointAt(m_vertIdxList[i].m_u, m_vertIdxList[i].m_v, &sdp, false);
		tVec	= sdp.getCoord();
		for(int j = 0; j < 3; j++)
		{
			double z = 0.0;
			double coord = tVec(j);
			if(MathExt::equal(coord, z))
			{
				tVec(j)	= 0.0;
			}
		}
		sdp.setCoord(tVec);
		tmp.push_back(sdp);
	}

	/*// Enlever les points répétés
	m_Corners.push_back(tmp[0]);
	for(unsigned int i = 1; i < tmp.size() - 1; i++) {
		if(!tmp[i].getCoord().isApprox(tmp[i-1].getCoord(), 1e-6)) {
			m_Corners.push_back(tmp[i]);
		}
	}
	if(!tmp.back().getCoord().isApprox(tmp.front().getCoord(), 1e-6)
		&& !tmp.back().getCoord().isApprox(tmp[tmp.size() - 2].getCoord(), 1e-6)) {
		m_Corners.push_back(tmp.back());
	}*/

	for(unsigned int i = 0; i < tmp.size(); i++) {
		m_Corners.push_back(&tmp[i]);
	}

	center.Zero();
	for(unsigned int i = 0; i < m_Corners.size(); i++) {
		center += m_Corners[i]->getCoord();
		m_CornersLoop.push_back(i);
		m_CornersOF.push_back(m_Corners[i]);
	}

	center /= m_vertIdxList.size();
	m_CornersCenter->setCoord(center);
}

bool SDFace::updateCornersFrame(vector<int>* cornerOrders) {
	if(m_Corners.size() <= 2)
		return false;
	Vec3	maxAxis, minAxis, norAxis;
	if(!cornerOrders) {
		maxAxis	= m_Corners[1]->getCoord() - m_Corners[0]->getCoord();
		maxAxis.normalize();
		minAxis	= m_Corners[m_Corners.size() - 1]->getCoord() - m_Corners[0]->getCoord();
		minAxis.normalize();
	} else {
		maxAxis	= m_Corners[cornerOrders->at(1)]->getCoord()
				- m_Corners[cornerOrders->at(0)]->getCoord();
		maxAxis.normalize();
		minAxis	= m_Corners[cornerOrders->at(m_Corners.size() - 1)]->getCoord()
				- m_Corners[cornerOrders->at(0)]->getCoord();
		minAxis.normalize();
	}

	norAxis	= maxAxis.cross(minAxis);
	m_CornersFrame.set(norAxis, maxAxis, minAxis);

	return true;
}

void SDFace::getLoopUniformParamsDistPoly(
			ON_BrepLoop* loop,
			vector<Vector2d>& poly) {
	poly.clear();
	ON_3dPoint uv1;
	Vector2d v2d;
	ON_Brep* brep	= m_Face->Brep();
	for(int ti = 0; ti < loop->TrimCount(); ti++) {
		ON_BrepTrim* trim 	= loop->Trim(ti);
		ON_Curve* curve		= brep->m_C2[trim->m_c2i];
		double 	t0, t1;
		curve->GetDomain(&t0, &t1);
		double 	dt 	= (t1-t0) / (m_ParamRefineStep - 1),	// A REGLER
				t	= t0;

		while(t < t1) {
			curve->EvPoint(t, uv1);
			v2d << uv1.x, uv1.y;
			poly.push_back(v2d);
			t	+= dt;
			//cout << "(" << uv1.x << "," << uv1.y << ")" << endl;
		}

		if(t != t1) {
			curve->EvPoint(t1, uv1);
			v2d << uv1.x, uv1.y;
			poly.push_back(v2d);
			//cout << "(" << uv1.x << "," << uv1.y << ")" << endl;
		}

		//cout << endl;
	}
}

void SDFace::getLoopUniformParamsPoly(
			ON_BrepLoop* loop,
			const double& minDist,
			const double& tolerance,
			vector<Vector2d>& poly) {
	poly.clear();
	ON_3dPoint uv1;
	SDPoint sdp1, sdp2;
	Vector2d v2d;
	ON_Brep* brep	= m_Face->Brep();
	for(int ti = 0; ti < loop->TrimCount(); ti++) {
		ON_BrepTrim* trim 	= loop->Trim(ti);
		ON_Curve* curve		= brep->m_C2[trim->m_c2i];
		double 	t0, t1;
		curve->GetDomain(&t0, &t1);
		double 	dt 	= (t1-t0) / 2.0,	// A REGLER
				t	= t0,
				dist = 0.0;
		curve->EvPoint(t, uv1);
		v2d << uv1.x, uv1.y;
		poly.push_back(v2d);
		PointAt(uv1.x, uv1.y, &sdp1, false);
		//m_CornersPos.push_back(sdp1.getCoord());

		while(t <= t1) {
			if(t + dt <= t1) {
				curve->EvPoint(t + dt, uv1);

				PointAt(uv1.x, uv1.y, &sdp2, false);
				dist	= (sdp2.getCoord() - sdp1.getCoord()).norm();
				if(dist < minDist - tolerance) {
					dt	*= 1.1;
				} else if(dist > minDist + tolerance){
					dt	*= 0.9;
				} else {
					v2d << uv1.x, uv1.y;
					poly.push_back(v2d);
					t	+= dt;
					cout << "(" << uv1.x << "," << uv1.y << ")" << endl;
					//m_CornersPos.push_back(sdp2.getCoord());
					sdp1.setCoord(sdp2.getCoord());
				}
			} else {
				curve->EvPoint(t1, uv1);
				v2d << uv1.x, uv1.y;
				cout << "(" << uv1.x << "," << uv1.y << ")" << endl;
				poly.push_back(v2d);
				PointAt(uv1.x, uv1.y, &sdp1, false);
				//m_CornersPos.push_back(sdp1.getCoord());
				break;
			}
		}
	}
}

SDPoint SDFace::getCornersCenter(vector<int>* cornerOrd) {
	vector<Vec3> corners;
	if(cornerOrd == NULL) {
		for(unsigned int i = 0; i < m_Corners.size(); i++) {
			corners.push_back(m_Corners[i]->getCoord());
		}
	} else {
		for(unsigned int i = 0; i < cornerOrd->size(); i++) {
			corners.push_back(m_Corners[cornerOrd->at(i)]->getCoord());
		}
	}

	Vec3 center;
	SDPoint sdCenter;
	if(corners.size() > 0) {
		center = corners[0] / corners.size();
		for(unsigned int i = 1; i < corners.size(); i++) {
			center += corners[i] / corners.size();
		}
	} else {
		center << NAN, NAN, NAN;
	}

	sdCenter.setCoord(center);

	return sdCenter;
}

void SDFace::generateUniformSamples() {
	if(!m_Face)
		return;

	ON_Interval uInt	= getONBrepFace()->SurfaceOf()->Domain(0);
	ON_Interval vInt	= getONBrepFace()->SurfaceOf()->Domain(1);

	double 	u	= uInt.Min(),
			v	= vInt.Min(),
			du	= uInt.Length() / 29.0,
			dv	= vInt.Length() / 29.0;
	m_PointsOnFace.clear();

	SDPoint point;
	for(u = uInt.Min(); u <= uInt.Max(); u+=du) {
		for(v = vInt.Min(); v <= vInt.Max(); v+=dv) {
			if(PointAt(u, v, &point, false)) {
				m_PointsOnFace.push_back(point.getCoord());
			}
		}
		if(PointAt(u, vInt.Max(), &point, false)) {
			m_PointsOnFace.push_back(point.getCoord());
		}
	}
	u	= uInt.Max();
	for(v = vInt.Min(); v <= vInt.Max(); v+=dv) {
		if(PointAt(u, v, &point, false)) {
			m_PointsOnFace.push_back(point.getCoord());
		}
	}
	if(PointAt(u, vInt.Max(), &point, false)) {
		m_PointsOnFace.push_back(point.getCoord());
	}
	m_IsSamplingUniform	= false;
}

void SDFace::generateSamplesInFace() {
	if(!m_Face)
		return;

	// get the convex hullOuter of 2d parameters region
	//cout << "Generating 2d region" << endl;
	vector<Vector2d>			polyOuter;	// take only 1 outer loop
	vector<vector<Vector2d> >	polyInner;	// There may be more than one inner loop
	vector<SDConvexHull>		hullInner;

	for(int li = 0; li < m_Face->m_li.Count(); li++) {
		ON_BrepLoop* loop = m_Face->Loop(li);
		if(loop->m_type == ON_BrepLoop::outer) {
			//cout << "outer loop" << endl;
			getLoopUniformParamsDistPoly(loop, polyOuter);
		} else if(loop->m_type == ON_BrepLoop::inner) {
			//cout << "inner loop" << endl;
			polyInner.push_back(vector<Vector2d>());
			getLoopUniformParamsDistPoly(loop, polyInner.back());
			hullInner.push_back(SDConvexHull());
			hullInner.back().init(polyInner.back(), false);
		}
	}

	SDConvexHull hullOuter;
	hullOuter.init(polyOuter, false);

	ON_Interval uInt	= getONBrepFace()->SurfaceOf()->Domain(0);
	ON_Interval vInt	= getONBrepFace()->SurfaceOf()->Domain(1);

	double 	u	= uInt.Min(),
			v	= vInt.Min(),
			du	= uInt.Length() / 29.0,
			dv	= vInt.Length() / 29.0;

	SDPoint point;
	for(u = uInt.Min(); u < uInt.Max(); u+=du) {
		for(v = vInt.Min(); v < vInt.Max(); v+=dv) {
			if(IsParamInsideParametricDomain(u,v,hullOuter,hullInner)) {
				if(PointAt(u, v, &point, false)) {
					m_PointsOnFace.push_back(point.getCoord());
				}
			}
		}
		v = vInt.Max();
		if(IsParamInsideParametricDomain(u,v,hullOuter,hullInner)) {
			if(PointAt(u, v, &point, false)) {
				m_PointsOnFace.push_back(point.getCoord());
			}
		}
	}
	u	= uInt.Max();
	for(v = vInt.Min(); v < vInt.Max(); v+=dv) {
		if(IsParamInsideParametricDomain(u,v,hullOuter,hullInner))
		{
			if(PointAt(u, v, &point, false)) {
				m_PointsOnFace.push_back(point.getCoord());
			}
		}
	}
	v = vInt.Max();
	if(IsParamInsideParametricDomain(u,v,hullOuter,hullInner))
	{
		if(PointAt(u, vInt.Max(), &point, false)) {
			m_PointsOnFace.push_back(point.getCoord());
		}
	}
	m_IsSamplingUniform	= false;
}

void SDFace::generateUniformDistanceSamples() {
	if(!m_Face)
		return;

	if(!m_IsSamplingUniform)
	{
		m_PointsOnFace.clear();
		m_IsSamplingUniform	= true;
	}
	else if(m_PointsOnFace.size() > 0)
	{
		return;
	}

	double 	minPtsDist		= SDModel::m_BboxDiag / 300.0,
			minPtsDisTol	= minPtsDist / 10.0,
			//minBorDist , minBorDisTol,
			dist	= 0.0;
	// get the convex hullOuter of 2d parameters region
	//cout << "Generating 2d region" << endl;
	vector<Vector2d>			polyOuter;	// take only 1 outer loop
	vector<vector<Vector2d> >	polyInner;	// There may be more than one inner loop
	vector<SDConvexHull>		hullInner;

	for(int li = 0; li < m_Face->m_li.Count(); li++) {
		ON_BrepLoop* loop = m_Face->Loop(li);
		if(loop->m_type == ON_BrepLoop::outer) {
			//minBorDist		= 5 * minPtsDist;
			//minBorDisTol	= 5 * minPtsDisTol;
			getLoopUniformParamsDistPoly(loop, polyOuter);
		} else if(loop->m_type == ON_BrepLoop::inner) {
			polyInner.push_back(vector<Vector2d>());
			//minBorDist		= 2 *minPtsDist;
			//minBorDisTol	= 2 * minPtsDisTol;
			getLoopUniformParamsDistPoly(loop, polyInner.back());
			hullInner.push_back(SDConvexHull());
			hullInner.back().init(polyInner.back(), false);
		}
	}

	SDConvexHull hullOuter;
	hullOuter.init(polyOuter, false);

	SDPoint point1, point2;

	// get u, v parameters
	ON_Interval uInt	= getONBrepFace()->Domain(0);
	ON_Interval vInt	= getONBrepFace()->Domain(1);

	double 	u	= uInt.Min(),
			v	= vInt.Min(),
			du	= uInt.Length() / 50.0,
			dv	= vInt.Length() / 50.0;

	// generate a grid of u and v parameters
	// The main idea is to look at the points distribution
	// for each u and v direction
	vector<double> uList, vList;

	// estimate parameters u
	/*cout << "(" << uInt.Min() << ", " << uInt.Max() << ") | ( "
			<< vInt.Min() << ", " << vInt.Max() << ")" << endl;
	cout << "Estimating u list" << endl;*/
	u	= uInt.Min();
	v	= vInt.Mid();
	uList.push_back(u);
	while(u <= uInt.Max()) {
		if(PointAt(u, v, &point1, false)) {
			//cout << "point1 = " << point1.getCoord().transpose() << endl;
			while(true) {
				if(u + du <= uInt.Max()) {
					PointAt(u + du, v, &point2, false);
					//cout << "point2 = " << point2.getCoord().transpose() << endl;
					dist = (point1.getCoord() - point2.getCoord()).norm();
					if(dist < minPtsDist - minPtsDisTol) {
						du	*=1.5;
					} else if(dist > minPtsDist + minPtsDisTol){
						du	*= 0.9;
					} else {
						u	+= du;
						uList.push_back(u);
						break;
					}
				} else { // u pass u.max
					uList.push_back(uInt.Max());
					u	= uInt.Max() + 1; // to break the outer loop
					break;
				}
			}
		} else {	// u is outside of parameter range
			uList.push_back(uInt.Max());
			u	= uInt.Max() + 1;
		}
	}

	// estimate parameters v
	u	= uInt.Mid();
	v	= vInt.Min();
	vList.push_back(v);
	//cout << "Estimating v list" << endl;
	while(v <= vInt.Max()) {
		if(PointAt(u, v, &point1, false)) {
			while(true) {
				if(v + dv < vInt.Max()) {
					PointAt(u, v + dv, &point2, false);
					dist = (point1.getCoord() - point2.getCoord()).norm();
					if(dist < minPtsDist - minPtsDisTol) {
						dv	*= 1.5;
					} else if(dist > minPtsDist + minPtsDisTol){
						dv	*= 0.9;
					} else {
						v	+= dv;
						vList.push_back(v);
						break;
					}
				} else { // v passees v.max
					vList.push_back(vInt.Max());
					v	= vInt.Max() + 1; // to break the outer loop
					break;
				}
			}
		} else {
			vList.push_back(vInt.Max());
			v	= vInt.Max() + 1;
		}
	}

	// generate points
	/*cout << "Hull corners = " << hullOuter.m_Hull.size() << endl;
	for(unsigned int i = 0; i < hullOuter.m_Hull.size(); i++) {
		cout << hullOuter.m_Hull[i](0) << " , " << hullOuter.m_Hull[i](1) << endl;
	}*/
	//cout << "Evaluating points." << endl;
	bool added	= false;
	unsigned int i, j, k;
	for(i = 0; i < uList.size(); i++) {
		for(j = 0; j < vList.size(); j++) {
			//cout << uList[i] << " | " << uList[j] << endl;
			// Test if point can be added
			added = hullOuter.isPointInside(uList[i], vList[j]);
			if(added) { // point outside of outer loop is also outside of inner loops
				for(k = 0; k < hullInner.size(); k++) {
					added = !hullInner[k].isPointInside(uList[i], vList[j]);
					if(!added) break;
				}
			}

			if(added){
				if(PointAt(uList[i], vList[j], &point1, false)) {
					m_PointsOnFace.push_back(point1.getCoord());
					//m_SDPointsOnFace.push_back(point1);
				}
			}
		}
	}

	// Exceptional: face is too small (< min points distance)
	// Ensure that the bounding box is also determined
	// by face corners
	for(i = 0; i < m_Corners.size(); i++) {
		m_PointsOnFace.push_back(m_Corners[i]->getCoord());
	}
	//cout << "Points num = " << m_PointsOnFace.size() << endl;
}

bool SDFace::IsParamInsideParametricDomain(double &u, double &v,
		SDConvexHull &hullOuter,
		vector<SDConvexHull> &hullInner) {
	bool added = hullOuter.isPointInside(u, v);
	if(added) { // point outside of outer loop is also outside of inner loops
		for(unsigned int k = 0; k < hullInner.size(); k++) {
			added = !hullInner[k].isPointInside(u, v);
			if(!added) break;
		}
	}
	return added;
}

bool SDFace::intersectPlane(Vec3& planeNormal, Vec3& planePoint) {
	//LOG(INFO) << "SDFace::intersectPlane";
	if(!m_BBox.IsValid()) {
		estimateBoundingBox();
	}

	ON_3dPoint max = m_BBox.m_max,
			min = m_BBox.m_min;
	vector<Vec3> vertices;	// 8 sommets du cube
	int i,j,k;
	double x, y, z;
	for( i = 0; i < 2; i++ ) {
		x = (i) ? max.x : min.x;
		for ( j = 0; j < 2; j++ ) {
			y = (j) ? max.y : min.y;
			for ( k = 0; k < 2; k++ ) {
				z = (k) ? max.z : min.z;
				Vec3 v;
				v << x, y, z;
				vertices.push_back(v);
			}
		}
	}

	int ei[24] = {0, 2, 4, 6, 5, 7, 1, 3,
						0, 4, 1, 5, 3, 7, 2, 6,
						0, 1, 4, 5, 6, 7, 2, 3};

	double dist1, dist2;
	bool rc	= false;
	for(i = 0; i < 12; i++) {
		dist1	= MathExt::distancePointPlane(vertices[ei[i*2]], planeNormal, planePoint);
		dist2	= MathExt::distancePointPlane(vertices[ei[i*2+1]], planeNormal, planePoint);
		rc	= ((dist1 * dist2) < 0);
		if(rc) break;
	}

	return rc;
}

void SDFace::estimateBoundingBox()
{
	if(m_PointsOnFace.size() == 0) generateUniformDistanceSamples();
	double minx, miny, minz, maxx, maxy, maxz;
	minx = m_PointsOnFace[0](0);
	miny = m_PointsOnFace[0](1);
	minz = m_PointsOnFace[0](2);
	maxx = m_PointsOnFace[0](0);
	maxy = m_PointsOnFace[0](1);
	maxz = m_PointsOnFace[0](2);
	for(unsigned int i = 1; i < m_PointsOnFace.size(); i++) {
		if (m_PointsOnFace[i](0) < minx) minx = m_PointsOnFace[i](0);
		else if (m_PointsOnFace[i](0) > maxx) maxx = m_PointsOnFace[i](0);
		if (m_PointsOnFace[i](1) < miny) miny = m_PointsOnFace[i](1);
		else if (m_PointsOnFace[i](1) > maxy) maxy = m_PointsOnFace[i](1);
		if (m_PointsOnFace[i](2) < minz) minz = m_PointsOnFace[i](2);
		else if (m_PointsOnFace[i](2) > maxz) maxz = m_PointsOnFace[i](2);
	}

	m_BBox = ON_BoundingBox(ON_3dPoint(minx, miny, minz),
			ON_3dPoint(maxx, maxy, maxz));

	m_BBDiagonal	= m_BBox.Diagonal().Length();
}
