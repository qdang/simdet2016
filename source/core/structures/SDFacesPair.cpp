/*
 * SDFacesPair.cpp
 *
 *  Created on: Jul 29, 2016
 *      Author: formation
 */

#include "core/structures/SDFacesPair.h"


SDFacesPair::SDFacesPair() {
	m_f1 = m_f2 = NULL;
	m_T = NULL;
}

SDFacesPair::~SDFacesPair() {
	m_f1 = m_f2 = NULL;
	for(unsigned int i = 0; i < m_OrigFaces.size(); i++) {
		m_OrigFaces[i]	= NULL;
		m_TransFaces[i]	= NULL;
	}
	m_OrigFaces.clear();
	m_TransFaces.clear();
	m_OrigPoints.clear();
	m_TransPoints.clear();
	m_F2OrdCornIdx.clear();
}

/*
 * f1 is in positive side of the symmetric plane
 */
SDFacesPair::SDFacesPair(SDFace* f1, SDFace* f2,
		SDTransformation* t, SDMatchType type) {
	m_T		= t;
	m_f1	= f1;
	m_f2	= f2;
	m_OrigFaces.push_back(m_f1);
	m_TransFaces.push_back(m_f2);
	m_MatchType	= type;
	f1->m_CurrentTransf	= t->getUuid();
	f2->m_CurrentTransf	= t->getUuid();
}

void SDFacesPair::add(SDFace* f1, SDFace* f2) {
	m_OrigFaces.push_back(f1);
	m_TransFaces.push_back(f2);
}

void SDFacesPair::set(SDFace* f1, SDFace* f2,
		SDTransformation* t, SDMatchType type) {
	m_f1	= f1;
	m_f2	= f2;
	m_T		= t;
	m_MatchType	= type;
}

SDFacesPair::SDFacesPair(const SDFacesPair& other) {
	m_f1	= other.m_f1;
	m_f2	= other.m_f2;
	m_T		= other.m_T;
	m_MatchType	= other.m_MatchType;
	m_F2OrdCornIdx	= other.m_F2OrdCornIdx;
	m_CornersIdx	= other.m_CornersIdx;
	m_OrigFaces.clear();
	m_OrigFaces.insert(m_OrigFaces.end(), other.m_OrigFaces.begin(), other.m_OrigFaces.end());
	m_TransFaces.clear();
	m_TransFaces.insert(m_TransFaces.end(), other.m_TransFaces.begin(), other.m_TransFaces.end());
	m_OrigPoints.clear();
	m_OrigPoints.insert(m_OrigPoints.end(), other.m_OrigPoints.begin(), other.m_OrigPoints.end());
	m_TransPoints.clear();
	m_TransPoints.insert(m_TransPoints.end(), other.m_TransPoints.begin(), other.m_TransPoints.end());
	m_Volume	= other.m_Volume;
}

SDFacesPair& SDFacesPair::operator =(const SDFacesPair& other) {
	m_f1	= other.m_f1;
	m_f2	= other.m_f2;
	m_T		= other.m_T;
	m_MatchType	= other.m_MatchType;
	m_F2OrdCornIdx	= other.m_F2OrdCornIdx;
	m_CornersIdx	= other.m_CornersIdx;
	m_OrigFaces.clear();
	m_OrigFaces.insert(m_OrigFaces.end(), other.m_OrigFaces.begin(), other.m_OrigFaces.end());
	m_TransFaces.clear();
	m_TransFaces.insert(m_TransFaces.end(), other.m_TransFaces.begin(), other.m_TransFaces.end());
	m_OrigPoints.clear();
	m_OrigPoints.insert(m_OrigPoints.end(), other.m_OrigPoints.begin(), other.m_OrigPoints.end());
	m_TransPoints.clear();
	m_TransPoints.insert(m_TransPoints.end(), other.m_TransPoints.begin(), other.m_TransPoints.end());
	m_Volume	= other.m_Volume;
	return *this;
}

SDFacesPairCorners::SDFacesPairCorners() {
	m_FiIdx.clear();
	m_FjIdx.clear();
	m_size	= 0;
}

SDFacesPairCorners::SDFacesPairCorners(int size) {
	m_size	= size;
	for(int i = 0; i < size; i++) {
		m_FiIdx.push_back(i);
		m_FjIdx.push_back(i);
	}
}

SDFacesPairCorners::~SDFacesPairCorners() {
	m_FiIdx.clear();
	m_FjIdx.clear();
	m_size	= 0;
}

SDFacesPairCorners::SDFacesPairCorners(const SDFacesPairCorners& other) {
	m_FiIdx	= other.m_FiIdx;
	m_FjIdx	= other.m_FjIdx;
	m_size	= other.m_size;
}

SDFacesPairCorners& SDFacesPairCorners::operator=(const SDFacesPairCorners& other) {
	m_FiIdx	= other.m_FiIdx;
	m_FjIdx	= other.m_FjIdx;
	m_size	= other.m_size;

	return *this;
}

void	SDFacesPairCorners::set(vector<int>& fi, vector<int>& fj) {
	m_FiIdx	= fi;
	m_FjIdx	= fj;
	m_size	= fi.size();
}

void	SDFacesPairCorners::setFj(vector<int>& fj) {
	if(fj.size() == m_size) {
		m_FjIdx	= fj;
	}
}

int	SDFacesPairCorners::size() {
	return m_size;
}

int	SDFacesPairCorners::getFi(int& i) {
	if(i >= 0 && i < m_size) {
		return m_FiIdx[i];
	} else {
		return -1;
	}
}

int	SDFacesPairCorners::getFj(int& i) {
	if(i >= 0 && i < m_size) {
		return m_FjIdx[i];
	} else {
		return -1;
	}
}

SDFacesPairCorners SDFacesPairCorners::inverse() {
	if(m_size > 0) {
		unsigned int minIdx = -1, minVal = UINT_MAX, i;
		for(i = 0; i < m_size; i++) {
			if(m_FjIdx[i] < minVal) {
				minVal	= m_FjIdx[i];
				minIdx	= i;
			}
		}
		int dir	= 1;
		if(m_FjIdx[minIdx] < m_FjIdx[MathExt::mod(minIdx - 1, m_size)]) {
			dir	= -1;
		}

		i = 0;
		vector<int> fi, fj;
		while(i < m_size) {
			fi.push_back(m_FjIdx[MathExt::mod(minIdx + i * dir, m_size)]);
			fj.push_back(m_FiIdx[MathExt::mod(minIdx + i * dir, m_size)]);
			i++;
		}

		SDFacesPairCorners result;
		result.set(fi, fj);
		fi.clear();
		fj.clear();

		return result;
	} else {
		return *this;
	}
}
