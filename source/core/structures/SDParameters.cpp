/*
 * SDParameters.cpp
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#include "core/structures/SDParameters.h"

const int 	SDParameters::SD_INVALID_INDEX				= -100;

const char* SDParameters::SD_SAMP_MODELFILEPATH 		= "SD_Samp_ModelFilePath";
const char* SDParameters::SD_SAMP_METHOD				= "SD_Samp_Method";
const char* SDParameters::SD_SAMP_UKEYRATIO				= "SD_Samp_UKeyRatio";
const char* SDParameters::SD_SAMP_VKEYRATIO			 	= "SD_Samp_VKeyRatio";
const char* SDParameters::SD_SAMP_URATIO				= "SD_Samp_URatio";
const char* SDParameters::SD_SAMP_VRATIO			 	= "SD_Samp_VRatio";
const char* SDParameters::SD_SAMP_UGAPNUMBER 			= "SD_Samp_UGapNumber";
const char* SDParameters::SD_SAMP_VGAPNUMBER 			= "SD_Samp_VGapNumber";
const int	SDParameters::SD_SAMP_UNIFORM				= 1;
const int	SDParameters::SD_SAMP_ADAPTIVE				= 2;

const char* SDParameters::SD_PAIR_UMBILICRATIO 			= "SD_Pair_UmbilicRatio";
const char* SDParameters::SD_PAIR_MAXPARTNERS 			= "SD_Pair_MaxPartners";
const char* SDParameters::SD_PAIR_NEIGHBORSRADIUS 		= "SD_Pair_NeighborsRadius";

const char* SDParameters::SD_CLUST_ISOMETRY 			= "SD_Clust_Isometry";
const char* SDParameters::SD_CLUST_TRANSFORMATIONDIMENSION = "SD_Clust_TransformDimension";
const char* SDParameters::SD_CLUST_MAXDATAPOINT 		= "SD_Clust_MaxDataPoint";
const char* SDParameters::SD_CLUST_MAXCLUSTERNUMBER		= "SD_Clust_MaxClusterNumber";

const char* SDParameters::SD_VAL_THRESHOLD				= "SD_Val_Threshold";

const char* SDParameters::SD_REND_ISMODELDRAWN 			= "SD_Rend_IsModelDrawn";
const char* SDParameters::SD_REND_ISSAMPLEDRAWN 		= "SD_Rend_IsSampleDrawn";
const char* SDParameters::SD_REND_ISPAIRDRAWN 			= "SD_Rend_IsPairDrawn";
const char* SDParameters::SD_REND_ISCLUSTERDRAWN 		= "SD_Rend_IsClusterDrawn";
const char* SDParameters::SD_REND_ISPATCHDRAWN			= "SD_Rend_IsPatchDrawn";

SDParameters::SDParameters() {
	m_IsDefault						= true;

	m_Sampling_ModelFilePath		= string("/home/dqviet/workspace/SimilaritiesDetection/data/Plane.3dm");
	m_Sampling_Method				= SD_SAMP_UNIFORM;
	m_Sampling_UGap					= NULL;
	m_Sampling_VGap					= NULL;
	m_Sampling_UGapQuantity			= NULL;
	m_Sampling_VGapQuantity			= NULL;
	m_Sampling_UKeyRatio			= 0.5;
	m_Sampling_VKeyRatio			= 0.5;
	m_Sampling_URatio				= 0.2;
	m_Sampling_VRatio				= 0.2;
	m_Sampling_MinDistSamples		= 0.001;

	m_Pairing_UmbilicRatio			= 0.95;
	m_Pairing_MaxPartners			= 20;
	m_Pairing_NeighborsRadius		= 9e-4;

	m_Clustering_Isometry			= 0;
	m_Clustering_MaxDataPoints		= 800;
	m_Clustering_MaxClustersNumber	= 10;
	m_Clustering_TransformDim		= 6;

	m_Rendering_IsModelDrawn		= false;
	m_Rendering_IsSamplesDrawn		= true;
	m_Rendering_IsPairsDrawn		= false;
	m_Rendering_IsClusterDrawn		= false;
	m_Rendering_IsPatchDrawn		= true;

	m_Validating_Threshold			= 0.1;
}

SDParameters::~SDParameters() {
	// TODO Auto-generated destructor stub
}

void SDParameters::LoadParameters(const char* section)
{
	CSimpleIniA ini(true, true, true);
	ini.LoadFile(m_FilePath.c_str());

	const char *tmp 			= ini.GetValue(section, SD_SAMP_MODELFILEPATH, "/home/dqviet/workspace/data/Plane.3dm");
	m_Sampling_ModelFilePath.assign(tmp);

	m_Sampling_Method				= atoi(ini.GetValue(section, SD_SAMP_METHOD, "0"));
	m_Sampling_UKeyRatio			= atof(ini.GetValue(section, SD_SAMP_UKEYRATIO, "0.5"));
	m_Sampling_VKeyRatio			= atof(ini.GetValue(section, SD_SAMP_VKEYRATIO, "0.5"));
	m_Sampling_URatio				= atof(ini.GetValue(section, SD_SAMP_URATIO, "0.2"));
	m_Sampling_VRatio				= atof(ini.GetValue(section, SD_SAMP_VRATIO, "0.2"));

	m_Pairing_UmbilicRatio 			= atof(ini.GetValue(section, SD_PAIR_UMBILICRATIO, "0.85"));
	m_Pairing_NeighborsRadius 		= atof(ini.GetValue(section, SD_PAIR_NEIGHBORSRADIUS, "1e-4"));
	m_Pairing_MaxPartners			= atoi(ini.GetValue(section, SD_PAIR_MAXPARTNERS, "20"));

	m_Clustering_MaxClustersNumber 	= atoi(ini.GetValue(section, SD_CLUST_MAXCLUSTERNUMBER, "10"));
	m_Clustering_MaxDataPoints 		= atoi(ini.GetValue(section, SD_CLUST_MAXDATAPOINT, "500"));

	m_Clustering_Isometry			= atoi(ini.GetValue(section, SD_CLUST_ISOMETRY, "0"));

	m_Validating_Threshold	 		= atof(ini.GetValue(section, SD_VAL_THRESHOLD, "0.1"));

	m_Rendering_IsModelDrawn 		= atoi(ini.GetValue(section, SD_REND_ISMODELDRAWN, "0"));
	m_Rendering_IsSamplesDrawn 		= atoi(ini.GetValue(section, SD_REND_ISSAMPLEDRAWN, "0"));
	m_Rendering_IsPairsDrawn 		= atoi(ini.GetValue(section, SD_REND_ISPAIRDRAWN, "0"));
	m_Rendering_IsClusterDrawn 		= atoi(ini.GetValue(section, SD_REND_ISCLUSTERDRAWN, "0"));
	m_Rendering_IsPatchDrawn 		= atoi(ini.GetValue(section, SD_REND_ISPATCHDRAWN, "0"));

	m_IsDefault					= false;
}

void SDParameters::Print()
{
	LOG(INFO) << SD_SAMP_MODELFILEPATH 			<< " : " << m_Sampling_ModelFilePath;
	LOG(INFO) << SD_SAMP_URATIO					<< " : " << m_Sampling_URatio	 	;
	LOG(INFO) << SD_SAMP_VRATIO			 		<< " : " << m_Sampling_VRatio	 	;
	LOG(INFO) << SD_SAMP_UKEYRATIO				<< " : " << m_Sampling_UKeyRatio	 	;
	LOG(INFO) << SD_SAMP_VKEYRATIO			 	<< " : " << m_Sampling_VKeyRatio	 	;

	LOG(INFO) << SD_PAIR_UMBILICRATIO 			<< " : " << m_Pairing_UmbilicRatio 		;
	LOG(INFO) << SD_PAIR_NEIGHBORSRADIUS 		<< " : " << m_Pairing_NeighborsRadius 	;
	LOG(INFO) << SD_PAIR_MAXPARTNERS	 		<< " : " << m_Pairing_MaxPartners	 	;

	LOG(INFO) << SD_CLUST_ISOMETRY 				<< " : " << m_Clustering_Isometry 		;
	LOG(INFO) << SD_CLUST_MAXDATAPOINT			<< " : " << m_Clustering_MaxDataPoints	;
	LOG(INFO) << SD_CLUST_MAXCLUSTERNUMBER 		<< " : " << m_Clustering_MaxClustersNumber ;

	LOG(INFO) << SD_VAL_THRESHOLD			 	<< " : " << m_Validating_Threshold	 	;
}

bool SDParameters::LoadIniFile(const char* iniFile)
{
	ifstream ifs(iniFile, ios::in);
	if(!ifs.good())
	{
		return false;
	}
	else
	{
		ifs.close();
		m_FilePath.assign(iniFile);
		return true;
	}
}

bool SDParameters::WriteIniFile(const char* section)
{
	CSimpleIniA ini(true, true, true);
	ini.LoadFile(m_FilePath.c_str());

	ini.SetValue(section, SD_SAMP_MODELFILEPATH, m_Sampling_ModelFilePath.c_str());
	ini.SetValue(section, SD_SAMP_UKEYRATIO, SDMaths::NumberToString(m_Sampling_UKeyRatio).c_str());
	ini.SetValue(section, SD_SAMP_VKEYRATIO, SDMaths::NumberToString(m_Sampling_VKeyRatio).c_str());

	ini.SetValue(section, SD_PAIR_UMBILICRATIO, SDMaths::NumberToString(m_Pairing_UmbilicRatio).c_str());
	ini.SetValue(section, SD_PAIR_NEIGHBORSRADIUS, SDMaths::NumberToString(m_Pairing_NeighborsRadius).c_str());
	ini.SetValue(section, SD_PAIR_MAXPARTNERS, SDMaths::NumberToString(m_Pairing_MaxPartners).c_str());

	ini.SetValue(section, SD_CLUST_ISOMETRY, SDMaths::NumberToString(m_Clustering_Isometry).c_str());

	ini.SetValue(section, SD_CLUST_MAXCLUSTERNUMBER, SDMaths::NumberToString(m_Clustering_MaxClustersNumber).c_str());
	ini.SetValue(section, SD_CLUST_MAXDATAPOINT, SDMaths::NumberToString(m_Clustering_MaxDataPoints).c_str());

	ini.SetValue(section, SD_VAL_THRESHOLD, SDMaths::NumberToString(m_Validating_Threshold).c_str());

	ini.SetValue(section, SD_REND_ISMODELDRAWN, SDMaths::NumberToString(m_Rendering_IsModelDrawn).c_str());
	ini.SetValue(section, SD_REND_ISSAMPLEDRAWN, SDMaths::NumberToString(m_Rendering_IsSamplesDrawn).c_str());
	ini.SetValue(section, SD_REND_ISPAIRDRAWN, SDMaths::NumberToString(m_Rendering_IsPairsDrawn).c_str());
	ini.SetValue(section, SD_REND_ISCLUSTERDRAWN, SDMaths::NumberToString(m_Rendering_IsClusterDrawn).c_str());
	ini.SetValue(section, SD_REND_ISPATCHDRAWN, SDMaths::NumberToString(m_Rendering_IsPatchDrawn).c_str());

	return (ini.SaveFile(m_FilePath.c_str()) < 0) ? false : true;
}

bool SDParameters::IsDefault()
{
	return m_IsDefault;
}

void SDParameters::SetDefault(bool d)
{
	m_IsDefault	= d;
}
