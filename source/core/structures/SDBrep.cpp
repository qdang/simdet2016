/*
 * SDBrep.cpp
 *
 *  Created on: Aug 1, 2016
 *      Author: formation
 */

#include "core/structures/SDBrep.h"


SDBrep::SDBrep() {
	m_Brep	= NULL;
}

SDBrep::SDBrep(ON_Brep* brep) {
	m_Brep	= brep;
	for(int i = 0; i < brep->m_F.Count(); i++) {
		SDFace* face	= new SDFace();
		face->setONBrepFace(brep->Face(i));
		face->setSDBrep(this);
		m_Faces.push_back(face);
	}
}

SDBrep::~SDBrep() {
	m_Brep	= NULL;
	for(unsigned int i = 0; i < m_Faces.size(); i++) {
		delete m_Faces[i];
	}
	m_Faces.clear();
}

SDBrep::SDBrep(const SDBrep& other) {
	m_Brep		= other.m_Brep;
	m_Faces		= other.m_Faces;
	m_BrepIndex	= other.m_BrepIndex;
}

SDBrep& SDBrep::operator =(const SDBrep& other) {
	m_Brep		= other.m_Brep;
	m_Faces		= other.m_Faces;
	m_BrepIndex	= other.m_BrepIndex;
	return *this;
}

SDBrep* SDBrep::operator =(SDBrep* other) {
	m_Brep		= other->m_Brep;
	m_Faces		= other->m_Faces;
	m_BrepIndex	= other->m_BrepIndex;
	return this;
}

ON_Brep* SDBrep::getONBrep() {
	return m_Brep;
}

SDFace* SDBrep::getSDFace(int i) {
	if(i < 0 || i >= (int)m_Faces.size()) {
		return NULL;
	} else {
		return m_Faces[i];
	}
}

void SDBrep::setBrepIndex(int& i) {
	m_BrepIndex	= i;
}

bool SDBrep::getSDFace(int ONFi, SDFace*& sdFace) {
	for(unsigned int i = 0; i < m_Faces.size(); i++) {
		if(m_Faces[i]->m_ONFaceIndex == ONFi) {
			sdFace	= m_Faces[i];
			return true;
		}
	}
	sdFace	= NULL;
	return false;
}

int SDBrep::getBrepIndex() {
	return m_BrepIndex;
}
