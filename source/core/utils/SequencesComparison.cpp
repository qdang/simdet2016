/*
 * SequencesComparison.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: dqviet
 */

#include "core/utils/SequencesComparison.h"

SequencesComparison::SequencesComparison() {}

double SequencesComparison::estimateGoodmanKruskalIndex(double* seq1, double* seq2, int& n) {
	int concordant = 0, discordant = 0, i;
	double temp;
	for(i = 0; i < n; i++) {
		temp = (seq1[i] - seq1[(i+1)%n]) * (seq2[i] - seq2[(i+1)%n]);
		if(temp > 0) {
			concordant++;
		} else if(temp < 0) {
			discordant++;
		} else {
			if((seq1[i] == seq1[(i+1)%n]) && (seq2[i] == seq2[(i+1)%n])) {
				concordant++;
			} else {
				discordant++;
			}
		}
	}

	return (concordant - discordant) / (concordant + discordant);
}

bool SequencesComparison::orderTwoSequences(vector<SwapableNumberPair>& seq1,
		vector<SwapableNumberPair>& seq2,
		vector<vector<int> >& outSeq2Idx) {
	int n1 = seq1.size(), n2 = seq2.size();
	if(n1 < 1 || n2 < 1) return false;
	if(n1 != n2) return false;
	int i, j, c, dir = 1;
	bool loopHasEqualVal = false,
		isEqual	= false;

	outSeq2Idx.clear();

	for(i = 0; i < n1; i++) {
		loopHasEqualVal = false;
		c = seq1[0].compareTo(seq2[i]);

		if(c == 1) { // égaux dans le sens augmenté
			loopHasEqualVal = true;
			// compare 2 sequences
			for(j = 0; j < n1; j++) {
				if(seq1[j].compareTo(seq2[MathExt::mod(i + j, n1)]) != 1) {
					loopHasEqualVal = false;
					break;
				}
			}
			dir	= 1;
		} else if(c == 2) { // égaux dans le sens diminué
			loopHasEqualVal = true;
			for(j = 0; j < n1; j++) {
				if(seq1[j].compareTo(seq2[MathExt::mod(i - j, n1)]) != 2) {
					loopHasEqualVal = false;
					break;
				}
			}
			dir = -1;
		}

		if(loopHasEqualVal) {
			isEqual	= true;
			// copy results
			vector<int> r;
			for(j = 0; j < n1; j++) {
				r.push_back(MathExt::mod(i + dir * j, n1));
			}
			outSeq2Idx.push_back(r);
		}
	}
	return isEqual;
}

SwapableNumberPair::SwapableNumberPair() {
	m_num1	= m_num2 = 0;
}

SwapableNumberPair::SwapableNumberPair(double& num1, double& num2) {
	m_num1	= num1;
	m_num2	= num2;
}

SwapableNumberPair::SwapableNumberPair(const SwapableNumberPair& other) {
	m_num1	= other.m_num1;
	m_num2	= other.m_num2;
}

SwapableNumberPair& SwapableNumberPair::operator =(
		const SwapableNumberPair& other) {
	m_num1	= other.m_num1;
	m_num2	= other.m_num2;
	return *this;
}

int SwapableNumberPair::compareTo(const SwapableNumberPair& other) {
	double tol = 5e-2;
	if(MathExt::equal(m_num1, other.m_num1, tol) && MathExt::equal(m_num2, other.m_num2, tol)) {
		return 1;
	} else if(MathExt::equal(m_num1, other.m_num2, tol) && MathExt::equal(m_num2, other.m_num1, tol)) {
		return 2;
	} else {
		return 0;
	}
}

void SwapableNumberPair::swap() {
	double temp = m_num1;
	m_num1 = m_num2;
	m_num2 = temp;
}
