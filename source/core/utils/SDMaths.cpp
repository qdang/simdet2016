/*
 * SDMaths.cpp
 *
 *  Created on: Jul 19, 2013
 *      Author: dqviet
 */

#include "core/utils/SDMaths.h"

SDMaths::SDMaths() {
	// TODO Auto-generated constructor stub

}

SDMaths::~SDMaths() {
	// TODO Auto-generated destructor stub
}

double SDMaths::EvalDotProduct(ON_3dVector v1, ON_3dVector v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

double SDMaths::EvalDotProduct2d(ON_2dPoint v1, ON_2dPoint v2)
{
	return v1.x * v2.x + v1.y * v2.y;
}

ON_3dVector SDMaths::EvalCrossProduct(ON_3dVector v1, ON_3dVector v2)
{
	ON_3dVector v;
	v.x = v1.y * v2.z - v1.z * v2.y;
	v.y = v1.z * v2.x - v1.x * v2.z;
	v.z = v1.x * v2.y - v1.y * v2.x;

	return v;
}

/**************************************************************************************
 *			MATRIX UTILITIES
 *************************************************************************************/
void SDMaths::dgeev_sort(double *Er, double *Ei, double **Evecs, int N)
{
	double temp, *E2, *angles;
	int i, j, k;

	E2 = new double[N];
	angles = new double[N];
	for (i=0; i<N; i++)
	{
		complex<double> compNum(Er[i], Ei[i]);
		E2[i] = abs(compNum); //Er[i]*Er[i]+Ei[i]*Ei[i];
		angles[i] = arg(compNum); //atan2(Ei[i], Er[i]);
	}

	bool swap;
	for (i=0; i<N-1; i++)
		for (j=i+1; j<N; j++)
		{
			if (E2[i] < E2[j])
			{
				swap = true;
			}
			else if(E2[i] == E2[j])
			{
				swap = (angles[i] < angles[j]) ? true:false;
			}
			else
			{
				swap = false;
			}

			if(swap)
			{
				temp = E2[i]; E2[i] = E2[j]; E2[j] = temp;
				temp = angles[i]; angles[i] = angles[j]; angles[j] = temp;
				temp = Er[i]; Er[i] = Er[j]; Er[j] = temp;
				temp = Ei[i]; Ei[i] = Ei[j]; Ei[j] = temp;

				for (k=0; k<N; k++) {
					temp = Evecs[k][i];
					Evecs[k][i] = Evecs[k][j];
					Evecs[k][j] = temp;
				}
			}
		}
	delete[] E2;
	delete[] angles;
}

double* SDMaths::dgeev_ctof(double **in, int rows, int cols)
{
	double *out;
	int i, j;

	out = new double[rows*cols];
	for (i=0; i<rows; i++) for (j=0; j<cols; j++) out[i+j*cols] = in[i][j];
	return out;
}

double SDMaths::EvalDeterminant(double** a, int n)
{
	// http://paulbourke.net/miscellaneous/determinant/
	int i,j,j1,j2;
	double det = 0;
	double **m = NULL;

	if (n < 1) { /* Error */

	} else if (n == 1) { /* Shouldn't get used */
		det = a[0][0];
	} else if (n == 2) {
		det = a[0][0] * a[1][1] - a[1][0] * a[0][1];
	} else {
		det = 0;
		for (j1=0;j1<n;j1++) {
			m = new double*[n - 1];
			for (i=0;i<n-1;i++)
				m[i] = new double[n - 1];
			for (i=1;i<n;i++) {
				j2 = 0;
				for (j=0;j<n;j++) {
					if (j == j1)
						continue;
					m[i-1][j2] = a[i][j];
					j2++;
				}
			}
			det += pow(-1.0,1.0+j1+1.0) * a[0][j1] * EvalDeterminant(m,n-1);
			for (i=0;i<n-1;i++)
				delete[] m[i];
			delete[] m;
		}
	}

	return det;
}

double SDMaths::EvalDeterminant(double a[][4], int n)
{
	// http://paulbourke.net/miscellaneous/determinant/
	int i,j,j1,j2;
	double det = 0;
	double **m = NULL;

	if (n < 1) { /* Error */

	} else if (n == 1) { /* Shouldn't get used */
		det = a[0][0];
	} else if (n == 2) {
		det = a[0][0] * a[1][1] - a[1][0] * a[0][1];
	} else {
		det = 0;
		for (j1=0;j1<n;j1++) {
			m = new double*[n - 1];
			for (i=0;i<n-1;i++)
				m[i] = new double[n - 1];
			for (i=1;i<n;i++) {
				j2 = 0;
				for (j=0;j<n;j++) {
					if (j == j1)
						continue;
					m[i-1][j2] = a[i][j];
					j2++;
				}
			}
			det += pow(-1.0,1.0+j1+1.0) * a[0][j1] * EvalDeterminant(m,n-1);
			for (i=0;i<n-1;i++)
				delete[] m[i];
			delete[] m;
		}
	}

	return det;
}

bool SDMaths::isMatrixIdentity(double** A, int size_m)
{
	return (SDMaths::equal(EvalDeterminant(A, size_m), 1.0))
			&& (SDMaths::equal(MatrixTrace(A, size_m), size_m * 1.0));
}

double SDMaths::MatrixTrace(double** A, int size_m)
{
	double trace = 0.0;
	for(int i = 0; i < size_m; i++)
	{
		trace += A[i][i];
	}
	return trace;
}

bool SDMaths::areMatricesEqual(ON_Matrix& A, ON_Matrix& B) {
	if(A.RowCount() != B.RowCount() || A.ColCount() != B.ColCount())
		return false;

	for(int i = 0; i < A.RowCount(); i++) {
		for(int j = 0; j < A.ColCount(); j++) {
			if(!equal(A[i][j], B[i][j], 1e-6))
				return false;
		}
	}

	return true;
}

void SDMaths::inverse(double* A, int N)
{
	int *IPIV = new int[N+1];
	int LWORK = N*N;
	double *WORK = new double[LWORK];
	int INFO;

	dgetrf_(&N,&N,A,&N,IPIV,&INFO);
	dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

	delete [] IPIV;
	delete [] WORK;
}

bool SDMaths::dgeev(double **H, int n, double *Er, double *Ei, double **Evecs, bool* hasConjugatedEigVectors)
{
	char jobvl, jobvr;
	int lda,  ldvl, ldvr, lwork, info;
	double *a, *vl, *vr, *work;

	jobvl = 'N';
	jobvr = 'V';
	lda = n;
	a = dgeev_ctof(H, n, lda);

	ldvl = n;
	vl = new double[n*n];
	ldvr = n;
	vr = new double[n*n];
	work = new double[4*n];
	lwork = 4*n;

	dgeev_(&jobvl, &jobvr, &n, a, &lda, Er, Ei, vl,
			&ldvl, vr, &ldvr, work, &lwork, &info);	// vl,vr: col_major

	// éviter cette fonction car elle ne traite pas les paires valeurs propres
	// conjugées
	//dgeev_ftoc(vr, Evecs, n, ldvr);
	bool hasCV = dgeev_getRealEigVec(&n, Er, Ei, vr, &ldvr, Evecs);
	if(hasConjugatedEigVectors)
		*hasConjugatedEigVectors = hasCV;
	dgeev_sort(Er, Ei, Evecs, n);

	delete[] a;
	delete[] vl;
	delete[] vr;
	delete[] work;

	return (info == 0);
}

bool SDMaths::dsyev(double **H, int n, double *Er, double **Evecs)
{
	char jobz = 'V';
	char uplo = 'U';
	int lda = n;
	int lwork = 3 * n - 1;
	double *work = new double[lwork];
	int info;

	double* a = dgeev_ctof(H,n,n);

	dsyev_(&jobz, &uplo, &n, a, &lda, Er, work, &lwork, &info);

	delete[] work;
	if(info == 0)
	{
		dgeev_ftoc(a, Evecs, n, n);
		dsyev_sort(Er, Evecs, n);
		return true;
	}
	else
		return false;
}

void SDMaths::dsyev_sort(double* Er, double** Evecs, int n)
{
	double tmp;
	for(int i = 0; i < n - 1; i++)
	{
		if(Er[i] > Er[i+1])
		{
			tmp		= Er[i];
			Er[i]	= Er[i+1];
			Er[i+1]	= tmp;
			for(int j = 0; j < n; j++)
			{
				tmp				= Evecs[i][j];
				Evecs[i][j]		= Evecs[i+1][j];
				Evecs[i+1][j]	= tmp;
			}
		}
	}
}

void SDMaths::dgeev_ftoc(double *in, double **out, int rows, int cols)
{
	int i, j;

	for (i=0; i<rows; i++)
		for (j=0; j<cols; j++)
		{
			if(equal(in[i+j*cols], SD_ZERO_TOLERANCE))
				out[i][j] = 0;
			else
				out[i][j] = in[i+j*cols];
		}
}

/*
 * Retirer les vecteurs propres à partir du résultat (en array) retourné par LAPACK
 * Output: evecs - les vecteurs propres sont des colonnes de cette matrice
 */
bool SDMaths::dgeev_getRealEigVec(int *n, double *wr, double *wi, double *vr, int *ldvr, double** evecs)
{
	bool hasConjugatedVectors = false;
	for(int i = 0; i < *n; i++)
	{
		for(int j = 0; j < *ldvr; j++)
		{
			evecs[i][j] = vr[i + j * (*ldvr)];
		}
	}

	for(int i = 0; i < *n -1; i++)
	{
		if((wr[i] == wr[i+1]) && (wi[i] != 0) && (wi[i] == (-1 * wi[i+1])) )// cas de deux vecteurs conjugués
		{
			hasConjugatedVectors = true;
			for(int k = 0; k < *ldvr; k++)
				evecs[k][i+1] = evecs[k][i];
		}
	}

	return hasConjugatedVectors;
}

bool SDMaths::dgesv(double** A, int n, double* B) {
	int nrhs 	= 1;	// columns number of B
	int lda 	= n;
	int ldb		= n;	// rows number of B
	int *ipiv 	= new int[n+1];
	int info;
	double* matrix = dgeev_ctof(A, n, n);
	dgesv_(&n, &nrhs, matrix, &lda, ipiv, B, &ldb, &info);

	delete[] ipiv;
	delete[] matrix;

	return (info == 0);
}

/** Geometry Utilities **/

double SDMaths::DistancePointToSegment(ON_2dPoint& C, ON_2dPoint& A, ON_2dPoint& B)
{
	double L2 =	((B.x-A.x)*(B.x-A.x) + (B.y-A.y)*(B.y-A.y));
	double r = ((C.x-A.x)*(B.x-A.x) + (C.y-A.y)*(B.y-A.y)) / L2;
	double d = 0;

	/*
	 *      AC dot AB
        r = ---------  		(1)
            ||AB||^2

    r has the following meaning:

        r=0      P = A
        r=1      P = B
        r<0      P is on the backward extension of AB
        r>1      P is on the forward extension of AB
        0<r<1    P is interior to AB
	 */
	if(r >= 0 && r <= 1)
	{
		//ON_2dPoint P(A.x + r * (B.x - A.x), A.y + r * (B.y - A.y));
		/*
		 * Use another parameter s to indicate the location along PC, with the
    	 * following meaning:
         *  s<0      C is left of AB
         *  s>0      C is right of AB
         *  s=0      C is on AB
		 */
		double s =  ((A.y-C.y)*(B.x-A.x)-(A.x-C.x)*(B.y-A.y) ) / L2;
		d = fabs(s)*sqrt(L2);
	}
	else if(r < 0)
	{
		d = sqrt((C.x-A.x)*(C.x-A.x) + (C.y-A.y)*(C.y-A.y));
	}
	else
	{
		d = sqrt((C.x-B.x)*(C.x-B.x) + (C.y-B.y)*(C.y-B.y));
	}

	return d;
}

// Réf: http://fr.wikipedia.org/wiki/Projection_orthogonale
// lineNormal doit être unitaire
ON_3dPoint SDMaths::ProjectPointOnLine(const ON_3dVector& lineDirective,
			const ON_3dPoint& pointOnLine, const ON_3dPoint& point)
{
	ON_3dPoint projectedPoint;

	ON_3dVector vectBA(point - pointOnLine);
	double d = EvalDotProduct(vectBA, lineDirective);
	projectedPoint.x	= pointOnLine.x + d * lineDirective.x;
	projectedPoint.y	= pointOnLine.y + d * lineDirective.y;
	projectedPoint.z	= pointOnLine.z + d * lineDirective.z;

	return projectedPoint;
}

/*
 * http://mathworld.wolfram.com/Reflection.html
 *
 *        ^ n
 * 	x1----|-----x2
 * 	 \    | xr /
 * 	  \   |   /			xr = x0 + $n$ * [(x1 - x0).$n$]
 * 	   \  |  /			$x1x2$ = 2 * $x1xr$
 * 	    \ | /		=>	x2 - x1 = 2xr - 2x1
 * 	     \|/ 		=>	x2 = 2xr - x1
 * 	      x0
 */

ON_3dPoint SDMaths::ReflectPointAcrossPlane(const ON_3dVector& planeNormal,
			const ON_3dPoint& pointOnPlan, const ON_3dPoint& pointOfInterest)
{
	ON_3dPoint pointOfReflection;

	ON_3dVector vect(pointOfInterest - pointOnPlan);
	double dist		= EvalDotProduct(vect, planeNormal);
	pointOfReflection = pointOfInterest - 2 * dist * planeNormal;

	return pointOfReflection;
}

double SDMaths::DistancePointToPlane(const ON_3dVector& planeNormal,
			const ON_3dPoint& pointOnPlan, const ON_3dPoint& pointOfInterest)
{
	double d = EvalDotProduct(planeNormal, pointOnPlan) * -1.0;
	return abs(EvalDotProduct(planeNormal, pointOfInterest) + d);
}

ON_3dPoint SDMaths::RotatePointAroundLine(const ON_3dVector& axis,
			const ON_3dPoint& origin, const double& angle, const ON_3dPoint& pointOfInterest)
{
	ON_3dPoint pointOfRotation;
	double a = origin.x, b = origin.y, c = origin.z;
	double u = axis.x, v = axis.y, w = axis.z;
	double x = pointOfInterest.x, y = pointOfInterest.y, z = pointOfInterest.z;
	pointOfRotation.x = ( a * (v * v + w * w) - u * (b * v + c * w - u * x - v * y - w * z)) * (1 - cos(angle)) +
						x * cos(angle) + (-c * v + b * w - w * y + v * z) * sin(angle);
	pointOfRotation.y = ( b * (u * u + w * w) - v * (a * u + c * w - u * x - v * y - w * z)) * (1 - cos(angle)) +
						y * cos(angle) + (c * u - a * w + w * x - u * z) * sin(angle);
	pointOfRotation.z = ( c * (u * u + v * v) - w * (a * u + b * v - u * x - v * y - w * z)) * (1 - cos(angle)) +
						z * cos(angle) + (-b * u + a * v - v * x + u * y) * sin(angle);

	return pointOfRotation;
}

bool SDMaths::FindIntersectionLinePlane(const ON_3dVector& planeNormal, const ON_3dPoint& pointOnPlan,
			const ON_3dPoint& linePoint1, const ON_3dPoint& linePoint2, ON_3dPoint& intersection)
{
	// Réf: http://www.thepolygoners.com/tutorials/lineplane/lineplane.html
	/*ON_3dVector lineDirection(linePoint2 - linePoint1);
	lineDirection.Unitize();
	double dotProduct = EvalDotProduct(planeNormal, lineDirection);

	if(equal(dotProduct, 0.0, 1e-6))
		return false;

	ON_3dVector tmp(pointOnPlan - linePoint1);

	double t = EvalDotProduct(planeNormal, tmp) / dotProduct;

	intersection = linePoint1 + t * lineDirection;*/

	// Réf: http://math.stackexchange.com/questions/83990/line-and-plane-intersection-in-3d
	ON_3dVector lineDirection(linePoint2 - linePoint1);
	if(!lineDirection.Unitize())
		return false;
	double dotProduct = EvalDotProduct(planeNormal, lineDirection);

	if(equal(dotProduct, 0.0, 1e-6))	// line and plane are parallel
	{
		intersection	= ON_3dPoint::UnsetPoint;
		return false;
	}

	double d 	= EvalDotProduct(planeNormal, pointOnPlan);

	double nDotA	= EvalDotProduct(planeNormal, linePoint1);
	double nDotBA	= EvalDotProduct(planeNormal, lineDirection);

	intersection = linePoint1 + (((d - nDotA)/nDotBA) * lineDirection);
	return true;
}

void SDMaths::AnalyzeEigenVectorsIndices(double* realEigenValues, double* imgEigenValues,
		int& minusOneIdx, int* oneIdx, int* oneNum, bool* hasConjEigenVectors)
{
	int tmpMinusOneIdx = -1, numEigValOne = 0;
	int tmpOneIdx[] = {-1, -1, -1};

	for(int i = 2; i >= 0; i--)
	{
		if(equal(realEigenValues[i], -1.0, 1e-5))
		{
			tmpMinusOneIdx	= i;
		}

		if(equal(realEigenValues[i], 1.0, 1e-5))
		{
			tmpOneIdx[numEigValOne++] = i;
		}
	}

	minusOneIdx = tmpMinusOneIdx;

	if(oneIdx)
	{
		for(int i = 0; i < 3; i++)
			oneIdx[i] = tmpOneIdx[i];
	}

	if(oneNum)
	{
		*oneNum	= numEigValOne;
	}

	if(hasConjEigenVectors)
	{
		for(int i = 0; i < 2; i++)
		{
			if((realEigenValues[i] == realEigenValues[i+1]) &&
			(imgEigenValues[i] != 0) && (imgEigenValues[i] == (-1 * imgEigenValues[i+1])) )// cas de deux vecteurs conjugués
			{
				*hasConjEigenVectors = true;
			}
		}
	}
}

bool SDMaths::almostEqual(double A, double B, int maxUlps) {
	// http://stackoverflow.com/questions/17333/most-effective-way-for-float-and-double-comparison
	// Make sure maxUlps is non-negative and small enough that the
	// default NAN won't compare as equal to anything.
	// assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
	int aInt = *(int*)&A;
	// Make aInt lexicographically ordered as a twos-complement int
	if (aInt < 0)
		aInt = 0x80000000 - aInt;
	// Make bInt lexicographically ordered as a twos-complement int
	int bInt = *(int*)&B;
	if (bInt < 0)
		bInt = 0x80000000 - bInt;
	int intDiff = abs(aInt - bInt);
	if (intDiff <= maxUlps)
		return true;
	return false;
}
