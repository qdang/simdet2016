#include "core/utils/SDConvexHull.h"

SDConvexHull::SDConvexHull() {
	m_Hull	= vector<Vector2d>();
	m_Vert	= 0;
}

double SDConvexHull::isLeft( Vector2d& P0, Vector2d& P1, Vector2d& P2 ) {
	return (P1(0) - P0(0))*(P2(1) - P0(1)) - (P2(0) - P0(0))*(P1(1) - P0(1));
}

int SDConvexHull::isPointInside(double& testx, double& testy) {
	if(m_Vert > 0) {
		int i, j, c = 0;
		for (i = 0, j = m_Vert-1; i < m_Vert; j = i++) {
			if ( ((m_Hull[i](1) > testy) != (m_Hull[j](1) > testy)) &&
					(testx < (m_Hull[j](0) - m_Hull[i](0)) *
							(testy - m_Hull[i](1)) / (m_Hull[j](1) - m_Hull[i](1)) + m_Hull[i](0)) )
				c = !c;
		}
		return c;
	} else {
		return -1;
	}
}

void SDConvexHull::init(vector<Vector2d>& P, bool runAlgo) {
	if(runAlgo) {
		int	n = P.size();
		int bot=0, top=(-1);  // indices for bottom and top of the stack
		int i;                // array scan index
		// Get the indices of points with min x-coord and min|max y-coord
		int minmin = 0, minmax;
		double xmin = P[0](0);
		for (i = 1; i < n; i++)
			if (P[i](0) != xmin) break;
		minmax = i-1;
		if (minmax == n-1)
		{       // degenerate case: all x-coords == xmin
			m_Hull.push_back(P[minmin]); ++top;
			if (P[minmax](1) != P[minmin](1))
			{ // a nontrivial segment
				m_Hull.push_back(P[minmax]); ++top;
			}
			m_Hull.push_back(P[minmin]); ++top;      // add polygon endpoint
			return;
		}

		// Get the indices of points with max x-coord and min|max y-coord
		int maxmin, maxmax = n-1;
		double xmax = P[n-1](0);
		for (i = n-2; i >= 0; i--)
			if (P[i](0) != xmax) break;
		maxmin = i+1;

		// Compute the lower hull on the stack H
		m_Hull.push_back(P[minmin]); ++top;    // push minmin point onto stack
		i = minmax;
		while (++i <= maxmin)
		{
			// the lower line joins P[minmin] with P[maxmin]
			if (isLeft( P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin)
				continue;          // ignore P[i] above or on the lower line

			while (top > 0)        // there are at least 2 points on the stack
			{
				// test if P[i] is left of the line at the stack top
				if (isLeft( m_Hull[top-1], m_Hull[top], P[i]) > 0)
					break;         // P[i] is a new hull vertex
				else
				{
					m_Hull.pop_back();
					top--;         // pop top point off stack
				}
			}
			m_Hull.push_back(P[i]);++top;       // push P[i] onto stack
		}

		// Next, compute the upper hull on the stack H above the bottom hull
		if (maxmax != maxmin)      // if distinct xmax points
			m_Hull.push_back(P[maxmax]);++top;  // push maxmax point onto stack
		bot = top;                 // the bottom point of the upper hull stack
		i = maxmin;
		while (--i >= minmax)
		{
			// the upper line joins P[maxmax] with P[minmax]
			if (isLeft( P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
				continue;          // ignore P[i] below or on the upper line

			while (top > bot)    // at least 2 points on the upper stack
			{
				// test if P[i] is left of the line at the stack top
				if (isLeft( m_Hull[top-1], m_Hull[top], P[i]) > 0)
					break;         // P[i] is a new hull vertex
				else
				{
					m_Hull.pop_back();
					top--;         // pop top point off stack
				}
			}
			m_Hull.push_back(P[i]);++top;       // push P[i] onto stack
		}
		if (minmax != minmin)
			m_Hull.push_back(P[minmin]);++top;  // push joining endpoint onto stack
	} else {
		m_Hull	= P;
	}
	m_Vert	= m_Hull.size();
}
