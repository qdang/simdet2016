/*
 * SDSurfaces.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: dqviet
 */

#include "core/utils/SDSurfaces.h"

bool SDSurfaces::SplitBrepFace(SDFace* sdFace,
		int dir, ON_NurbsSurface *leftOrTopSrf, ON_NurbsSurface *rightOrBotSrf) {
	ON_BrepFace* inFace	= sdFace->getONBrepFace();
	//ON_Brep* brep	= inFace->Brep();
	ON_NurbsSurface* srf	= const_cast<ON_NurbsSurface*>(ON_NurbsSurface::Cast(inFace->SurfaceOf()));
	if(srf) {
		double midParam	= srf->Domain(dir).Mid();

		ON_Surface *lftS = 0, *rgtS = 0;
		// découper la surface en 2
		if(!srf->Split(dir, midParam, lftS, rgtS)) {
			LOG(INFO) << "Surface cannot split";
			return false;
		}

		leftOrTopSrf->CopyFrom(lftS);
		rightOrBotSrf->CopyFrom(rgtS);

		/*int leftIdx = 0, rightIdx = 0;
		leftIdx		= brep->AddSurface(leftOrTopSrf);
		//rightIdx	= brep->AddSurface(rightOrBotSrf);

		if(leftIdx == -1) {
			LOG(INFO) << "Cannot add new surface to brep";
			return false;
		}

		ON_BrepFace &lFace	= brep->NewFace(leftIdx);
		for(int li = 0; li < inFace->m_li.Count(); li++) {
			ON_BrepLoop* sLoop = inFace->Loop(li);
			ON_BrepLoop& loop = brep->NewLoop(sLoop->m_type, lFace);
			for(int ti = 0; ti < sLoop->m_ti.Count(); ti++) {
				ON_BrepTrim* sTrim = brep->Trim(sLoop->m_ti[ti]);
				ON_BrepTrim& trim = brep->NewTrim(brep->m_E[sTrim->m_ei], sTrim->m_bRev3d,
									loop, sTrim->m_c2i);
				trim.m_tolerance[0] = sTrim->m_tolerance[0];
				trim.m_tolerance[1] = sTrim->m_tolerance[1];
				trim.m_type			= sTrim->m_type;
				trim.m_iso			= sTrim->m_iso;
			}
		}
		LOG(INFO) << "New BrepFace added at " << lFace.m_face_index;

		SDBrep* sdb	= sdFace->getSDBrep();
		SDFace* sdf1 = new SDFace();
		sdf1->setONBrepFace(brep->Face(lFace.m_face_index));
		sdf1->setSDBrep(sdb);
		sdb->m_Faces[sdb->m_Faces.size()] = sdf1;

		FILE* pFile;
		pFile = fopen("SplittedSrfs.txt", "w");
		ON_TextLog log(pFile);
		lFace.IsValid(&log);
		fclose(pFile);*/

		return true;
	} else {
		LOG(INFO) << "Surface is not a NURBS";
		return false;
	}
}

bool SDSurfaces::SplitEdge(ON_Brep* brep,
		int eid,
		double t3d,
		const ON_SimpleArray<double>& t2d,
		int vid,
		bool bSetTrimBoxesAndFlags
)
{
	if ( eid > 0 )
	{
		// adjust eid from possible component index to true edge index
		const ON_BrepEdge* edge = brep->Edge(eid);
		if ( !edge || edge->m_edge_index < 0 )
			return false;
		eid = edge->m_edge_index;
	}
	if ( vid > 0 )
	{
		// adjust vid from possible component index to true edge index
		const ON_BrepVertex* vertex = brep->Vertex(vid);
		if ( !vertex || vertex->m_vertex_index < 0 )
			vid = -1;
		else
			vid = vertex->m_vertex_index;
	}

	if (vid == brep->m_E[eid].m_vi[0] || vid == brep->m_E[eid].m_vi[1])
		return true;
	if (t2d.Count() != brep->m_E[eid].m_ti.Count())
		return false;
	brep->m_E.Reserve(brep->m_E.Count() + 1);
	ON_BrepEdge& Edge = brep->m_E[eid];
	brep->m_T.Reserve(brep->m_T.Count()+t2d.Count());

	if (vid < 0){
		ON_3dPoint P = Edge.PointAt(t3d);
		ON_BrepVertex& NewV= brep->NewVertex(P);
		vid = NewV.m_vertex_index;
	}

	ON_BrepVertex& NewV= brep->m_V[vid];

	//new edge is to right of t3d
	ON_BrepEdge& NewE = brep->NewEdge(NewV, brep->m_V[Edge.m_vi[1]], Edge.m_c3i);
	NewE.ON_CurveProxy::operator = (Edge);
	ON_BrepVertex& OldV = brep->m_V[Edge.m_vi[1]];
	int i;
	for (i=0; i<OldV.EdgeCount(); i++){
		if (OldV.m_ei[i] == eid){
			OldV.m_ei.Remove(i);
			break;
		}
	}

	Edge.m_vi[1] = vid;
	NewV.m_ei.Append(eid);



	//NewE.m_domain.m_t[1] = Edge.m_domain[1];
	//Edge.m_domain.m_t[1] = t3d;
	//NewE.m_domain.m_t[0] = t3d;

	NewE.ON_CurveProxy::Trim(ON_Interval(t3d, Edge.Domain()[1]));
	Edge.ON_CurveProxy::Trim(ON_Interval(Edge.Domain()[0], t3d));

	/*
  double real_t3d = Edge.RealCurveParameter(t3d);
  ON_Interval NewE_dom( real_t3d,Edge.ProxyCurveDomain()[1]);
  ON_Interval Edge_dom( Edge.ProxyCurveDomain()[0],real_t3d);
  NewE.SetProxyCurveDomain( NewE_dom );
  Edge.SetProxyCurveDomain( Edge_dom );
  NewE.SetDomain( t3d,Edge.Domain()[1] );
  Edge.SetDomain( Edge.Domain()[0],t3d );
	 */

	NewE.m_tolerance = Edge.m_tolerance;

	for (i=0; i<Edge.m_ti.Count(); i++){
		ON_BrepTrim& T = brep->m_T[Edge.m_ti[i]];
		T.m_pline.Destroy();
		T.m_pbox.Destroy();
		if (T.m_bRev3d) T.m_vi[0] = vid;
		else T.m_vi[1] = vid;
		ON_BrepTrim& NewT = brep->NewTrim(NewE, T.m_bRev3d, T.m_c2i);
		NewT.m_pline.Destroy();
		NewT.m_pbox.Destroy();
		NewT.ON_CurveProxy::operator =(T);
		NewT.m_type = T.m_type;
		NewT.m_iso = T.m_iso;
		NewT.m_tolerance[0] = T.m_tolerance[0];
		NewT.m_tolerance[1] = T.m_tolerance[1];
		ON_BrepLoop& Loop = brep->m_L[T.m_li];
		Loop.m_pbox.Destroy();
		NewT.m_li = T.m_li;
		int tid = 0;
		int j;
		for (j=0; j<Loop.m_ti.Count(); j++){
			if (Loop.m_ti[j] == T.m_trim_index){
				tid = j;
				break;
			}
		}

		ON_Interval left(T.Domain()[0],t2d[i]);
		ON_Interval right(t2d[i],T.Domain()[1]);

		if (!T.m_bRev3d)
		{
			//NewT.m_t.m_t[1] = T.m_t[1];
			//T.m_t.m_t[1] = NewT.m_t.m_t[0] = t2d[i];
			T.Trim(left);
			NewT.Trim(right);

			//insert NewT in Loop after T
			Loop.m_ti.Insert(tid+1, NewT.m_trim_index);
		}
		else
		{
			//NewT.m_t.m_t[0] = T.m_t[0];
			//T.m_t.m_t[0] = NewT.m_t.m_t[1] = t2d[i];
			NewT.Trim(left);
			T.Trim(right);

			//insert NewT in Loop before T
			Loop.m_ti.Insert(tid, NewT.m_trim_index);
		}

		if (bSetTrimBoxesAndFlags && T.m_iso == ON_Surface::not_iso){
			brep->SetTrimIsoFlags(T);
			brep->SetTrimIsoFlags(NewT);
		}

	}

	if ( bSetTrimBoxesAndFlags )
	{
		for (i=0; i<Edge.m_ti.Count(); i++){
			ON_BrepLoop& L = brep->m_L[brep->m_T[Edge.m_ti[i]].m_li];
			brep->SetTrimBoundingBoxes(L, true);
		}
	}

	return true;
}
