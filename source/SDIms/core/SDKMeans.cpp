#include <SDIms/core/SDKMeans.hpp>

SDKMeans::SDKMeans() {
	m_ClusterNum	= 0;
}

void SDKMeans::setDataPoints(vector<Feature*>& data) {
	m_Data	= data;
	m_PtsNum	= m_Data.size();
	m_PtsLabels.resize(m_PtsNum);
	for(int i = 0; i < m_PtsNum; i++) {
		m_PtsLabels[i]	= 0;
	}
}

void SDKMeans::run(int maxClust, int maxLoopClust) {
	VectorXd isotropies(maxClust - 1);
	if(m_Data.size() > 1 && maxClust >= 2) {
		m_ClusterNum	= 1;
		generateCovarianceMatrix();

		int iPt, iClust, clustNum;
		vector<vector<int> > ptsLabels, centroids;
		vector<int> ptsLabelK;

		for(clustNum = 2; clustNum <= maxClust; clustNum++) {
			vector<int> cents;
			if(getCentroids(clustNum, cents)) {
				isotropies(clustNum - 2)	= 0.0;
				for(int loop = 0; loop < maxLoopClust; loop++) {
					double loopIso	= 0.0;
					MatrixXd matrix(clustNum, m_PtsNum);
					estimateDistPointsCentroids(cents, matrix);

					// Get cluster label for points & calculate isotropies of k clusters
					VectorXd minVal = matrix.colwise().minCoeff();
					ptsLabelK.clear();
					for(iPt = 0; iPt < m_PtsNum; iPt++) {
						for(iClust = 0; iClust < clustNum; iClust++) {
							if(MathExt::equal(matrix(iClust, iPt), minVal(iPt))) {
								ptsLabelK.push_back(iClust);
								loopIso	+= m_CovMatrix(iPt, cents[iClust]);
								break;
							}
						}
					}

					// Test for halt
					if(loop > 0) {
						if(fabs(loopIso - isotropies(clustNum - 2)) < 0.1) {
							isotropies(clustNum - 2)	= loopIso;
							break;
						} else {
							// update new cents
							cents.clear();
							for(iClust = 0; iClust < clustNum; iClust++) {
								vector<int> ptsIdx;
								for(iPt = 0; iPt < m_PtsNum; iPt++) {
									if(ptsLabelK[iPt] == iClust) {
										ptsIdx.push_back(iPt);
									}
								}
								int n = ptsIdx.size(), i, j;
								MatrixXd covMat(n, n);
								for(i = 0; i < n; i++) {
									covMat(i,i) = 0.0;
									for(j = i + 1; j < n; j++) {
										covMat(i,j) = m_CovMatrix(ptsIdx[i], ptsIdx[j]);
									}
								}
								VectorXd sumVal = covMat.colwise().sum();
								VectorXd::Index minIdx;
								sumVal.minCoeff(&minIdx);
								cents.push_back(ptsIdx[minIdx]);
							}
						}
					}
					isotropies(clustNum - 2)	= loopIso;
				} // for loop
				ptsLabels.push_back(ptsLabelK);
				centroids.push_back(cents);
			} else {
				// cannot get k separated cents
				break;
			}
		} // for clustNum

		int finalClustNum	= clustNum - 1;

		// get final result
		if(finalClustNum >= 2) {
			for(clustNum = 2; clustNum < finalClustNum; clustNum++) {
				isotropies(clustNum - 2) = isotropies(clustNum - 2) / clustNum;
			}
			VectorXd::Index minIdx;
			isotropies.head(finalClustNum - 2).minCoeff(&minIdx);

			m_ClusterNum	= minIdx + 2;
			for(iPt = 0; iPt < m_PtsNum; iPt++) {
				m_PtsLabels(iPt)	= ptsLabels[minIdx][iPt];
			}
			m_CentroidsIdx.resize(m_ClusterNum);
			for(iPt = 0; iPt < m_ClusterNum; iPt++) {
				m_CentroidsIdx(iPt)	= centroids[minIdx][iPt];
			}
		}
	}

	cout << "Classnum = " << m_ClusterNum << endl;
	cout << "Isotropy = " << isotropies.transpose() << endl;
	cout << m_PtsLabels.transpose() << endl;
}

void SDKMeans::generateCovarianceMatrix() {
	int nd	= m_Data.size();
	if(nd > 1) {
		m_CovMatrix.resize(nd, nd);
		for(int i = 0; i < nd; i++) {
			m_CovMatrix(i,i) = 0.0;
			for(int j = i + 1; j < nd; j++) {
				m_CovMatrix(i,j)	= m_Data[i]->distanceTo(m_Data[j]);
				m_CovMatrix(j,i)	= m_CovMatrix(i,j);
			}
		}

		cout << "Cov = " << m_CovMatrix << endl;
	}
}

bool SDKMeans::getCentroids(int k, vector<int>& centroids) {
	centroids.clear();
	vector<int> centIdx;
	int i, j;
	shuffle(centIdx, m_PtsNum);
	centroids.push_back(centIdx[0]);
	for(i = 1; i < m_PtsNum; i++) {
		if((int)centroids.size() == k) break;
		bool flag = true;
		for(j = 0; j < (int)centroids.size(); j++) {
			if(m_CovMatrix(centIdx[i],centroids[j]) <= 0.1) {
				flag = false;
				break;
			}
		}
		if(flag)
			centroids.push_back(centIdx[i]);
	}

	return ((int)centroids.size() == k);
}

void SDKMeans::estimateDistPointsCentroids(vector<int>& centroids, MatrixXd& matrix) {
	int i, j, nc = centroids.size();
	for(i = 0; i < nc; i++) {
		for(j = 0; j < m_PtsNum; j++) {
			matrix(i,j)	= m_CovMatrix(j, centroids[i]);
		}
	}
}

void SDKMeans::setIsometryClass(IsometryClasses c) {
	m_IsomClass	= c;
}

int SDKMeans::getClustersNumber() {
	return m_ClusterNum;
}

int SDKMeans::getClusterPtsIndices(int iClust, vector<int>& idx) {
	idx.clear();
	if(m_ClusterNum > 1 && iClust >= 0 && iClust < m_ClusterNum) {
		for(int i = 0; i < m_PtsNum; i++) {
			if(m_PtsLabels(i) == iClust) {
				idx.push_back(i);
			}
		}
		return idx.size();
	} else {
		return 0;
	}
}

int SDKMeans::getClusterCentroidIndex(int iClust) {
	if(iClust >= 0 && iClust < m_ClusterNum)
		return m_CentroidsIdx[iClust];
	else
		return -1;
}

void SDKMeans::shuffle(vector<int>& array, size_t n) {
	if (n > 1) {
		array.clear();
		size_t i;
		for(i = 0; i < n; i++) {
			array.push_back(i);
		}
		for (i = 0; i < n - 1; i++) {
			size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
			int t = array[j];
			array[j] = array[i];
			array[i] = t;
		}
	}
}
