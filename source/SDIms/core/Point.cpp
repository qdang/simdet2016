/*
 * Point.cpp
 *
 *  Created on: Oct 8, 2013
 *      Author: dqviet
 */

#include "SDIms/core/Point.hpp"

SDIms::Frame::Frame() {}

SDIms::Frame::Frame(double* normal, double* maxTangent,
		double* minTangent) {
	if(normal && maxTangent && minTangent) {
		m_Normal << normal[0], normal[1], normal[2];
		m_MaxTangent << maxTangent[0], maxTangent[1], maxTangent[2];
		m_MinTangent << minTangent[0], minTangent[1], minTangent[2];
		updateMatrix();
	}
}

SDIms::Frame::Frame(Vec3& normal, Vec3& maxTangent, Vec3& minTangent) {
	m_Normal		= normal;
	m_MaxTangent	= maxTangent;
	m_MinTangent	= minTangent;
	updateMatrix();
}

SDIms::Frame::Frame(const Frame& other) {
	m_Normal		= other.m_Normal;
	m_MaxTangent	= other.m_MaxTangent;
	m_MinTangent	= other.m_MinTangent;
	updateMatrix();
}

Frame& SDIms::Frame::operator =(const Frame& other) {
	m_Normal		= other.m_Normal;
	m_MaxTangent	= other.m_MaxTangent;
	m_MinTangent	= other.m_MinTangent;
	updateMatrix();
	return *this;
}

Vec3& SDIms::Frame::getMaxTangent() {
	return m_MaxTangent;
}

Vec3& SDIms::Frame::getMinTangent() {
	return m_MinTangent;
}

Vec3& SDIms::Frame::getNormal() {
	return m_Normal;
}

Matrix3d& SDIms::Frame::getMatrix() {
	return m_Matrix;
}

void SDIms::Frame::set(double* normal, double* maxTangent, double* minTangent) {
	m_Normal << normal[0], normal[1], normal[2];
	m_MaxTangent << maxTangent[0], maxTangent[1], maxTangent[2];
	m_MinTangent << minTangent[0], minTangent[1], minTangent[2];
	updateMatrix();
}

void SDIms::Frame::setMaxTangent(const Vec3& maxTangent) {
	m_MaxTangent	= maxTangent;
	updateMatrix();
}

void SDIms::Frame::setMinTangent(const Vec3& minTangent) {
	m_MinTangent	= minTangent;
	updateMatrix();
}

void SDIms::Frame::setNormal(const Vec3& normal) {
	m_Normal	= normal;
	updateMatrix();
}

string SDIms::Frame::toString() {
	ostringstream oss;
	oss << "[" << m_Matrix << "]";
	return oss.str();
}

void SDIms::Frame::updateMatrix() {
	m_Matrix	<< 	m_Normal,
				 	m_MaxTangent,
				 	m_MinTangent;
	m_Matrix.transposeInPlace();
	// std::cout << "Frame = " << m_Matrix << endl;
}

SDIms::Frame::~Frame() {}

SDIms::Point::Point() {}

SDIms::Point::~Point() {}

SDIms::Point::Point(double* coord, double* normal, double* maxTangent,
		double* minTangent) {
	if(coord)
		m_Coord 	<< coord[0], coord[1], coord[2];
	m_Frame.set(normal, maxTangent, minTangent);
}

void SDIms::Point::set(Vec3& coord, Vec3& normal, Vec3& maxTangent, Vec3& minTangent) {
	m_Coord	= coord;
	m_Frame	= Frame(normal, maxTangent, minTangent);
}

void SDIms::Point::set(double* coord, double* normal, double* maxTangent,
		double* minTangent) {
	if(coord)
		m_Coord 	<< coord[0], coord[1], coord[2];
	m_Frame.set(normal, maxTangent, minTangent);
}

SDIms::Point::Point(Vec3& coord, Vec3& normal, Vec3& maxTangent,
		Vec3& minTangent) {
	m_Coord	= coord;
	m_Frame	= Frame(normal, maxTangent, minTangent);
}

SDIms::Point::Point(Vec3& coord, Frame& frame) {
	m_Coord	= coord;
	m_Frame	= frame;
}

SDIms::Point::Point(const Point& other) {
	m_Coord	= other.m_Coord;
	m_Frame	= other.m_Frame;
}

Point& SDIms::Point::operator =(const Point& other) {
	m_Coord	= other.m_Coord;
	m_Frame	= other.m_Frame;
	return *this;
}

string SDIms::Point::toString() {
	ostringstream oss;
	oss << "Coord = [" << m_Coord.transpose() << "]" << endl;
	//oss << "Frame = " << m_Frame.toString();
	return oss.str();
}

Vec3& SDIms::Point::getCoord() {
	return m_Coord;
}

Frame& SDIms::Point::getFrame() {
	return m_Frame;
}

void SDIms::Point::setCoord(const Coord3& coord) {
	m_Coord	= coord;
}

void SDIms::Point::setFrame(const Frame& frame) {
	m_Frame	= frame;
}

void SDIms::Point::setFrame(Vec3& normal, Vec3& maxTangent, Vec3& minTangent) {
	m_Frame.set(normal, maxTangent, minTangent);
}

void SDIms::Frame::set(Vec3& normal, Vec3& maxTangent, Vec3& minTangent) {
	m_Normal		= normal;
	m_MaxTangent	= maxTangent;
	m_MinTangent	= minTangent;
}

void SDIms::Frame::normalizeCoord(const Vec3& input, Vec3& output) {
	output << MathExt::scalarProjection(input, m_MaxTangent),
			MathExt::scalarProjection(input, m_MinTangent), 0;
}
