/*
 * Isometry.cpp
 *
 *  Created on: Oct 8, 2013
 *      Author: dqviet
 */

#include "SDIms/core/Isometry.hpp"

double SDIms::Isometry::model_BBoxDiagonal	= 1;

SDIms::Isometry::Isometry() {
	m_Feature		= NULL;
	m_P1			= NULL;
	m_P2			= NULL;
	m_EigValT1Num	= 0;
	m_EigValT_1Num	= 0;
}


SDIms::Isometry::Isometry(const Isometry& other) {
	m_P1	= other.m_P1;
	m_P2	= other.m_P2;
	m_Class	= other.m_Class;
	m_Type	= other.m_Type;
	m_Feature	= other.m_Feature;
	m_T		= other.m_T;
	m_G		= other.m_G;
	m_ta	= other.m_ta;
	m_FixedPoint	= other.m_FixedPoint;
	m_EigSpaceT		= other.m_EigSpaceT;
	m_EigValT1Idx	= other.m_EigValT1Idx;
	m_EigValT1Num	= other.m_EigValT1Num;
	m_EigValT_1Idx	= other.m_EigValT_1Idx;
	m_EigValT_1Num	= other.m_EigValT_1Num;
	m_BBDiagonal	= other.m_BBDiagonal;
	m_EigValT		= other.m_EigValT;
}


SDIms::Isometry& SDIms::Isometry::operator =(const Isometry& other) {
	m_P1	= other.m_P1;
	m_P2	= other.m_P2;
	m_Class	= other.m_Class;
	m_Type	= other.m_Type;
	m_Feature	= other.m_Feature;
	m_T		= other.m_T;
	m_G		= other.m_G;
	m_ta	= other.m_ta;
	m_FixedPoint	= other.m_FixedPoint;
	m_EigSpaceT		= other.m_EigSpaceT;
	m_EigValT1Idx	= other.m_EigValT1Idx;
	m_EigValT1Num	= other.m_EigValT1Num;
	m_EigValT_1Idx	= other.m_EigValT_1Idx;
	m_EigValT_1Num	= other.m_EigValT_1Num;
	m_BBDiagonal	= other.m_BBDiagonal;
	m_EigValT		= other.m_EigValT;
	return *this;
}


SDIms::Isometry::~Isometry() {
	m_Feature	= NULL;
	m_P1		= NULL;
	m_P2		= NULL;
}


void SDIms::Isometry::init(Point* p1, Point* p2) {
	m_P1	= p1;
	m_P2	= p2;
	m_ta.Zero();m_EigValT1Idx.Zero();m_EigValT_1Idx.Zero();
	evalTransformation();
	evalInvariants(m_T);
	if(m_EigValT1Num < 3)
		evalFixedPoints();
	classify();
}

void SDIms::Isometry::init(Point* p1, Point* p2, Matrix4d& transformation) {
	m_P1	= p1;
	m_P2	= p2;
	//m_T		= transformation;
	for(int i = 0; i < 4; i++) {
		for(int j = 0; j < 4; j++) {
			if(fabs(transformation(i,j)) < SDIms_ZERO_TOL) {
				m_T(i,j) = 0.0;
			} else {
				m_T(i,j) = transformation(i,j);
			}
		}
	}
	Block<Matrix4d, 3, 3> rotation = m_T.topLeftCorner<3,3>();
	//cout << "R = " << rotation << endl;
	//cout << "det(R) = " << rotation.determinant() << endl;
	if(MathExt::equal(rotation.determinant(), 1.0)) {
		m_Type	= DIRECT;
	} else {
		m_Type	= INDIRECT;
	}
	m_ta.setZero();m_EigValT1Idx.setZero();m_EigValT_1Idx.setZero();

	evalInvariants(m_T);
	if(evalTranslation()) {	// il existe t_a
		Matrix4d tmp;
		tmp.setZero(); tmp.Identity();
		tmp.block(0, 3, 3, 1) 	= m_ta;
		tmp.inverse();
		//m_G.block(0, 3, 3, 1)	-= m_ta;
		m_G	= m_T * tmp;
		evalInvariants(m_G);
	} else {
		m_G	= m_T;
	}

	// TODO: implémentation du calcul des points fixes.

	/*if(m_EigValT1Num < 3)
		evalFixedPoints();*/
	classify();
}

double	SDIms::Isometry::distanceTo(Isometry& other) {
	return distanceTo(&other);
}

double	SDIms::Isometry::distanceTo(Isometry* other) {
	if(other) {
		return m_Feature->distanceTo(other->m_Feature);
	} else {
		return -1;
	}
}

int SDIms::Isometry::TOneEigSpaceSize() {
	return m_EigValT1Num;
}

int SDIms::Isometry::TMinusOneEigSpaceSize() {
	return m_EigValT_1Num;
}

void SDIms::Isometry::evalTransformation() {
	Matrix3d f1		= m_P1->getFrame().getMatrix();
	Matrix3d f2T 	= m_P2->getFrame().getMatrix().transpose().eval();
	m_T.setZero();m_T(3,3) = 1;
	Block<Matrix4d, 3, 3> rotation = m_T.topLeftCorner<3,3>();
	rotation	= f1 * f2T;

	//Matrix3d f1T 	= m_P1->getFrame().getMatrix().transpose().eval();
	//rotation = f1T * f1 * f2T * f1;

	m_T.topRightCorner<3,1>()	= m_P2->getCoord() -
			rotation * m_P1->getCoord();

	if(MathExt::equal(rotation.determinant(), 1.0)) {
		m_Type	= DIRECT;
	} else {
		m_Type	= INDIRECT;
	}
}

bool SDIms::Isometry::evalInvariants(Matrix4d& mat) {
	m_EigValT1Idx.setZero();
	m_EigValT1Num	= 0;
	m_EigValT_1Idx.setZero();
	m_EigValT_1Num	= 0;
	EigenSolver<Matrix3d> eigSolver(m_T.topLeftCorner<3,3>());
	if(eigSolver.info() == Success) {
		// cout << "Eigenvalues = " << eigSolver.eigenvalues().transpose() << endl;
		// cout << "Eigenvectors = " << eigSolver.eigenvectors() << endl;
		Matrix3Xcd eigVals 	= eigSolver.eigenvalues();
		//cout << "Eigen vals = " << eigVals << endl;
		m_EigSpaceT	= eigSolver.eigenvectors();
		m_EigValT	= eigSolver.eigenvalues();

		for(int i = 0; i < 3; i++) {
			complex<double> row	= eigVals(i,0);
			//cout << "Eigenvalue " << i << " = " << real(row)
			//		<< " + i*" << imag(row) << endl;
			if(MathExt::equal(real(row), 1.0)) {
				m_EigValT1Idx(m_EigValT1Num++)	= i;
			} else if (MathExt::equal(real(row), -1.0)) {
				m_EigValT_1Idx(m_EigValT_1Num++)	= i;
			}
		}
		return true;
	} else {
		// cout << "Failed at computing eigen system" << endl;
		return false;
	}
}

bool SDIms::Isometry::evalTranslation() {
	m_ta.setZero();
	Vec3 p1p2 = m_P2->getCoord() - m_P1->getCoord();
	for(int i = 0; i < 3; i++) {
		if(fabs(p1p2(i)) < SDIms_ZERO_TOL)
			p1p2(i)	= 0.0;
	}
	if(p1p2.isZero()) return false;
	Vec3 v	= p1p2.normalized();
	//cout << "v = " << v.transpose() << endl;
	if(MathExt::is_nan(v)) return false;

	if(TOneEigSpaceSize() == 0) {
		return false;
	} else if (TOneEigSpaceSize() == 1) {
		Vec3 line;
		line << real(m_EigSpaceT(0, m_EigValT1Idx(0))),
				real(m_EigSpaceT(1, m_EigValT1Idx(0))),
				real(m_EigSpaceT(2, m_EigValT1Idx(0)));
		MathExt::vectorProjection(p1p2, line, v);
		m_ta = MathExt::scalarProjection(p1p2, v) * p1p2;
		if(MathExt::equal(m_ta.norm(), 0.0)) {
			m_ta.setZero();
			return false;
		} else {
			return true;
		}
	} else if (TOneEigSpaceSize() == 2) {
		Vec3 t1, t2;
		t1 << real(m_EigSpaceT(0, m_EigValT1Idx(0))),
				real(m_EigSpaceT(1, m_EigValT1Idx(0))),
				real(m_EigSpaceT(2, m_EigValT1Idx(0)));
		t2 << real(m_EigSpaceT(0, m_EigValT1Idx(1))),
				real(m_EigSpaceT(1, m_EigValT1Idx(1))),
				real(m_EigSpaceT(2, m_EigValT1Idx(1)));
		//cout << "t1 = " << t1.transpose() << endl;
		//cout << "t2 = " << t2.transpose() << endl;
		Vec3 normal;
		if(t1.isApprox(t2)) {
			normal << real(m_EigSpaceT(0, m_EigValT_1Idx(0))),
					real(m_EigSpaceT(1, m_EigValT_1Idx(0))),
					real(m_EigSpaceT(2, m_EigValT_1Idx(0)));
		} else {
			normal	= t1.cross(t2);
		}
		normal.normalize();
		return MathExt::lineSegmentOnPlaneProjection(normal,
				m_P1->getCoord(), m_P1->getCoord(), m_P2->getCoord(), m_ta);
	} else {
		m_ta	= p1p2;
	}

	for(int i = 0; i < 3; i++) {
		if(fabs(m_ta(i)) < SDIms_ZERO_TOL) {
			m_ta(i) = 0.0;
		}
	}

	//cout << "calculating m_ta = " << m_ta.transpose() << endl;

	return true;
}

void SDIms::Isometry::evalFixedPoints() {
	// Trouver x tel que ($f$ - IdX) $Ox$ = $f(O)O$		($f$ est le vecteur f)
	// => ($f$ - IdX) x = $f(O)O$ + ($f$ - IdX) O
	// suppose A = ($f$ - IdX)
	//		et B = $f(O)O$ + ($f$ - IdX) O
	if(m_EigValT1Num == 2) {
		m_FixedPoint	= (m_P1->getCoord() + m_P2->getCoord()) / 2.0;
	} else {
		Matrix3d A;
		A.setIdentity();
		A *= -1; A += m_G.block(0,0,3,3);
		Vec3 B = (m_P1->getCoord() - m_P2->getCoord()) + A * m_P1->getCoord();
		//Vec3 B; B.Zero();
		//B -= m_G.block(0,3,3,1);
		m_FixedPoint	= A.colPivHouseholderQr().solve(B);
	}
	for(int i = 0; i < 3; i++) {
		if(fabs(m_FixedPoint(i)) < SDIms_ZERO_TOL)
			m_FixedPoint(i) = 0.0;
	}
}

void SDIms::Isometry::classify() {
	if(m_ta.isZero(SDIms_ZERO_TOL)) {
		if(m_EigValT1Num == 0) {
			// symétrie + rotation
			m_Class		= SYMMETRY_ROTATION;
			m_Feature	= new FeatureSymRot(this);
		} else if (m_EigValT1Num == 1) {
			// rotation
			m_Class		= AXIS_ROTATION;
			m_Feature	= new FeatureRot(this);
			if(m_Class == IDENTITY) {
				delete m_Feature;
				m_Feature	= NULL;
			}
		} else if (m_EigValT1Num == 2) {
			// symétrie
			m_Class		= SYMMETRY;
			m_Feature	= new FeatureSym(this);
		} else {
			m_Class		= IDENTITY;
			m_Feature	= NULL;
		}
	} else {
		if(m_EigValT1Num == 1) {
			// rotation + translation
			m_Class		= VISSAGE;
			m_Feature	= new FeatureViss(this);
		} else if(m_EigValT1Num == 2) {
			// symétrie + translation
			m_Class		= SYMMETRY_TRANSLATION;
			m_Feature	= new FeatureSymTrans(this);
		} else if(m_EigValT1Num == 3) {
			m_Class		= TRANSLATION;
			m_Feature	= new FeatureTrans(this);
		} else {
			m_Class		= UNKNOWN;
			m_Feature	= NULL;
		}
	}
}

SDIms::Feature::Feature() {
	m_featureSize	= 0;
}

SDIms::Feature::Feature(Isometry* isometry) {
	m_featureSize	= 0;
	m_isometry		= isometry;
	m_Class			= isometry->m_Class;
}

SDIms::Feature::Feature(const Feature& other) {
	m_featureSize	= other.m_featureSize;
	m_isometry		= other.m_isometry;
	m_Class			= other.m_Class;
}

Feature& SDIms::Feature::operator=(const Feature& other) {
	m_featureSize	= other.m_featureSize;
	m_isometry		= other.m_isometry;
	m_Class			= other.m_Class;
	return *this;
}

SDIms::Feature::~Feature() {
}

double	SDIms::Feature::distanceTo(Feature* other) {
	if(other && m_Class == other->m_Class) {
		if(m_Class == SDIms::SYMMETRY) {
			FeatureSym *fs1 = dynamic_cast<FeatureSym*>(this),
					*fs2	= dynamic_cast<FeatureSym*>(other);
			return fs1->distanceTo(fs2);
		} else if(m_Class == SDIms::TRANSLATION) {
			FeatureTrans *ft1 = dynamic_cast<FeatureTrans*>(this),
					*ft2	= dynamic_cast<FeatureTrans*>(other);
			return ft1->distanceTo(ft2);
		} else if (m_Class == SDIms::AXIS_ROTATION) {
			FeatureRot *fr1 = dynamic_cast<FeatureRot*>(this),
					*fr2	= dynamic_cast<FeatureRot*>(other);
			return fr1->distanceTo(fr2);
		} else if (m_Class == SDIms::VISSAGE) {
			FeatureViss *fv1 = dynamic_cast<FeatureViss*>(this),
					*fv2	= dynamic_cast<FeatureViss*>(other);
			return fv1->distanceTo(fv2);
		} else if (m_Class == SDIms::SYMMETRY_ROTATION) {
			FeatureSymRot *fsr1 = dynamic_cast<FeatureSymRot*>(this),
					*fsr2	= dynamic_cast<FeatureSymRot*>(other);
			return fsr1->distanceTo(fsr2);
		} else if (m_Class == SDIms::SYMMETRY_TRANSLATION) {
			FeatureSymTrans *fst1 = dynamic_cast<FeatureSymTrans*>(this),
					*fst2	= dynamic_cast<FeatureSymTrans*>(other);
			return fst1->distanceTo(fst2);
		} else {
			return -1;
		}
	} else {
		return -1;
	}
}

string SDIms::Feature::info() {
	ostringstream oss;
	if(m_Class == SDIms::SYMMETRY) {
		FeatureSym *fs1 = dynamic_cast<FeatureSym*>(this);
		oss << "Normale du plan = " << fs1->m_PlaneNormal.transpose() << endl;
		oss << "Point du plan = " << fs1->m_PlanePoint.transpose() << endl;
	}else if(m_Class == AXIS_ROTATION) {
		//cout << "BBox = " << m_BBDiagonal << endl;
		FeatureRot *fr1 = dynamic_cast<FeatureRot*>(this);
		oss << "Rotation Axis = " << fr1->m_Axis.transpose() << endl;
		oss << "Origin = " << fr1->m_Origin.transpose() << endl;
		oss << "Angle = " << fr1->m_Angle << endl;
	} else if(m_Class == TRANSLATION) {
		FeatureTrans *ft1 = dynamic_cast<FeatureTrans*>(this);
		oss << "Translation vector = " << ft1->m_Translation.transpose() << endl;
	} else if(m_Class == VISSAGE) {
		FeatureViss *fv1 = dynamic_cast<FeatureViss*>(this);
		oss << "Rotation Axis = " << fv1->m_Axis.transpose() << endl;
		oss << "Origin = " << fv1->m_Origin.transpose() << endl;
		oss << "Angle = " << fv1->m_Angle << endl;
		oss << "Translation vector = " << fv1->m_Translation.transpose() << endl;
	} else if(m_Class == SYMMETRY_ROTATION) {
		FeatureSymRot *fsr1 = dynamic_cast<FeatureSymRot*>(this);
		oss << "Plane normale = " << fsr1->m_PlaneNormal.transpose() << endl;
		oss << "Plane point = " << fsr1->m_PlanePoint.transpose() << endl;
		oss << "Rotation Axis = " << fsr1->m_Axis.transpose() << endl;
		oss << "Origin = " << fsr1->m_Origin.transpose() << endl;
		oss << "Angle = " << fsr1->m_Angle << endl;
	} else if(m_Class == SYMMETRY_TRANSLATION) {
		FeatureSymTrans *fst1 = dynamic_cast<FeatureSymTrans*>(this);
		oss << "Plane normale = " << fst1->m_PlaneNormal.transpose() << endl;
		oss << "Plane point = " << fst1->m_PlanePoint.transpose() << endl;
		oss << "Translation vector = " << fst1->m_Translation.transpose() << endl;
	}

	return oss.str();
}

SDIms::FeatureRot::FeatureRot(Isometry* isometry) :
	Feature(isometry) {
	m_featureSize	= 4;
	// Information sur la rotation axiale
	Matrix3d vMat	= isometry->m_G.block(0, 0, 3, 3);
	double vCos	= (vMat.trace() - 1) / 2;
	if(vCos == 1) {
		isometry->m_Class	= IDENTITY;
	} else {
		m_Angle	= acos((vMat.trace() - 1) / 2);
		m_Axis	<<	real(isometry->m_EigSpaceT(0, isometry->m_EigValT1Idx[0])),
				real(isometry->m_EigSpaceT(1, isometry->m_EigValT1Idx[0])),
				real(isometry->m_EigSpaceT(2, isometry->m_EigValT1Idx[0]));

		// Trouver un point sur l'axe de rotation (optionel)
		Vec3 M = (isometry->m_P1->getCoord() + isometry->m_P2->getCoord()) / 2.0;
		double d1, d2, d3;
		d1	= (M - isometry->m_P1->getCoord()).norm();
		d3	= d1 / sin(m_Angle/2);
		d2	= sqrt(d3 * d3 - d1 * d1);
		// C ~ m_Origin
		Vec3 MC	= m_Axis.cross((isometry->m_P1->getCoord() - isometry->m_P2->getCoord()).normalized());
		m_Origin	= M + d2 * MC;
	}
}

SDIms::FeatureRot::FeatureRot(
		const FeatureRot& other) : Feature(other){
	m_Angle	= other.m_Angle;
	m_Axis	= other.m_Axis;
	m_Origin	= other.m_Origin;
}

SDIms::FeatureRot::~FeatureRot() {
}

double SDIms::FeatureRot::distanceTo(FeatureRot& other) {
	return distanceTo(&other);
}

double SDIms::FeatureRot::distanceTo(FeatureRot* other) {
	double d 	= 1 - m_Axis.dot(other->m_Axis);
	d	+= fabs(m_Angle - other->m_Angle) / (2 * M_PI);
	/*Vec3 pro1, pro2;
	MathExt::pointOnLineProjection(other->m_Axis, other->m_Origin, m_Origin, pro1);
	d	+= (pro1 - m_Origin).norm() / Isometry::model_BBoxDiagonal;
	MathExt::pointOnLineProjection(m_Axis, m_Origin, other->m_Origin, pro2);
	d	+= (pro2 - other->m_Origin).norm() / Isometry::model_BBoxDiagonal;*/

	return d;
}

SDIms::FeatureTrans::FeatureTrans(
		Isometry* isometry) : Feature(isometry) {
	m_featureSize	= 3;
	m_Translation	= isometry->m_ta;
}

SDIms::FeatureTrans::FeatureTrans(
		const FeatureTrans& other) : Feature(other) {
	m_Translation	= other.m_Translation;
}

SDIms::FeatureTrans::~FeatureTrans() {
}

double SDIms::FeatureTrans::distanceTo(FeatureTrans& other) {
	return distanceTo(&other);
}

double SDIms::FeatureTrans::distanceTo(FeatureTrans* other) {
	double normTa1	= m_Translation.norm(),
			normTa2	= other->m_Translation.norm();
	double d = 1 - ((m_Translation.dot(other->m_Translation))
			/ (normTa1 * normTa2));
	d += fabs(normTa1 - normTa2) / ((normTa1 > normTa2) ? normTa1 : normTa2);

	return d;
}

SDIms::FeatureSym::FeatureSym(Isometry* isometry)
	: Feature(isometry) {
	m_featureSize	= 6;
	m_PlanePoint	= (isometry->m_P1->getCoord() + isometry->m_P2->getCoord()) / 2.0;
	if(isometry->m_EigValT_1Num == 1) {
		// dim(E(1,f)) = 0, dim(E(-1,f)) = 1, le plan a la normale de direction
		// du vecteur propre associé à la valeur propre -1
		m_PlaneNormal	<< real(m_isometry->m_EigSpaceT(0, m_isometry->m_EigValT_1Idx(0))),
				real(m_isometry->m_EigSpaceT(1, m_isometry->m_EigValT_1Idx(0))),
				real(m_isometry->m_EigSpaceT(2, m_isometry->m_EigValT_1Idx(0)));
	} else {
		// dim(E(1,f)) = 2, le plan est défini par deux vecteurs propres associés
		// à la valeur propre 1
		Vec3 t1, t2;
		t1	<< real(m_isometry->m_EigSpaceT(0, m_isometry->m_EigValT1Idx(0))),
				real(m_isometry->m_EigSpaceT(1, m_isometry->m_EigValT1Idx(0))),
				real(m_isometry->m_EigSpaceT(2, m_isometry->m_EigValT1Idx(0)));
		t2	<< real(m_isometry->m_EigSpaceT(0, m_isometry->m_EigValT1Idx(1))),
				real(m_isometry->m_EigSpaceT(1, m_isometry->m_EigValT1Idx(1))),
				real(m_isometry->m_EigSpaceT(2, m_isometry->m_EigValT1Idx(1)));
		m_PlaneNormal	= t1.cross(t2);
	}
	for(int i = 0; i < 3; i++) {
		if(fabs(m_PlaneNormal(i)) < SDIms_ZERO_TOL) {
			m_PlaneNormal(i)	= 0.0;
		}
	}
}

SDIms::FeatureSym::FeatureSym(
		const FeatureSym& other) : Feature(other) {
	m_PlaneNormal	= other.m_PlaneNormal;
	m_PlanePoint	= other.m_PlanePoint;
	m_GLA			= other.m_GLA;
	m_GLB			= other.m_GLB;
	m_GLC			= other.m_GLC;
	m_GLD			= other.m_GLD;
}

SDIms::FeatureSym::~FeatureSym() {
}

double SDIms::FeatureSym::distanceTo(FeatureSym& other) {
	return distanceTo(&other);
}

double SDIms::FeatureSym::distanceTo(FeatureSym* other) {
	double d = (1 - fabs(m_PlaneNormal.dot(other->m_PlaneNormal)));
	d	+= fabs(MathExt::distancePointPlane(m_PlanePoint, other->m_PlaneNormal, other->m_PlanePoint) / Isometry::model_BBoxDiagonal);
	d	+= fabs(MathExt::distancePointPlane(other->m_PlanePoint, m_PlaneNormal, m_PlanePoint) / Isometry::model_BBoxDiagonal);
	return d;
}

SDIms::FeatureViss::FeatureViss(Isometry* isometry)
	: Feature(isometry), FeatureRot(isometry),
	  FeatureTrans(isometry) {
	FeatureRot::m_featureSize	= 6;
}

SDIms::FeatureViss::FeatureViss(
		const FeatureViss& other) :
	Feature(other), FeatureRot(other), FeatureTrans(other) {
}

SDIms::FeatureViss::~FeatureViss() {
}

double SDIms::FeatureViss::distanceTo(FeatureViss& other) {
	return distanceTo(&other);
}

double SDIms::FeatureViss::distanceTo(FeatureViss* other) {
	double normTa1	= m_Translation.norm(),
			normTa2	= other->m_Translation.norm();
	// translation
	double d = (1 - fabs(m_Translation.dot(other->m_Translation)))
				/ (normTa1 * normTa2);
	d += fabs(normTa1 - normTa2) / ((normTa1 > normTa2) ? normTa1 : normTa2);

	// rotation
	d	+= fabs(m_Angle - other->m_Angle) / (2 * M_PI);
	Vec3 pro1, pro2;
	MathExt::pointOnLineProjection(other->m_Axis, other->m_Origin, m_Origin, pro1);
	d	+= (pro1 - m_Origin).norm() / Isometry::model_BBoxDiagonal;
	MathExt::pointOnLineProjection(m_Axis, m_Origin, other->m_Origin, pro2);
	d	+= (pro2 - other->m_Origin).norm() / Isometry::model_BBoxDiagonal;

	return d;
}

SDIms::FeatureSymRot::FeatureSymRot(
		Isometry* isometry) :
	Feature(isometry), FeatureSym(isometry), FeatureRot(isometry){
}

SDIms::FeatureSymRot::FeatureSymRot(
		const FeatureSymRot& other) :
	Feature(other), FeatureSym(other), FeatureRot(other) {
}

SDIms::FeatureSymRot::~FeatureSymRot() {
}

double SDIms::FeatureSymRot::distanceTo(FeatureSymRot& other) {
	return distanceTo(&other);
}

double SDIms::FeatureSymRot::distanceTo(FeatureSymRot* other) {
	return -1;
}

SDIms::FeatureSymTrans::FeatureSymTrans(
		Isometry* isometry) :
	Feature(isometry), FeatureSym(isometry), FeatureTrans(isometry) {
}

SDIms::FeatureSymTrans::FeatureSymTrans(
		const FeatureSymTrans& other) :
	Feature(other), FeatureSym(other), FeatureTrans(other) {
}

SDIms::FeatureSymTrans::~FeatureSymTrans() {
}

double SDIms::FeatureSymTrans::distanceTo(FeatureSymTrans& other) {
	return distanceTo(&other);
}

double SDIms::FeatureSymTrans::distanceTo(FeatureSymTrans* other) {
	double d = (1 - fabs(m_PlaneNormal.dot(other->m_PlaneNormal)));
	d	+= fabs(MathExt::distancePointPlane(m_PlanePoint, other->m_PlaneNormal, other->m_PlanePoint)
			/ Isometry::model_BBoxDiagonal);
	d	+= fabs(MathExt::distancePointPlane(other->m_PlanePoint, m_PlaneNormal, m_PlanePoint)
			/ Isometry::model_BBoxDiagonal);
	double normTa1	= m_Translation.norm(),
			normTa2	= other->m_Translation.norm();
	d += fabs(normTa1 - normTa2) / ((normTa1 > normTa2) ? normTa1 : normTa2);
	return d;
}

SDIms::FeatureCSym::FeatureCSym(Isometry* isometry) :
	Feature(isometry) {
	m_featureSize	= 3;
}

SDIms::FeatureCSym::FeatureCSym(
		const FeatureCSym& other) : Feature(other) {
	m_Center	= other.m_Center;
}

SDIms::FeatureCSym::~FeatureCSym() {
	m_Center	= NULL;
}

string SDIms::Isometry::GetIsometryClassName(int name) {
	ostringstream oss;
	switch(name) {
	case IDENTITY :
		oss << "Identité";
		break;
	case TRANSLATION :
		oss << "Translation";
		break;
	case AXIS_ROTATION :
		oss << "Rotation d'axe";
		break;
	case VISSAGE :
		oss << "Vissage";
		break;
	case SYMMETRY :
		oss << "Symétrie";
		break;
	case SYMMETRY_ROTATION :
		oss << "Symétrie + Rotation";
		break;
	case SYMMETRY_TRANSLATION :
		oss << "Symétrie + Translation";
		break;
	case CENTRAL_SYMMETRY :
		oss << "Symétrie centrale";
		break;
	default:
		oss << "Isométrie invalide";
		break;
	}
	return oss.str();
}

string SDIms::Isometry::info() {
	ostringstream oss;
	oss << "****************************************" << endl;
	/*if(m_P1)
		oss << "P1 = " << m_P1->toString() << endl;
	oss << "---------------------------" << endl;
	if(m_P2)
		oss << "P2 = " << m_P2->toString() << endl;*/
	oss << "---------------------------" << endl;
	oss << "Isométrie : " << m_Class << endl;
	oss << "Transformation T = " << m_T << endl;
	/*oss << "Det(T) = " << m_T.topLeftCorner<3,3>().determinant() << endl;
	oss << "Eigen val = " << m_EigValT.transpose() << endl;
	oss << "EigenSpace = " << m_EigSpaceT << endl;
	oss << "One indices = " << m_EigValT1Idx.transpose() << endl;
	oss << "Num One indices = " << TOneEigSpaceSize() << endl;
	oss << "Minus One indices = " << m_EigValT_1Idx.transpose() << endl;
	oss << "Num Minus One indices = " << m_EigValT_1Num << endl;
	oss << "t_a = " << m_ta.transpose() << endl;
	oss << "G = " << m_G << endl;
	oss << "Fixed point = " << m_FixedPoint.transpose() << endl;*/
	if(m_Feature)
		oss << m_Feature->info() << endl;
	oss << "****************************************" << endl;

	return oss.str();
}
