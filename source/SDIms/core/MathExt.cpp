/*
 * MathExt.cpp
 *
 *  Created on: Oct 10, 2013
 *      Author: dqviet
 */

#include "SDIms/core/MathExt.hpp"

double SDIms::MathExt::scalarProjection(const Vec3& a,
		const Vec3& b) {
	if(b.isZero())
		return 0.0;
	else
		return a.dot(b) / b.norm();
}

void SDIms::MathExt::vectorProjection(const Vec3& a, const Vec3& b,
		Vec3& projection) {
	projection = (a.dot(b) / b.dot(b)) * b;
}

bool SDIms::MathExt::vectorOnPlaneProjection(const Vec3& t1,
		const Vec3& t2, const Vec3& a, Vec3& projection) {
	// Tester la colinarité de t1 et t2
	double scaT1T2	= scalarProjection(t1, t2);
	if(equal(scaT1T2, 1.0) || equal(scaT1T2, -1.0)) {
		vectorProjection(a, t1, projection);
		return false;
	} else {
		Vec3 n = t1.cross(t2);	// normale
		Vec3 tmp = n.cross(a.normalized()); // ortho à n et a
		tmp = tmp.cross(n); // direction de a dans le plan (t1,t2)
		vectorProjection(a, tmp, projection);
		return true;
	}
}

bool SDIms::MathExt::vectorOnPlaneProjection(const Vec3& normal,
		const Vec3& a, Vec3& projection) {
	// Tester la colinarité de t1 et t2
	if(normal.isZero() || a.isZero()) {
		projection.Zero();
		return false;
	}
	Vec3 tmp = a.normalized();
	tmp = a.cross(normal); // ortho à n et a
	tmp = normal.cross(tmp); // direction de a dans le plan
	vectorProjection(a, tmp, projection);
	return true;
}

bool SDIms::MathExt::lineSegmentOnPlaneProjection(const Vec3& planNormal,
												const Vec3& planPoint,
												const Vec3& pStart,
												const Vec3& pEnd,
												Vec3& vProj) {
	Vec3 tmp	= pEnd - pStart;
	if(MathExt::equal(tmp.norm(), 0.0)) return false;
	double d = (tmp.dot(planNormal)) / (tmp.norm() * planNormal.norm());
	if(MathExt::equal(fabs(d), 1.0)) return false; // colinears
	d = -(planNormal.dot(planPoint));
	double sn = pStart.dot(planNormal) + d;
	double en = pEnd.dot(planNormal) + d;
	Vec3 startProj = pStart - sn * planNormal;
	Vec3 endProj = pEnd - en * planNormal;
	vProj = endProj - startProj;
	return true;
}

bool SDIms::MathExt::pointOnPlaneProjection(const Vec3& point,
											const Vec3& planeNormal,
											const Vec3& planePoint,
											Vec3& vProj) {
	Vec3 v 		= point - planePoint;
	double dist	= v.dot(planeNormal);
	vProj		= point - dist * planeNormal;
	return true;
}

bool SDIms::MathExt::reflectPointViaPlane(const Vec3& point,
				const Vec3& planeNormal,
				const Vec3& planePoint,
				Vec3& vRefl) {
	Vec3 v 		= point - planePoint;
	double dist	= v.dot(planeNormal);
	vRefl		= point - 2 * dist * planeNormal;
	return true;
}

double SDIms::MathExt::distancePointPlane(const Vec3& pa, const Vec3& normal, const Vec3& pPlane) {
	return normal.dot(pa - pPlane);
}

double SDIms::MathExt::HausdorffDistance(const vector<Vec3>& p, const vector<Vec3>& q) {
	double h = 0.0;

	vector<Vec3>::const_iterator pIt, qIt;

	for (pIt = p.begin(); pIt != p.end(); ++pIt) {
		double shortest = INT_MAX;

		for (qIt = q.begin(); qIt != q.end(); ++qIt) {
			double dPQ = (*pIt - *qIt).norm();

			if (dPQ < shortest)
				shortest = dPQ;
		}

		if (shortest > h)
			h = shortest;
	}

	return h;
}

void SDIms::MathExt::pointOnLineProjection(const Vec3& lineDirective,
		const Vec3& linePoint, const Vec3& point, Vec3& projection) {
	Vec3 v = point - linePoint;
	double length = scalarProjection(v, lineDirective);
	projection = linePoint + length * lineDirective;
}

double SDIms::MathExt::evalOrientedAngle2Vectors(const Vec3& refV, const Vec3& normal, const Vec3& intV) {
	double angle, sign;
	double dotProduct = refV.dot(intV);
	// prevent value outside of [-1, 1]
	if(dotProduct > 1.0) dotProduct = 1.0;
	if(dotProduct < -1.0) dotProduct = -1.0;
	angle = acos(dotProduct);
	sign = normal.dot(refV.cross(intV));
	return (sign < 0) ? angle * -1 : angle;
}

int SDIms::MathExt::mod(int a, int b) {
	int r = a % b;
	return (r < 0) ? r + b : r;
}
